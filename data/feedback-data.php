<?php

return [
  'education' => [
    //['Regelmäßige Nachhilfe zum Nacharbeiten', 'Super, vielen Dank.', 'male'],
    ['Abiturvorbereitung Mathematik intensiv', 'Es hat mir sehr viel gebracht.
    Ich fand die lockere Art sehr angenehm und lernfördernd.', 'female'],
    ['Regelmäßige Mathematik-Nachhilfe über mehrere Jahre', 'Fortschritte sind schnell bemerkbar.', 'male'],
    ['Regelmäßige Mathematik-Nachhilfe über mehrere Jahre', 'Vollkommen zufrieden mit Nachhilfe, nur die Terminvergabe ist für mich ein wenig ungeschickt.', 'male'],
    //['Regelmäßige Nachhilfe', 'Alles Super!', 'female'],
    ['Regelmäßige Mathematik-Nachhilfe über mehrere Jahre', 'vielen Dank - es war immer lustig und hat mir geholfen Grundlagen aufzuholen', 'female'],
    ['Online-Nachhilfe in PHP für berufliche Weiterbildung', 'Ich habe Nachhilfe in PHP in Anspruch genommen und muss sagen, dass es die absolut richtige Entscheidung war. Herr Beyer stellt sich spontan auf die anstehenden Themen ein und gibt ohne jegliche Vorbereitung sehr detaillierte und kompetente Antworten. Ich kann jedem, besonders Studenten, die Nachhilfe bei Herr Beyer empfehlen.', 'female'],
    ['Java Nachhilfe online, Studium', 'Ich war sehr zufrieden mit der Nachhilfe.
    Termine konnten auch recht spontan und kurzfristig ausgemacht werden.
    Angefangen von den Basics bis hin zu schweren Klausuraufgaben wurde alles mit gut verständlichen und fachlichen Erklärungen gezeigt.
    Ein sehr guter Nachhilfelehrer im Bereich des Programmierens!
    Jederzeit wieder gerne.', 'male'],
  ],

  'coding'=> [
    ['Umgesetzt wurde ein individueller Angebots-Konfigurator mit Wordpress-Integration', 'Herr Beyer hat die gewünschte Programmierarbeit schnell und zur vollsten Zufriedenheit erledigt. Den Preis halte ich ebenso für gerechtfertigt und würde die Dienstleistung definitiv weiterempfehlen.'],
    ['Planung und Umsetzung diverser Spezialkomponenten für die Moodle-Platform.', 'Ich bin insgesamt sehr zufrieden.'],
    ['Planung und Umsetzung kleiner Stand-Alone-Webanwedungen.', 'Vielen Dank für die gute Arbeit! Der aktuelle Arbeitsstand übersteigt unsere Erwartungen.'],
  ],
];