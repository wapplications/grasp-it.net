<?php
define('ENTRY_POINT', __FILE__);
require_once './inc/boot.php';
Middleware::all();
StatUte::tag('site', 'index');

# extending site template
($t = new Template('site.php'))
->set('body_class', 'general')
->start('main_content');
# START MAIN CONTENT
?>


<?= Template::component('paralax', ['image'=>'/images/f4.jpg', 'class'=>"px_start"])  ?>

<section class="container" style="
border-bottom: 1px solid #777;
text-align:center;
font-size:larger;
display:flex;
flex-direction:row;
gap:1em;
justify-content:center;
">
  <a href="/blog/">Zum Blog&nbsp;»</a>
  <a href="https://kursbegleiter.de">Zum Kursbegleiter-Portal&nbsp;»</a>
</section>

<section class="container">
  <section class="two-parts">
    <section class="section-coding">
      <h2 class="theme-fg">
        <?= Template::component('icon', ['name'=>'coding', 'color'=>COLOR_CODING, 'size'=>'1.2em', 'class'=>"ri"]) ?>
        <a href="coding.php">Software-Anpassungen</a>
      </h2>
      <h3>Web-Entwicklung</h3>
      <p>Umsetzung von Spezial&shy;anforderungen in Funktion und Optik. Fehleranalyse. Beratung.</p>
      <div class="catchwords theme-bg-light">
        <ul>
          <li>PHP</li>
          <li>API-Anbindungen</li>
          <li>Datenbanken</li>
          <li>Datenformate</li>
          <li>Javascript</li>
          <li>CSS</li>
          <li>Grafikanpassungen</li>
        </ul>
      </div>
      <div class="more">
        <a href="coding.php">Mehr <?= Template::component('icon', ['name'=>'arrow-right', 'color'=>COLOR_CODING, 'class'=>"hopp_h"]) ?></a>
      </div>
    </section>
    <hr/> 
    <section class="section-education">
      <h2 class="theme-fg">
        <?= Template::component('icon', ['name'=>'education', 'color'=>COLOR_EDUCATION, 'size'=>'1.2em', 'class'=>"ri"]) ?>
        <a href="education.php">Kurse &amp; Unterricht</a>
      </h2>
      <h3>Programmierung · Mathematik</h3>
      <p>Individueller Einzel&shy;unterricht und unter&shy;stützende Gruppen-Kurse. Online und vor Ort.</p>
      <div class="catchwords theme-bg-light">
        <ul>
          <li>Programmier-Kurse</li>
          <li>online Programmier-Coaching</li>
          <li>Mathematik-Unterricht</li>
        </ul>
      </div>
      <div class="more">
        <a href="education.php">Mehr <?= Template::component('icon', ['name'=>'arrow-right', 'color'=>COLOR_EDUCATION, 'class'=>"hopp_h"]) ?></a>
      </div>
    </section>
  </section>
</section>


<?php ($personal = new Template('personal.php'))
->set('featured', true)
->set('heading', 'Ein paar Worte über mich…')
->set('image', '/images/portrait.jpg')
->start('content'); ?>
  <p>
    Nach meinem Studium und der Ausbildung zum Mathematik- und Informatik-Lehrer begann 
    mein beruflicher Weg als Lehrer an einem beruflichen Gymnasium.
    Nach ein paar Jahren verließ ich den normalen Schuldienst,
    um dem Wunsch nach einer anderen Art zu leben und zu arbeiten nachzugehen:
    <a href="/education.php" class="education-fg">mehr individuelle Unterstützung, weniger Noten.</a>
  </p>
  <p>
    Über die Jahre habe ich mehrfach meine eigene Software geschrieben
    anstatt bereits vorhandene Frameworks und Plattformen zu verwenden:
    So lernte ich viele Konzepte und Strategien auf eine sehr intensive Art und Weise kennen.
    Das kommt mir jetzt zu Gute, wenn ich für meine Auftraggeber <a href="/coding.php" class="coding-fg">Speziallösungen erarbeite</a>.
  </p>
<?php unset($personal); ?>


<section class="info-notice"  style="padding:3em 0;">
  <section class="container">
    <div class="row">
      <div class="col-small">
        <svg width="150" height="90" style="display:block; margin-left:auto;">
          <use xlink:href="/images/geld.svg#layer1" style="fill:#55696d;" />
        </svg>
      </div>
      <div class="col-big" style="">
        <h2 style="margin-top:0;">Sozial &amp; Nachhaltig</h2>
        <p>
          Ich unterstütze nachhaltige oder soziale Auftraggeber: Bitte erfragen Sie besondere Konditionen.
        </p>
      </div>
    </div>
  </section>
</section>


<?= Template::component('contact.php', ['subject' => 'start', 'ruler' => true]) ?>

