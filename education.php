<?php
define('ENTRY_POINT', __FILE__);
require_once './inc/boot.php';
Middleware::all();
StatUte::tag('site', 'education');

# extending site template
($t = new Template('site.php'))
->set('body_class', 'education')
->set('title', 'Bildung')
->start('main_content');
# START MAIN CONTENT
?>

<?= Template::component('paralax', ['image'=>'/images/paralax-education.jpg', 'class'=>'px_other'])  ?>

<section class="container">
  <h1>
    <?= Template::component('icon', ['name'=>'education', 'color'=>COLOR_EDUCATION, 'size'=>'1.2em', 'class'=>"ri"]) ?>
    Kurse &amp; Unterricht
  </h1>


  <ul class="skills">
    <li>Einzel- und Gruppenunterricht Mathematik bis zum Abitur</li>
    <li>online Programmier-Coaching für Studenten und Berufstätige</li>
    <li>Grundlagen-Kurse Programmierung</li>
  </ul>
  
  <br/><br/>

  <h2>Mathematik</h2>
  
  <div class="illuFeature">
    <?= Template::component('illu', ['name' => 'denken', 'x' => 5, 'y' => 0]) ?>
    <div>
      <h4>Jeder denkt anders logisch!</h4>
      <p>
        Mathematik ist einfach nur pure Logik.
        Warum fällt es vielen trotzdem so schwer, dieser Logik zu folgen?
        Ganz einfach: wir sind keine Logikmaschinen. Unser Gehirn funktioniert ganz anders.
        Ob etwas sinnvoll und nachvollziehbar erscheint, hängt stark von unserer eigenen Erfahrungswelt
        und der eigenen Art über die Welt zu denken ab.
      </p>
    </div>
  </div>

  <div class="illuFeature">
    <?= Template::component('illu', ['name' => 'sinn', 'x' => 15, 'y' => 0]) ?>
    <div>
      <h4>Wo ist der Sinn?</h4>
      <p>
        Wenn man die Mathematik für sinnlos oder irrelevant hält,
        ist es nur folgerichtig, sich am liebsten gar nicht mit ihr beschäftigen zu wollen.
        Aber mal abgesehen von den Aufgaben im Schulbuch, die nicht selten an den Haaren herbeigezogen sind:
        Die Mathematik ist <em>die</em> Grundlage wichtiger wissenschaftlicher Erkenntnis,
        der wir in vielen Bereichen vertrauen.
      </p>
    </div>
  </div>

  <h2>Programmieren</h2>

  <div class="illuFeature">
    <?= Template::component('illu', ['name' => 'programmierung', 'x' => 15, 'y' => 0]) ?>
    <div>
      <h4>Programmierung ist überall!</h4>
      <p>
        Unglaublich viele Bereiche des alltäglichen Lebens werden von programmierten Systemen bestimmt;
        ob wir in unserer täglichen Arbeit mit Computern zu tun haben oder auch nur Dinge benutzen,
        die von Computern überwacht, geregelt oder unterstützt werden.
        Sich mit Programmierung zu beschäftigen,
        kann also berufliche Gründe haben – es hat aber auch eine allgemeinbildende Komponente!
      </p>
    </div>
  </div>
</section>

<?php ($personal = new Template('personal.php'))
->set('featured', true)
->set('image', '/images/portrait.jpg')
->set('heading', 'Erfahrungen')
->start('content'); ?>
  <p>
    Ich bin ausgebildeter Gymnasial-Lehrer für die Fächer Mathematik und Informatik.
    Ich habe Unterrichts- und Abitur-Erfahrung am allgemeinbildenden und beruflichen Gymnasium.
  </p>
  <p>
    2015–2020 war ich als Privatlehrer unter anderem an der <a href="https://www.birklehof.de">Internats-Schule Birklehof</a> tätig.
    Immer wieder fanden Studenten und Berufstätige bei mir Hilfe beim Programmieren.
  </p>
  <p>
    Seit 2020 arbeite ich als Lehrer / Lernbegleiter / Lern-Coach am <a href="https://montessori-offenburg.de">Montessorie-Zentrum Ortenau</a> in der Sekundarstufe.
  </p>
<?php $personal->start('after'); ?>
  <?= Template::component('feedback.php', ['section'=>'education', 'mode' => 'single', 'class' => 'education-bg-lightest']) ?>
  <p style="text-align:center;">
    Eigene Erfahrung teilen?<br/>
    <br/>
    <a class="button" href="/feedback.php">Feedback geben!</a>
  </p>
<?php unset($personal); ?>



<?= Template::component('contact.php', ['subject' => 'education', 'ruler' => false]) ?>
