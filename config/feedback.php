<?php
return (function() {
  $educationDetails = [
    'bonndorf-05-2020'=>(object)[
      'title'=>'HTML und CSS (VHS, Bonndorf, Mai 2020)',
      'form'=>'Sie'
    ],
    'nachhilfe-birklehof'=>(object)[
      'title'=>'Nachhilfe Birklehof',
      'form'=>'du'
    ],
    'programmier-coaching'=>(object)[
      'title'=>'Programmier Coaching',
      'form'=>'Sie'
    ],
  ];

  $codingDetails = [
    'programmierung'=>(object)[
      'title'=>'Programmierung, Datenverarbeitung',
    ],
    'design'=>(object)[
      'title'=>'Design, Grafik, Gestaltung'
    ],
  ];


  return (object)[
    'education' => (object)[
      'title'   => 'Kurs, Unterricht, Nachhilfe',
      'details' => $educationDetails,
    ],
    'coding'    => (object)[
      'title'   => 'Programmierung, Design, IT-Dienstleistung',
      'details' => $codingDetails,
      'form'=>'Sie'
    ],
  ];

})();





