<?php
use PHPUnit\Framework\TestCase;

final class CaptchaTest extends TestCase {
  private $captcha;

  public function setUp() : void {
    $this->captcha = Captcha::get_instance();
  }

  public function testIsCaptchaModule(): void {
    $this->assertInstanceOf(Captcha::class, $this->captcha);
  }

  public function testGetWordReturnsValidResult(): void {
    $obj = $this->captcha->getWord();
    $this->assertInstanceOf(stdClass::class, $obj);
    $this->assertObjectHasAttribute('right', $obj);
    $this->assertObjectHasAttribute('wrong', $obj);
    $this->assertObjectHasAttribute('hash', $obj);
    $this->assertNotEquals($obj->right, $obj->wrong);
    $this->assertEquals(md5($obj->right), $obj->hash);
  }

  public function testTestWordSucess() {
    $obj = $this->captcha->getWord();
    $this->assertTrue($this->captcha->testWord($obj->right, $obj->hash));
  }

  public function testTestWordFailure() {
    $obj = $this->captcha->getWord();
    $this->assertFalse($this->captcha->testWord($obj->right, $obj->wrong));
  }

  public function testTestWordFalseOnAnyEmptyInput() {
    $this->assertFalse($this->captcha->testWord('a', ''));
    $this->assertFalse($this->captcha->testWord('', 'a'));
  }

}
