<?php
use PHPUnit\Framework\TestCase;

final class TemplateTest extends TestCase {

  public function setUp() : void {
    Template::$template_dir = './tests/templates/';
  }


  public function testOutputOnDestruct() {
    $this->expectOutputString('test');
    $t = new Template('test.php');
    unset($t);
  }

  public function testRecordingParts() {
    $this->expectOutputString('testAAABBBXXX');
    ($t = new Template('test.php'))
    ->set('param3', 'XXX')
    ->start('param1');
    echo 'AAA';
    $t->start('param2');
    echo 'BBB';
    $t->output();
  }

  public function testFileComponent() {
    $this->expectOutputString('test!abx');
    echo Template::component('test.php', ['param1'=>'!abx']);
  }

  public function testFunctionComponent() {
    Template::register_component('test', function($params) {
      echo 'XXX'.$params['a'].$params['b'];
    }, ['a'=>'AAA', 'b'=>'BBB']);
    $this->expectOutputString('XXXZZZBBB');
    echo Template::component('test', ['a'=>'ZZZ']);
  }

}
