<?php
define('ENTRY_POINT', __FILE__);
require_once './inc/boot.php';
Middleware::all();
StatUte::tag('site', 'coding');

# extending site template
($t = new Template('site.php'))
->set('body_class', 'coding')
->set('title', 'Entwicklung')
->start('main_content');
# START MAIN CONTENT
?>

<?= Template::component('paralax', ['image'=>'/images/paralax-coding.jpg', 'class'=>'px_other'])  ?>


<section class="container">
<h1>
  <?= Template::component('icon', ['name'=>'coding', 'color'=>COLOR_CODING, 'size'=>'1.2em', 'class'=>"ri"]) ?>
  Software-Anpassungen
</h1>

  <div class="illuFeature">
    <?= Template::component('illu', ['name' => 'feature', 'x' => 5, 'y' => 10]) ?>
    <div>
      <h4>Neue Features – alte Bugs</h4>
      <p>
        Früher oder später fehlt in jeder Software eine Funktion,
        die so speziell ist, dass es nicht mal ein Plugin dafür gibt.
      </p>
      <p>
        Ich arbeite mich in Ihre Anwendung ein und finde für Sie eine möglichst nachhaltige Lösung.
        Manchmal ist es ein neues Plugin, manchmal eine neue Datei
         – und manchmal muss man etwas tiefer einsteigen.
      </p>
      <a href="#skills" ><?= Template::component('icon', ['name'=>'arrow-down', 'color'=>COLOR_CODING, 'class'=>'hopp_v']) ?>Skills</a>
    </div>
  </div>

  <div class="illuFeature">
    <?= Template::component('illu', ['name' => 'template', 'x' => 5, 'y' => 10]) ?>
    <div>
      <h4>Theme- und Template-Anpassungen</h4>
      <p>
        Fertige Themes und Templates nehmen einem viel Arbeit ab.
        Doch dann stellt man fest, dass sich nicht alles so verwirklichen
        lässt, wie man das gerne hätte.
      </p>
      <p>
        Ich finde die Stelle, an der eine Anpassung vorgenommen werden muss
        und passe auf diese Weise Themes und Templates nach Ihren Wünschen an.
      </p>
      <a href="#skills" ><?= Template::component('icon', ['name'=>'arrow-down', 'color'=>COLOR_CODING, 'class'=>'hopp_v']) ?>Skills</a>
    </div>
  </div>

  <div class="illuFeature">
    <?= Template::component('illu', ['name' => 'legacy', 'x' => 5, 'y' => 10]) ?>
    <div>
      <h4>Die Bürden einer Erbschaft</h4>
      <p>
        Ich unterstütze Sie dabei, altbewährte Systeme zu pflegen oder zu modernisieren.
        Falls nötig, kommen hierbei genau zugeschnittene Import- oder Export-Tools zum Einsatz,
        um Daten aus einem alten in das neue System zu überführen.
      </p>
      <ul>
        <li>Umwandeln von Daten in andere Formate</li>
        <li>PHP-Versions-Portierung</li>
        <li>Pflege und Modernisierung von Legacy-Code</li>
      </ul>
      <a href="#skills" ><?= Template::component('icon', ['name'=>'arrow-down', 'color'=>COLOR_CODING, 'class'=>'hopp_v']) ?>Skills</a>
    </div>
  </div>

</section>




<div class="white-feature" style="padding: 3em 0;">
<section class="container">

  <h2 id="skills" style="margin-top:0;">Skills</h2>
  <div class="row">
    <div class="col-half">
    <h4>Backend</h4>
    <ul class="skills">
    <li>PHP<small>OOP, Laravel, Wordpress, Moodle, Plugins, …</small></li>
    <li>API-Anbindungen<small>REST, Client- und Server-seitig</small></li>
    <li>Java</li>
    <li>Datenbanken<small>*SQL</small></li>
    <li>Python</li>
    <li>XML, XSLT</li>
  </ul>

    </div>
    <div class="col-half">
    <h4>Frontend</h4>
    <ul class="skills">
    <li>JavaScript, Typescript<small>JQuery, Vue.js, React, node, …</small></li>
    <li>CSS 3, SCSS, Sass<small>Bootstrap, …</small></li>
  </ul>
  <h4>Sonstiges</h4>
    <ul class="skills">
    <li>Grafikanpassungen</li>
    <li>Datenformate umwandeln</li>
    <li>Linux</li>
    </ul>
      
    </div>
  </div>
</section>
</div>



<?php ($personal = new Template('personal.php'))
->set('featured', false)
->set('heading', 'Ich bin der Richtige, wenn Sie…')
->set('image', '/images/portrait.jpg')
->start('content'); ?>
  <ul class="i-am-the-man">
    <li>…diese <strong>eine neue Funktion</strong> in Ihrer Software brauchen;</li>
    <li>…diesen <strong>einen lästigen Fehler</strong> endlich korrigiert haben wollen;</li>
    <li>…das teure Theme hier und da doch nicht ganz Ihrem <strong>Anspruch</strong> genügt;</li>
    <li>…einen zuverlässigen, <strong>kurzfristig verfügbaren</strong>, gewissenhaften IT-Partner suchen;</li>
    <li>…<strong>nachhaltige Lösungen</strong> für Ihre besonderen Wünsche benötigen.</li>
  </ul>
<?php $personal->start('after'); ?>
  <?= Template::component('feedback.php', ['section'=>'coding', 'mode' => 'single', 'class' => 'coding-bg-lightest']) ?>
<?php unset($personal); ?>





<?= Template::component('contact.php', ['subject' => 'coding', 'ruler' => true]) ?>
