<?php

spl_autoload_register(function ($class_name) {
  $sp = explode('\\', $class_name);
  if ($sp[0] === 'PHPUnit') return;
  $file = implode('/', $sp) . '.php';
  include CLASSES_DIR . '/' . $file;
});