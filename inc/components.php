<?php
/**
 * Define function components
 */
//if (!defined('ENTRY_POINT')) die();


Template::global_set('privacy_uri', '/legal.php#privacy');
Template::global_set('imprint_uri', '/legal.php#imprint');


/**
 * feathericons component
 * @param width
 * @param height
 * @param size (alternative for width and height)
 * @param color
 * @param class
 */ 
Template::register_component('icon', function($params) {
  $file = $params['file'];
  $w = $params['width'] ?? $params['size'];
  $h = $params['height'] ?? $params['size'];
  $c = $params['color'];
  $cl = $params['class'];
  return '<svg class="'.$cl.'" viewbox="0 0 24 24" height="'.$w.'" width="'.$h.'" fill="none" stroke="'.$c.'" stroke-width="2" stroke-linecap="round" style="vertical-align:middle;"><use xlink:href="'.$file.'#'.$params['name'].'" /></svg>';
}, [
  'file'=>'/images/all.svg',
  'width'=>null,
  'height'=>null,
  'size'=>24,
  'color'=>'black',
  'class'=>''
]);



/**
 * paralax component
 * @param image
 * @param class
 */
Template::register_component('paralax', function($params) {
  //$o = $params['offset'] ?? 0;
  //$h = $params['height'] ?? 80;
  $i = $params['image'];
  $c = $params['class'];
  //return '<div class="paralax" id="paralax" style="background-image:url('.$i.');  height: '.$h.'px; background-position-y:-'.$o.'px;"></div>';
  return '<div class="paralax '.$c.'" id="paralax" style="background-image:url('.$i.');"></div>';
}, [
  'image'=>'',
  'class'=>''
]);



/**
 * illustrations helper
 * @param name
 * @param x
 * @param y
 * @param w
 * @param h
 */
Template::register_component('illu', function($params) {
  return '  <svg width="'.$params['w'].'" height="'.$params['h'].'">
  <use xlink:href="/images/illus.svg#'.$params['name'].'" x="'.$params['x'].'" y="'.$params['y'].'" />
</svg>
';
}, [
  'name'=>null,
  'x'=>0,
  'y'=>0,
  'w'=>230,
  'h'=>230,
]);
