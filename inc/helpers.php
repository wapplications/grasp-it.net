<?php
/**
 * Hilfsfunktionen
 */

function e($s) {
    if (is_object($s) && method_exists($s, 'toHtml')) {
        return $s->toHtml();
    }
    return htmlspecialchars($s);
}

function timeTag($date) {
    if (empty($date)) return '';
    $hasTime = true;
    if (!($date instanceof DateTime)) {
        $dateObject = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        if ($dateObject === false) {
            $dateObject = DateTime::createFromFormat('Y-m-d', $date);
            $hasTime = false;
        }
    } else {
        $dateObject = $date;
    }
    if (empty($dateObject)) return '';
    return '<time datetime="'.e($dateObject->format(DateTimeInterface::ATOM)).'">'.e($dateObject->format('d.m.Y'.($hasTime?' H:i':''))).'</time>';
}

function hashtag($tag) {
    return '<a href="/blog?tag='.e($tag).'">#'.e($tag).'</a>';
}
  
function cachePrevent($httpFile) {
    if (preg_match('`^https?:`', $httpFile)) return $httpFile;
    if (substr($httpFile, 0, 1) === '/') $httpFile = substr($httpFile, 1);
    return '/'.$httpFile.'?'.filemtime(ROOT_DIR.'/'.$httpFile);
}