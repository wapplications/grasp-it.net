<?php

/**
 * Sehr primitives Captcha-Modul.
 * 
 * Wird mit einer Liste von "falschen" Begriffe nund den dazugehörigen
 * "richtigen" Begriffen konfiguriert.
 * In einem Formular zeigt man das falsche Wort in einem editierbaren Eingabefeld an
 * und den Hash des richtigen wortes in einem versteckten Feld.
 * Der Benutzer wird aufgefordert, das falsche Wort zu korrigieren, bevor er das Formular absendet.
 * Bei der Auswertung des Formulars wird dann geprüft, ob der Hasch des übermittelten Wortes, dem
 * übermittelten Hash entspricht.
 */
class Captcha {
  static protected $instance = null;
  private $words = [];

  protected function __construct() {

  }

  static public function get_instance() : self {
    if (is_null(self::$instance)) {
      self::$instance = new Captcha();
    }
    return self::$instance;
  }

  /**
   * @param words Wortliste
   * [right => wrong, ...]
   */
  public function setWords(array $words) {
    $this->words = $words;
  }

  /**
   * Einen zufälligen Begriff wählen.
   */
  public function getWord() : object {
    $currentWord = array_rand($this->words);
    $currentMissspelled = $this->words[$currentWord];
    return (object)[
      'right'=>$currentWord,
      'wrong'=>$currentMissspelled,
      'hash'=>md5($currentWord)
    ];
  }

  public function testWord($word, $word_hash) : bool {
    if (!$word_hash) return false;
    return (md5($word) === $word_hash);
  }



}