<?php

class Middleware {
  static $config;

  static function configure(array $cfg) {
    self::$config = $cfg;
  }

  static function all() {
    self::only(array_keys(self::$config));
  }

  static function only($only) {
    if (is_string($only)) {
      $only = [$only];
    }
    foreach ($only as $mw) {
      if (!isset(self::$config[$mw])) {
        continue;
      }
      $result = call_user_func(self::$config[$mw]);
    }
  }

  static function except($except) {
    if (is_string($except)) {
      $except = [$except];
    }
    $only = [];
    foreach (self::$config as $mw=>$callback) {
      if (!in_array($mw, $except)) {
        $only[] = $mw;
      }
    }
    self::only($only);
  }



}