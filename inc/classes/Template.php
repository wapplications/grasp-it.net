<?php

/**
 * Das ist eine relativ einfach Template-Engine.
 * 
 * Templates sind PHP-Dateien.
 * Nach dem Erzeugen eines Template-Objektes können Template-Parameter
 * auf drei Arten gesetzt werden:
 * ->set(): setzt einen Wert direkt.
 * ->add(): fügt zu einem Array hinzu.
 * ->start() / ->end(): Startet quasi die Aufnahme eines Parameters.
 * Alle Ausgaben, die zwischen ->start() und ->end() getätigt werden, 
 * werden als Wert für den Parameter gespeichert. In bestimmten Fällen
 * wird ->end() auch implizit aufgerufen (z.b. vor einem weiteren ->start() und vor der Ausgabe des Templates).
 * 
 * Die Ausgabe des Templates wird erzeugt, indem das Template-Skript aufgerufen wird.
 * Bevor das Template-Skript ausgeführt wird, werden alle zuvor gesetzt oder aufgezeichneten
 * Template-Parambeter als Variablen erzeugt und können im Template-Skript als solche verwendet werden. 
 * 
 * Die Ausgabe wird implizit erzeugt, wenn das Template-Objekt gelöscht wird.
 * So muss man im einfachsten Fall nur das Template-Objekt erzeugen, die benötigten Parameter 
 * aufzeichnen und am Ende des Skriptes wird die Ausgabe automatisch erzeugt.
 * Über ->output() kann die Ausgabe auch explizit erzeugt werden.
 * 
 * Beispiel für eine Template-Datei `seite.php`:
 * ```
 * <header><h1><?= $ueberschrift ?></h1></header>
 * <main><?= $inhalt ?></main>
 * <footer><?= $fuss ?></footer>
 * ```
 * 
 * Beispiel für den Aufruf eines Templates:
 * ```
 * <?php
 * ($T = new Template('seite.php'))
 * ->set('ueberschrift', 'Der Artikel')
 * ->start('inhalt');
 * ?>
 * <p>Hier wird der Inhalt über einfaches HTML erzeugt.</p>
 * <?php $T->end()->start('fuss')?>
 * <p>&copy; S. Beyer</p>
 * ```
 * Erzeugt folgende Ausgabe:
 * ```
 * <header><h1>Der Artikel</h1></header>
 * <main>
 * <p>Hier wird der Inhalt über einfaches HTML erzeugt.</p>
 * </main>
 * <footer>
 * <p>&copy; S. Beyer</p>
 * </footer>
 * ```
 * 
 * Hinweis: die Datei, die ein Template aufruft könnte ein View-Controller-Skript oder selbst auch ein Template sein.
 * 
 * Ein anderer Weg, ein Template aufzurufen ist ::component().
 * 
 * Die Klasse bietet noch eine weitere Funktionalität:
 * man kann mit ::register_component() eine Komponenten-Callback-Funktion registrieren, die man später 
 * über ::component() aufrufen kann.
 */
class Template {
  static $debug = false;
  static $template_dir = null;

  private $filename;

  private $parts = [];
  private $currentPartName = null;
  private $outputDone = false;
  private $name = '';

  //private $output = '';


  function __construct(string $fn, bool $rel = true) {
    $this->name = str_replace('/', '_', preg_replace('`\\.php$`', '', $fn));
    if ($rel) {
      $this->filename = self::$template_dir . $fn;
    } else {
      $this->filename = $fn;
    }
  }

  function __destruct() {
    $this->log('__destruct()');
    $this->output();
  }

  function set(string $name, $content) : Template {
    // ->end() entfernt, da man sonst nicht wärend der verarbeitung einen anderen parameter setzen kann...
    $this->parts[$name] = $content;
    return $this;
  }

  function add(string $name, $content) : Template {
    $this->end();
    if (!isset($this->parts[$name])) {
      $this->parts[$name] = [];
    }
    if (is_array($this->parts[$name])) {
      $this->parts[$name][] = $content;
    } else {
      $this->parts[$name] .= PHP_EOL . $content;
    }
    return $this;
  }

  function start(string $name) {
    $this->log('start');
    $this->end();
    $this->currentPartName = $name;
    ob_start(/*[$this, 'onOBOutput']*/);
  }

  function end() : Template {
    if (!$this->currentPartName) return $this;
    $content = ob_get_contents();
    ob_end_clean();
    //$this->log($content);
    $this->parts[$this->currentPartName] = $content; //$this->output;
    //$this->output = '';
    $this->log('end');
    $this->currentPartName = null;
    return $this;
  }

  /*function onOBOutput($d) {
    $this->log('onOBOutput '.$d);
    $this->output .= $d;
    return '';
  }*/


  function output() {
    $this->end();
    if ($this->outputDone) return;
    $this->outputDone = true;
    foreach($this->parts as $___name => $___content) {
      //$this->log($___content);
      # define template params as php variables
      ${$___name} = $___content;
    }
    include $this->filename;
  }

  function getOutput() {
    $this->end();
    ob_start();
    $this->output();
    $contents = ob_get_contents();
    ob_end_clean();
    return $contents;
  }





  function log($d) {
    if (!self::$debug) return;
    self::write_log('['.$this->name.':'.$this->currentPartName.'] '.$d);
  }

  static function write_log($d) {
    if (!self::$debug) return;
    file_put_contents('php://stdout', $d.PHP_EOL);
  }






  static $global = [];
  static function global_set($name, $content) {
    self::$global[$name] = $content;
  }
  static function global_add($name, $content) {
    if (!isset(self::$global[$name])) {
      self::$global[$name] = [];
    }
    if (is_array(self::$global[$name])) {
      self::$global[$name][] = $content;
    } else {
      self::$global[$name] .= PHP_EOL . $content;
    }
  }
  static function global_get($name) {
    return self::$global[$name] ?? null;
  }
  static function has_global($name) {
    return isset(self::$global[$name]);
  }



  static $components = [];
  static function register_component($name, $fkt, $default=[]) {
    self::$components[$name] = (object)['function'=>$fkt, 'default'=>$default];
  }
  
  static function has_component($name) : bool {
    if (isset(self::$components[$name])) {
      return true;
    }
    if (file_exists(self::$template_dir . $name)) {
      return true;
    }
    return false;
  }

  /**
   * @param name the name of a registered function component or a template file
   * @param params
   */
  static function component($name, $params = []) {
    if (!isset(self::$components[$name])) {
      if (!file_exists(self::$template_dir . $name)) {
        self::write_log('component '.$name. ' not found.');
        return;
      }
      # php template component
      $t = new self($name);
      foreach ($params as $k=>$v) {
        $t->set($k, $v);
      }
      return $t->getOutput();
    }
    # registered function component
    $d = self::$components[$name]->function;
    $params = array_merge(self::$components[$name]->default, $params);
    return $d($params);
  }


  private $componentParams = [];
  function startComponent($name, $params = []) {
    $this->componentParams[] = $params;
    $this->start($name);
  }

  function endComponent($name) {
    $this->end();
    $content = $this->parts[$name] ?? '';
    $params = array_pop($this->componentParams);
    $params['inner'] = $content;
    echo static::component($name, $params);
  }





}