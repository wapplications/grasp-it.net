<?php


interface PathProvider {
    function getPath($filename = ''): string;
    function getHTTPPath($filename = ''): string;
}