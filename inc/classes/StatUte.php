<?php


class StatSaver {
  private $data = [];
  private $encoding = 'json';

  function __construct($enc) {
    $this->encoding = $enc;
  }

  function __destruct() {
    $data = [];
    $data[] = date('Y-m-d H:i:s');
    $data[] = $this->data['ip'] ?? '?';
    $data[] = $this->data['method'] ?? '?';
    $uri = $this->data['uri'] ?? '?';
    unset($this->data['ip']);
    unset($this->data['uri']);
    unset($this->data['method']);
    switch ($this->encoding) {
      case 'query':
        $data[] = http_build_query($this->data);
      break;
      case 'json': 
        $data[] = json_encode($this->data);
      break;
      case 'special':
        foreach ($this->data as $key => $val) {
          if (is_integer($key)) {
            $data[] = urlencode($val);
          } else {
            $val = str_replace(' ', '_', $val);
            $val = str_replace("\n", '', $val);
            $val = str_replace("\r", '', $val);
            $val = str_replace("\t", ' ', $val);
            $data[] = urlencode($key).'='.json_encode($val);
          }
        }
      break;
      
      default:
    }
    $data[] = $uri;
    file_put_contents(DATA_DIR.'/statute.log', implode(' ', $data) . PHP_EOL, FILE_APPEND);
  }

  function addData($n, $v = null) {
    // TODO was wenn schon gesetzt? in array umwandeln?
    if ($v === null) {
      if ($this->encoding === 'json') {
        $this->data[$n] = true;
      } else {
        $this->data[] = $n;
      }
      return;
    }
    $this->data[$n] = $v;
  }

};

class StatUte {
  static $saver = null;

  static function middleware() {
    self::$saver = new StatSaver('special');
    self::$saver->addData('uri', $_SERVER['REQUEST_URI'] ?? '?');
    self::$saver->addData('ip', $_SERVER['REMOTE_ADDR'] ?? '?');
    self::$saver->addData('method', $_SERVER['REQUEST_METHOD'] ?? '?');
  }

  static function tag($name, $value = null) {
    if (!self::$saver) return;
    self::$saver->addData($name, $value /*?? true*/);
  }


}