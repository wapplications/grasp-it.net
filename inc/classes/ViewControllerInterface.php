<?php

interface ViewControllerInterface {
	function controller();
	function render();
}
