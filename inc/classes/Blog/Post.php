<?php

namespace Blog;

use PathProvider;
use RuntimeException;
use Series;
use Template;


class PostException extends RuntimeException {
}

class MetaNotFoundException extends PostException {
}

class NoPostException extends PostException {
}


class Post implements PathProvider {
    static $path = BLOG_ROOT;
    static $tag_cloud = [];
    static $sorter;


    private $meta;
    private $name;
    private $content = null;
    private $headings = [];


    public function __construct($name, $full = true) {
        $this->name = $name;

        if (!is_dir($this->getPath(''))) throw new NoPostException('Post »'.$name.'« ist not a directory');
        $fn = $this->getPath('meta.php');
        if (!file_exists($fn)) {
            throw new MetaNotFoundException('Post »'.$name.'« has no meta.php');
        }
        $meta = (include $fn);
        if (!is_array($meta)) throw new MetaNotFoundException('Post »'.$name.'« has valid meta.php');
        $this->meta = (object)$meta;
        $this->meta->name = $name;
        if ($full) {
            $this->loadContent();
        }
    }

    function getPath($name = ''): string {
        $postPath = self::$path.'/'.$this->name.'/';
        return $postPath . $name;
    }

    function getHTTPPath($name = ''): string {
        $httpBase = str_replace(ROOT_DIR, '', self::$path);
        $postPath = $httpBase.'/'.$this->name.'/';
        return $postPath . $name;
    }

    function loadContent() {
        $mainContentFile = $this->getPath('content.php');
        if (file_exists($mainContentFile)) {
            $template = new Template($mainContentFile, false);
            $template->set('post', $this);
            $this->splitPages($template->getOutput());
        }
    }

    function splitPages($content) {
        $regex = '`<!--\\s*PAGEBREAK:?\\s*(.*)\\s*-->`';

        if (!preg_match_all($regex, $content, $m, PREG_OFFSET_CAPTURE|PREG_SET_ORDER)) {
            $this->content = [$content];
            $this->headings = [];
            return;
        }

        $this->content = [];
        $this->headings = [];

        $startIndex = 0;
        foreach ($m as $set) {
            $endIndex = $set[0][1];
            $this->content[] = substr($content, $startIndex, $endIndex-$startIndex);
            $startIndex = $endIndex + strlen($set[0][0]);
            $this->headings[] = $set[1][0];
        }
        $this->content[] = substr($content, $startIndex);

        //echo '<pre>';
        //print_r($this->headings);
        //echo e(print_r($m, true));
        //echo '</pre>';
    }

    function getName(): string {
        return $this->name;
    }

    function getContent($page = null): string {
        if ($page === null) return implode('<hr class="clear" />'.PHP_EOL, $this->content);
        return $this->content[$page-1] ?? '';
    }

    function getPageCount(): int {
        return count($this->content);
    }


    function getPageHeading($page) {
        return $this->headings[$page-2] ?? '';
    }


    function getTitle() {
        return $this->getMeta('title', '- kein Titel -');
    }

    function getDate() {
        return $this->getMeta('date', null);
    }

    function getDescription() {
        return $this->getMeta('description', '');
    }

    function getTags() {
        return $this->getMeta('tags', []);
    }

    function getMeta($n, $d = null) {
        return $this->meta->{$n} ?? $d;
    }

    function isVisible(): bool {
        return (bool)$this->getMeta('show', false);
    }

    static function list($filter = [], $sort = []) {
        $posts = [];
        if (!is_dir(self::$path)) {
            throw new \Exception(self::$path . ' not an directory');
        }
        if (!($handle = opendir(self::$path))) {
            throw new \Exception('cannot open dir ' . self::$path);
        }
        while (($datei = readdir($handle)) !== false) {
            if (substr($datei, 0, 1) == ".") continue;
            $postPath = self::$path.'/'.$datei;
            if (!is_dir($postPath)) continue;
            try {
                $posts[$datei] = self::load($datei, false);
            } catch (PostException $e) {

            }
        }
        closedir($handle);
        $posts = self::filter_hidden($posts);
        self::create_tag_cloud($posts);
        $posts = self::filter($posts, $filter);
        self::sort($posts, $sort);
        return $posts;
    }

    static function create_tag_cloud($posts) {
        self::$tag_cloud = [];
        foreach ($posts as $post) {
            $tags = $post->getTags();
            foreach ($tags as $tag) {
                if (!isset(self::$tag_cloud[$tag])) {
                    self::$tag_cloud[$tag] = 0;
                }
                self::$tag_cloud[$tag]++;
            }
        }
        arsort(self::$tag_cloud);
    }

    static function filter_hidden($posts) {
        return array_filter($posts, function($post) {
            if (!$post->isVisible()) return false;
            return true;
        });
    }

    static private function filter($posts, $filter) {
        $tags = $filter->tags ?? null;
        $tags = $tags ? array_map(fn ($e) => strtolower($e), $tags) : null;
        $series = $filter->series;
        $names = null;
        if ($series) {
            $series = Series::find($series);
        }
        if ($series) {
            $names = $series->names;
        }
        return array_filter($posts, function($post) use($tags, $names) {
            if ($names && !in_array($post->getName(), $names)) return false;
            if ($tags && empty(array_intersect($tags, array_map(fn ($e) => strtolower($e), $post->getTags())))) return false;
            return true;
        });
    }

    static private function sort(&$posts, $sort) {
        $sortFunction = self::$sorter[$sort] ?? null;
        if (!$sortFunction) return;
        uasort($posts, $sortFunction);
    }

    static function load($name, $full = true): self {
        return new self($name, $full);
    }


    function getSeries($all = false) {
        $series = Series::for($this->getName());
        if ($all) return $series;
        if (empty($series)) return null;
        reset($series);
        return current($series);
    }
}