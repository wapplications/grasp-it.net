<?php
namespace Feedback;


class NameAndRating extends QuestionnaireElement {

protected $ratings;

function __construct($ratings, $config) {
  parent::__construct($config);
  $this->ratings = $ratings;
  $this->data['name'] = '';
}

function controller() {
  $this->data['name'] = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
  //if (empty($this->data['name'])) {
    //$this->setErrorMsg('name', 'Wenn Sie Ihren');
  //}
  // TODO ratings auch als ViewControllerBase ?
  $ratingErrors = [];
  foreach ($this->ratings as $rating) {
    $rating->value = filter_input(INPUT_POST, $rating->name, FILTER_VALIDATE_INT);
    if (!is_null($rating->value)) {
      $rating->value = floatval($rating->value);
    } else {
      $ratingErrors[] = $rating->name;
    }
  }
  if (!empty($ratingErrors)) {
    $this->setErrorMsg('ratings', $ratingErrors);
  }

  $ratingsData = [];
  foreach ($this->ratings as $rating) {
    $ratingsData[$rating->name] = $rating->value;
  }
  $this->data['ratings'] = $ratingsData;

}

function render() {
  
  $text = $this->getSieDuText('Wie zufrieden waren Sie mit', 'Wie zufrieden warst du mit');

  $ratingText = '<label '.($this->hasErrorMsg('ratings') ? 'class="error"' : '').' >'.$text.' …</label>';
  ?>
  <div class="row">
    <div class="col-small">
      <!-- TODO if (!$SECTION->fbObject)-->
      <p>
        <label for="feedback_name">Name (optional)</label>
        <input id="feedback_name" type="text" name="name" value="<?= htmlspecialchars($this->data['name'] ?? '' ) ?>" />
      </p>
      <!-- else $person = $this->dbObject->name; $form .= '
  <p>    <big><strong>Hallo '.htmlspecialchars($person) .'!</strong></big><br/><em>Schön, dass du mir ein Feedback geben möchtest.</em></p>
  ';-->
    </div>
    <div class="col-big">
    <?php
    Rating::$goodText = \Template::component('icon', ['name'=>'smile', 'size'=>'2em']);
    Rating::$badText = \Template::component('icon', ['name'=>'frown', 'size'=>'2em']);
    ?>
    <?= Rating::renderAll($ratingText, $this->ratings, ($this->hasErrorMsg('ratings') ? $this->getErrorMsg('ratings') : [])) ?><!-- TODO Du Sie -->
    </div>
  </div>
  <?php
}


}


