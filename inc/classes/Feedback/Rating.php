<?php
namespace Feedback;

// TODO Rating auch als ViewControllerBase ?

// TODO smily frowny
class Rating {
	static $ratingValues = array(0.0, 1.0, 2.0, 3.0, 4.0, 5.0);
	static $goodText = 'zu-<br/>frieden';
	static $badText = '<u>un</u>zu-<br/>frieden';
	
	var $title;
	var $name;
	var $value;
	
	function __construct($t, $n) {
		$this->title = $t;
		$this->name = $n;
		$this->value = null;
	}
	
	function render($error) {
		$row = '<tr>';
		$row .= '<td '.($error ? 'class="error-fg"' : '').' align="right">'.($this->title).'</td>';
		foreach (static::$ratingValues as $rv) {
			$checked = !is_null($this->value) && ($rv == $this->value);
			$row .= '<td align="center"><label class="ratingsRadio"><input name="'.$this->name.'" type="radio" value="'.$rv.'" '.($checked ? 'checked':'').'/><span></span></label></td>';
		}
		$row .= '</tr>';
		return $row;
	}
	
	static function renderAll($text, array $ratings, array $errors) {
		$table = '
		<table width="100%" class="ratingsTable">
			<tr>
				<th>
					<strong>'.$text.'</strong>
				</th>
				<th>
  				'.static::$badText.'
				</th>
				<th colspan="'.(count(static::$ratingValues)-2).'"></th>
				<th>
  				'.static::$goodText.'
				</th>
			</tr>
		';
		
		foreach ($ratings as &$r) {
			$table .= $r->render(in_array($r->name, $errors));
		}
		
		$table .='
		</table>';
		
		return $table;
	}
} // FeedbackRating

