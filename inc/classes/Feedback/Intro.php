<?php
namespace Feedback;


class Intro extends QuestionnaireElement {
  protected $intro;

  function __construct($intro, $config) {
    parent::__construct($config);
    $this->intro = $intro;
  }

  function controller() {
  }

  function render() {
    echo $this->intro;
  }

}

