<?php
namespace Feedback;



class Questionnaire extends QuestionnaireElement {
	protected $components = [];
	protected $hasErrors = false;
	protected $dataFile = null;
	protected $captcha = null;

	function __construct($components, $dataFile, $config) {
		parent::__construct($config);
		$this->components = $components;
		$this->dataFile = $dataFile;
		$this->data['privacy'] = false;
		$this->captcha = \Captcha::get_instance();
	}

	function controller() {
		if (!filter_has_var(INPUT_POST, 'feedback')) {
			return true;
		}
		
		$__mail_fake = filter_input(INPUT_POST, '__mail');
		if (!empty($__mail_fake)) {
			sleeo(40);
			die('nope');
		}

		$captchaInput = filter_input(INPUT_POST, 'captcha');
		$captchaHash = filter_input(INPUT_POST, 'captcha_hash');
		if (!$this->captcha->testWord($captchaInput, $captchaHash)) {
			$this->setErrorMsg('captcha', 'Spam-Schutz-Aufgabe fehlgeschlagen.');
		}

		$this->data['privacy'] = !!filter_input(INPUT_POST, 'privacy', FILTER_VALIDATE_INT);
		if (!$this->data['privacy']) {
			$this->setErrorMsg('privacy', 'Bitte Datenschutzbestimmungen beachten!');
		}
		$this->hasErrors = $this->hasError();
		foreach ($this->components as $k=>$component) {
			$component->controller();
			$this->hasErrors = $this->hasErrors || $component->hasError();
		}
		$this->hasErrors = $this->hasErrors || $this->hasError();
		if ($this->hasErrors) {
			return true;
		}
		# Speichern
		$data = $this->getMergedData();
		$data['section'] = $this->config['section']->sectionName;
		$data['sectionDetail'] = $this->config['section']->detailName;
		$jsonData = json_encode($data);

		$fp = \fopen($this->dataFile, 'a');
		if (\flock($fp, LOCK_EX)) {
			if (file_exists($this->dataFile) && filesize($this->dataFile)) {
				$jsonData = ',' . PHP_EOL . $jsonData;
			}
			\fwrite($fp, $jsonData);
			\flock($fp, LOCK_UN);
		} else {

		}
		\fclose($fp);

		return false;
	}

	function render() {
		$captchaChallenge = $this->captcha->getWord();
		?><form method="post"><?php
		if ($this->hasErrors) :?>
			<p class="error"><?= $this->getSieDuText('Bitte korrigieren oder ergänzen Sie die Angaben.', 'Bitte korrigiere oder ergänze die Angaben.') ?></p>
		<?php endif;
		foreach ($this->components as $component) {
			$component->render();
		}
		?>
        <input type="email" value="" style="display:none;" name="__email"/>
				<p>
		  		<label class="<?= $this->hasErrorMsg('captcha') ? 'error' : '' ?>"><?= $this->getSieDuText('Bitte korrigieren Sie den Fehler', 'Bitte korrigiere den Fehler') ?> (Spam-Schutz)</label>
          <input type="text" name="captcha" value="<?= htmlentities($captchaChallenge->wrong) ?>" />
					<input type="hidden" name="captcha_hash" value="<?= htmlentities($captchaChallenge->hash) ?>" />
				</p>
        <p>
					<div class="checkbox"><label <?= $this->hasErrorMsg('privacy') ? 'class="error"' : '' ?>>
					<input type="checkbox" name="privacy" value="1" <?= !!$this->data['privacy'] ? 'checked' : '' ?> />
					Ich willige in die Übermittlung und Speicherung meiner Daten gemäß <a target="_blank" href="<?= \Template::global_get('privacy_uri') ?>">Datenschutzerklärung, insbesondere zur Feedback-Funktion</a>, ein.</label></div>
				</p>
        <p>
          <input type="submit" name="feedback" value="Feedback geben" />
        </p>
		</form><?php
	}


	function getMergedData() {
		$data = $this->getData();
		foreach ($this->components as $component) {
			$data = array_merge($data, $component->getData());
		}
		return $data;
	}

}















return;


abstract class FeedbackController {
	protected $dbObject = null;
	/*protected $path = '';*/
	protected $data = []; // post data
	protected $error = [];
	/*protected $group = '';*/
	protected $ratings = [];
	
	public function __construct() {
		/*$this->controller = $controller;*/
		/*$this->path = $path;
		$this->group = $group;*/
		$this->dbObject = $feedbackObject = (object)['code'=>'',];
		$this->data = [];
	}
	
	public function &getData() {
		return $this->data;
	}
	
	public function getRatings() {
		return $this->ratings;
	}
	
	public function getError() {
		return $this->error;
	}
	
	public function setError($e) {
		$this->error = $e;
	}
	
	abstract function thx();
	//abstract function form();
	abstract function post();
  
  // TODO implement isPredefined()
  function isPredefined():bool {
    return false;
  }

	/*
	function getList($publish_only = true) {
		$f = [
			['group', $this->group],
		];
		if ($publish_only) {
			$f[] = ['anzeigen', 1, '>'];
		}
		$fbs = \Gewerbe\Feedback::getList($f, \Gewerbe\Feedback::LOAD_FULL);
		return $fbs;
	}
	
	
	
	
	function getFeedbackPublic() {
		$fbs = $this->getList(true);
		//_o($fbs);
		
		$ratings = $this->getRatings();
		$ratingValues = array();
		foreach ($ratings as &$r) {
			$ratingValues[$r->name] = 0;
		}
		unset($r);
		
		$count = 0;
		foreach ($fbs as &$fb) {
			$r = json_decode($fb->rating1);
			if (is_object($r)) {
				$count++;
				foreach ($ratings as &$_r) {
					$ratingValues[$_r->name] += $r->{$_r->name};
				}
				unset($_r);
			}
		}
		unset($fb);
		$maxRating = 5;
		if ($count) {
			foreach ($ratingValues as &$_r) {
				$_r /= $count;
				//$_r *= 2;
			}
			unset($_r);
		}
		//_o($ratingValues);
		
		$html = '
		<div class="infobox normaltextsize beige">
			<div>
				<table width="100%" style="width:100%;max-width:100%;">
		';
		
		foreach ($ratings as &$r) {
			$html .= '
					<tr>
						<td>'. $r->title .'</td>
						<td align="right" style="white-space:nowrap;" title="'. $ratingValues[$r->name] .'">
					';
			
			$fsc = (int)$ratingValues[$r->name];
			if ($fsc>$maxRating) $fsc = $maxRating;
			$h = ($ratingValues[$r->name]-$fsc)>=0.5;
			$esc = $maxRating-$fsc - ($h?1:0);
			
			$html .= str_repeat('<img src="/img/gewerbe/fullstar.png" alt="*"/>', $fsc);
			$html .= $h ? '<img src="/img/gewerbe/halfstar.png" alt="·"/>' : '';
			$html .= str_repeat('<img src="/img/gewerbe/emptystar.png" alt=""/>', $esc);
			
			$html .= '
						</td>
					</tr>
			';
		}
		
		$html .= '
				</table>
			</div>
		</div>
		';
		
		foreach ($fbs as &$fb) {
			$html .= '
			<div>
				<h4>'. htmlentities($fb->name) .'</h4>
				<strong>'. htmlentities($fb->zusatz) .'</strong>
				<p style="font-style:italic;">»'. nl2br(htmlentities($fb->text)) .'«</p>
			</div>
			';
		}
		$html .= '
		<p></p>
		<hr/>
		<p>
			<small>* Name geändert.</small>
		</p>
		';
		
		return $html;
	}*/
	
} // FeedbackHelperBase





class FeedbackControllerEducation extends FeedbackController {
		
	function __construct() {
		parent::__construct();
		

		$this->ratings = [
			new FeedbackRating('Terminvergabe',						'termine'),
			new FeedbackRating('Zuverlässigkeit',					'zuverlaessig'),
			new FeedbackRating('Erklärungen',						'erklaerungen'),
			new FeedbackRating('fachliche Kompetenz',				'kompetenz'),
			new FeedbackRating('zwischen&shy;menschlicher Umgang',		'umgang'),
		];
	}
	
	
	
	function thx() {
		return '			<p>
				Danke für dein Feedback.<br/>
				<small>Deine Rückmeldung hilft mir, mein Angebot zu verbessern.</small>
			</p>
			<p>
				Falls du die Nachhilfe
				deinen Geschwistern, Freunden oder Bekannten weiterempfehlen möchtest,
				schicke ihnen einfach diesen Link:
			</p>
			<p>
				<a href="http://www.wapplications.net/bildung/nachhilfe/"><code>http://www.wapplications.net/bildung/nachhilfe/</code></a>
			</p>
';
	}
	
	
	
	function post() {
		$this->data = [];
		
		$this->data['name']       = trim($this->p('name'));
		$this->data['text']       = trim($this->p('text'));
		$this->data['empfehlung'] = $this->p('empfehlung');
		
		$post = isset($_POST['feedback']);
		
		
		if (isset($_POST['anzeigen'])) {
			$this->data['anzeigen'] = $this->p('anzeigen', 0, 'int');
		} else {
			$this->data['anzeigen'] = '';
		}
		
		
		foreach ($this->ratings as &$r) {
			$r->value = $this->p($r->name, null, 'float');
		}
		
		
		
		if ($post) {
			$this->error = array();
			if (!$this->data['name'] && !$this->dbObject->code) {
				$this->error[] = 'Bitte gib zumindest deinen Vornamen an.';
			}
			if (!$this->data['text']) {
				$this->error[] = 'Bitte schreibe einen kleinen Feedback-Text.';
			}
			if (($this->data['anzeigen'])==='') {
				$this->error[] = 'Bitte sagen Sie mir, ob ich Ihr Feedback veröffentlichen darf!';
			}
			if (!isset($_POST['privacy']) || !$_POST['privacy']) {
				$this->error[] = 'Sie müssen in die Speicherung und Verarbeitung Ihrer Daten einwilligen.';
			}
			/*
			if (count($this->error)==0) {
				
				if (!$this->dbObject->name) $this->dbObject->name = $this->data['name'];
				$this->dbObject->text = $this->data['text'];
				$this->dbObject->empfehlung = $this->data['empfehlung'];
				
				$rating1 = array();
				foreach ($this->ratings as &$r) {
					$rating1[$r->name] = $r->value;
				}
				$this->dbObject->rating1 = json_encode($rating1);
				
				$this->dbObject->anzeigen = $this->data['anzeigen'];
				$this->dbObject->datum = new \DateTime(); //date('Y-m-d');
				$this->dbObject->group = $this->group;
				
				if ($this->dbObject->save()) {
					\WEPPO\System::redirect($this->path.'thx');
					trigger_error('Neues Feedback ID '.$this->dbObject->id, E_USER_NOTICE);
					exit;
				}
			}*/
		}
	}
	
} // FeedbackHelperNachhilfe

















/*



class FeedbackHelperCode extends FeedbackHelperBase {
	
	
	var $ratings;
	
	
	function __construct(&$controller, $dbObject, $path) {
		parent::__construct($controller, $dbObject, $path, 'code');
		
		$this->ratings = [
			new FeedbackRating('Kommunikation / Absprachen',	'kommunikation'),
			new FeedbackRating('Beratung',						'beratung'),
			new FeedbackRating('End-Ergebnis',					'ergebnis'),
			new FeedbackRating('Kosten',						'kosten'),
			new FeedbackRating('zwischen&shy;menschlicher Umgang',	'umgang'),
		];
	}
	
	function title() {
		return 'Feedback zum Programmier-Auftrag';
	}
	
	function description() {
		return '			<p>
				Ich würde mich freuen, wenn ich von meinen Auftraggebern ein Feedback zur gesamten Auftragsabwicklung bekomme.
			</p>
			<p>
				So kann ich mein Angebot verbessern und potentielle Auftraggeber können sehen, was sie erwartet.
			</p>
';
	}
	
	function thx() {
		return '			<p>
				Danke für Ihr Feedback.<br/>
				<small>Ihre Rückmeldung hilft mir, mein Angebot zu verbessern.</small>
			</p>
			';
	}
	
	
	function form() {
		$form = '<form action="'.$this->path.'post/'. htmlspecialchars($this->dbObject->code) .'" method="post">';
		if (!$this->dbObject->code) {
			$form .= '
						<div class="col2-nobr">
							<div style="padding-right:5px;">
								<div class="nice-input-padding small">
									<label for="feedback_name">Name</label>
									<input id="feedback_name" type="text" name="name" value="'. (isset($this->data['name']) ? htmlspecialchars($this->data['name']) : '' ) . '" class="nice-input small" style="width:100%;" />
								</div>
							</div>
						</div>
						<div class="col2-nobr">
							<div style="padding-left:5px;">
								<!--<small><em>Das Feedback muss nicht anonym sein - du musst ja nichts befürchten ...
								Dein Name wird niemals veröffentlicht.</em></small>-->
							</div>
						</div>
						
						<br class="clear" />
						';
		} else {
			$person = $this->dbObject->name;
			$form .= '
							<p>
								<big><strong>Hallo '.htmlspecialchars($person) .'!</strong></big><br/>
								<em>Schön, dass Sie mir ein Feedback geben möchten.</em>
							</p>
							';
		}
		
		$form .= '
						<hr style="margin-top:10px;" />
						';
		
		$form .= FeedbackRating::renderAll('Wie zufrieden waren Sie mit ...', $this->ratings);
		
		$form .= '
						<hr/>
						
						<div class="nice-input-padding small">
							<label class="" for="feedback_text">
								Ihre Nachricht<br/>
								<small>Worte sagen mehr als ein Rating, vor allem wenn etwas nicht zufriedenstellend verlief.</small>
							</label>
							<textarea id="feedback_text" name="text" class="nice-input small" style="width:100%;height:100px;" >'. (isset($this->data['text']) ? htmlspecialchars($this->data['text']) : '' ) . '</textarea>
						</div>
						
						<hr/>
			';
		
		$empfehlung = isset($this->data['empfehlung']) ? $this->data['empfehlung'] : '';
		
		$form .= '
						<p>
							Würden Sie mich weiterempfehlen?
						</p>
						<p>
							<input type="radio" value="positiv" name="empfehlung" id="feedback_e1" '.($empfehlung=='positiv' ? 'checked' : '' ).'/><label for="feedback_e1" class="inline">Ja, sicher!</label><br/>
							<input type="radio" value="negativ" name="empfehlung" id="feedback_e2" '.($empfehlung=='negativ' ? 'checked' : '' ).'/><label for="feedback_e2" class="inline">Nein, eher nicht!</label><br/>
							<!--<input type="radio" value="no-one"  name="empfehlung" id="feedback_e3" '.($empfehlung=='no-one'  ? 'checked' : '' ).'/><label for="feedback_e3" class="inline">niemandem</label>-->
						</p>
						<hr/>
		';
		
		
		$form .= '
						
						<div class="col2-nobr">
							<div style="padding-right:5px;">
								<p>
									Darf ich anderen Ihr Feedback anonymisiert zeigen?
								</p>
								<p>
									<input type="radio" value="1" name="anzeigen" id="anzeigen1" '. ((isset($this->data['anzeigen']) && ($this->data['anzeigen'])!=='' && !!$this->data['anzeigen']) ? 'checked' : '' ).' />
									<label for="anzeigen1" class="inline">Ja, mein Feedback anonymisiert veröffentlichen.</label><br/>
									<input type="radio" value="0" name="anzeigen" id="anzeigen0" '. ((isset($this->data['anzeigen']) && ($this->data['anzeigen'])!=='' && !$this->data['anzeigen']) ? 'checked' : '' ).' />
									<label for="anzeigen0" class="inline">Nicht veröffentlichen.</label><br/>
								</p>
							</div>
						</div>
						<div class="col2-nobr">
							<div style="padding-left:5px;">
								<p>
									<small><em>
									Wenn andere potentielle Auftraggeber Ihr Feedback sehen, können sie
									besser entscheiden, ob mein Angebot auch etwas für sie ist ...
									</em>
									</small>
								</p>
							</div>
						</div>
						
						
						<hr class="clear" />
						
						<input type="email" value="" style="display:none;" name="__email"/>
						
						<div class="checkbox"><label><input type="checkbox" name="privacy" value="1" /> Ich willige in die Übermittlung und Speicherung meiner Daten gemäß <a target="_blank" href="/datenschutz/">Datenschutzerklärung, insbesondere zur Feedback-Funktion</a>, ein.</label></div>
						
						<input type="submit" name="feedback" value="Feedback geben" class="nice-button green small" />
					
					</form>
		';
		return $form;
	}
	
	function post() {
		$this->data = [];
		
		$this->data['name']       = trim($this->controller->p('name'));
		$this->data['text']       = trim($this->controller->p('text'));
		$this->data['empfehlung'] = $this->controller->p('empfehlung');
		
		$post = isset($_POST['feedback']);
		
		
		
		if (isset($_POST['anzeigen'])) {
			$this->data['anzeigen'] = $this->controller->p('anzeigen', 0, 'int');
		} else {
			$this->data['anzeigen'] = '';
		}
		
		
		foreach ($this->ratings as &$r) {
			$r->value = $this->controller->p($r->name, null, 'float');
		}
		
		
		
		if ($post) {
			$this->error = array();
			if (!$this->data['name'] && !$this->dbObject->code) {
				$this->error[] = 'Bitte geben Sie zumindest Ihren Namen an.';
			}
			if (!$this->data['text']) {
				$this->error[] = 'Bitte schreiben Sie einen kleinen Feedback-Text.';
			}
			if (($this->data['anzeigen'])==='') {
				$this->error[] = 'Bitte sagen Sie mir, ob ich Ihr Feedback veröffentlichen darf!';
			}
			if (!isset($_POST['privacy']) || !$_POST['privacy']) {
				$this->error[] = 'Sie müssen in die Speicherung und Verarbeitung Ihrer Daten einwilligen.';
			}
			
			if (count($this->error)==0) {
				
				if (!$this->dbObject->name) $this->dbObject->name = $this->data['name'];
				$this->dbObject->text = $this->data['text'];
				$this->dbObject->empfehlung = $this->data['empfehlung'];
				
				$rating1 = array();
				foreach ($this->ratings as &$r) {
					$rating1[$r->name] = $r->value;
				}
				$this->dbObject->rating1 = json_encode($rating1);
				
				$this->dbObject->anzeigen = $this->data['anzeigen'];
				$this->dbObject->datum = new \DateTime(); //date('Y-m-d');
				$this->dbObject->group = $this->group;
				
				if ($this->dbObject->save()) {
					\WEPPO\System::redirect($this->path.'thx');
					trigger_error('Neues Feedback ID '.$this->dbObject->id, E_USER_NOTICE);
					exit;
				}
			}
		}
	}
	
} // FeedbackHelperCode








*/


































/*
class FeedbackHelperLektorat extends FeedbackHelperBase {
	function __construct(&$controller, $dbObject, $path) {
		parent::__construct($controller, $dbObject, $path, 'lektorat');
		
		$this->ratings = [
			new \Gewerbe\FeedbackRating('Kommunikation',			'kommunikation'),
			new \Gewerbe\FeedbackRating('Korrektur',				'korrektur'),
			new \Gewerbe\FeedbackRating('Lektorat',					'Lektorat'),
			new \Gewerbe\FeedbackRating('Preisgestaltung',			'preisgestaltung'),
			new \Gewerbe\FeedbackRating('Zuverlässigkeit',			'zuverlaessigkeit'),
		];
	}
	
	
	function title() {
		return 'Lektorat Feedback';
	}
	
	function description() {
		return '			<p>
				Ich würde mich freuen, wenn ich von Ihnen ein Feedback zu meiner Korrektur / meinem Lektorat bekomme.
			</p>
';
	}
	
	function thx() {
		return '			<p>
				Danke für Ihr Feedback.<br/>
				<small>Ihre Rückmeldung hilft mir, mein Angebot zu verbessern.</small>
			</p>
';
	}
	
	
	function form() {
		$form = '<form action="'.$this->path.'post/'. htmlspecialchars($this->dbObject->code) .'" method="post">';
		if (!$this->dbObject->code) {
			$form .= '
						<div class="col2-nobr">
							<div style="padding-right:5px;">
								<div class="nice-input-padding small">
									<label for="feedback_name">Name</label>
									<input id="feedback_name" type="text" name="name" value="'. (isset($this->data['name']) ? htmlspecialchars($this->data['name']) : '' ) . '" class="nice-input small" style="width:100%;" />
								</div>
							</div>
						</div>
						<div class="col2-nobr">
							<div style="padding-left:5px;">
								<small><em>Ihr Name wird niemals veröffentlicht.</em></small>
							</div>
						</div>
						
						<br class="clear" />
						';
		} else {
			$person = $this->dbObject->name;
			$form .= '
							<p>
								<big><strong>Hallo '.htmlspecialchars($person) .'!</strong></big><br/>
								<em>Schön, dass Sie mir ein Feedback geben möchten.</em>
							</p>
							';
		}
		
		$form .= '
						<hr style="margin-top:10px;" />
						';
		
		$form .= \Gewerbe\FeedbackRating::renderAll('Wie zufrieden waren Sie mit ...', $this->ratings);
		
		$form .= '
						<hr/>
						
						<div class="nice-input-padding small">
							<label class="" for="feedback_text">
								Ihre Nachricht<br/>
								<small>Worte sagen mehr als ein Rating. Ihre Nachricht an mich:</small>
							</label>
							<textarea id="feedback_text" name="text" class="nice-input small" style="width:100%;height:100px;" >'. (isset($this->data['text']) ? htmlspecialchars($this->data['text']) : '' ) . '</textarea>
						</div>
						
			';
		/ *
		$form .= '
						<hr/>
						<p>
							Wem würdest Sie meine Dienstleistung weiterempfehlen?
						</p>
			';
			
		$empfehlung = isset($this->data['empfehlung']) ? $this->data['empfehlung'] : '';
		$form .= '
						<p>
							<input type="radio" value="positiv" name="empfehlung" id="feedback_e1" '.($empfehlung=='positiv' ? 'checked' : '' ).'/><label for="feedback_e1" class="inline">meinen Freunden</label><br/>
							<input type="radio" value="negativ" name="empfehlung" id="feedback_e2" '.($empfehlung=='negativ' ? 'checked' : '' ).'/><label for="feedback_e2" class="inline">Leuten, die ich nicht mag</label><br/>
							<input type="radio" value="no-one"  name="empfehlung" id="feedback_e3" '.($empfehlung=='no-one'  ? 'checked' : '' ).'/><label for="feedback_e3" class="inline">niemandem</label>
						</p>
		';* /
		
		$form .= '
						<hr/>
						
						<div class="col2-nobr">
							<div style="padding-right:5px;">
								<p>
									Darf ich anderen Ihr Feedback anonymisiert zeigen?
								</p>
								<p>
									<input type="radio" value="1" name="anzeigen" id="anzeigen1" '. ((isset($this->data['anzeigen']) && ($this->data['anzeigen'])!=='' && !!$this->data['anzeigen']) ? 'checked' : '' ).' />
									<label for="anzeigen1" class="inline">Ja, mein Feedback anonymisiert veröffentlichen.</label><br/>
									<input type="radio" value="0" name="anzeigen" id="anzeigen0" '. ((isset($this->data['anzeigen']) && ($this->data['anzeigen'])!=='' && !$this->data['anzeigen']) ? 'checked' : '' ).' />
									<label for="anzeigen0" class="inline">Nicht veröffentlichen.</label><br/>
								</p>
							</div>
						</div>
						<div class="col2-nobr">
							<div style="padding-left:5px;">
								<p>
									<small><em>
									Wenn andere Besucher Ihr Feedback sehen, können sie
									besser entscheiden, ob mein Angebot auch etwas für sie ist ...
									</em>
									</small>
								</p>
							</div>
						</div>
						
						
						<hr class="clear" />
						
						<input type="email" value="" style="display:none;" name="__email"/>
						
						<div class="checkbox"><label><input type="checkbox" name="privacy" value="1" /> Ich willige in die Übermittlung und Speicherung meiner Daten gemäß <a target="_blank" href="/datenschutz/">Datenschutzerklärung, insbesondere zur Feedback-Funktion</a>, ein.</label></div>
						
						<input type="submit" name="feedback" value="Feedback geben" class="nice-button green small" />
					
					</form>
		';
		return $form;
	}
	
	function post() {
		$this->data = [];
		
		$this->data['name']       = trim($this->controller->p('name'));
		$this->data['text']       = trim($this->controller->p('text'));
		//$this->data['empfehlung'] = $this->controller->p('empfehlung');
		
		$post = isset($_POST['feedback']);
		
		
		
		if (isset($_POST['anzeigen'])) {
			$this->data['anzeigen'] = $this->controller->p('anzeigen', 0, 'int');
		} else {
			$this->data['anzeigen'] = '';
		}
		
		
		foreach ($this->ratings as &$r) {
			$r->value = $this->controller->p($r->name, null, 'float');
		}
		
		
		
		if ($post) {
			$this->error = array();
			if (!$this->data['name'] && !$this->dbObject->code) {
				$this->error[] = 'Bitte geben Sie zumindest Ihren Vornamen an.';
			}
			if (!$this->data['text']) {
				$this->error[] = 'Bitte schreiben Sie einen kleinen Feedback-Text.';
			}
			if (($this->data['anzeigen'])==='') {
				$this->error[] = 'Bitte sagen Sie mir, ob ich Ihr Feedback veröffentlichen darf!';
			}
			if (!isset($_POST['privacy']) || !$_POST['privacy']) {
				$this->error[] = 'Sie müssen in die Speicherung und Verarbeitung Ihrer Daten einwilligen.';
			}
			
			if (count($this->error)==0) {
				
				if (!$this->dbObject->name) $this->dbObject->name = $this->data['name'];
				$this->dbObject->text = $this->data['text'];
				$this->dbObject->empfehlung = ''; //$this->data['empfehlung'];
				
				$rating1 = array();
				foreach ($this->ratings as &$r) {
					$rating1[$r->name] = $r->value;
				}
				$this->dbObject->rating1 = json_encode($rating1);
				
				$this->dbObject->anzeigen = $this->data['anzeigen'];
				$this->dbObject->datum = new \DateTime(); //date('Y-m-d');
				$this->dbObject->group = $this->group;
				
				if ($this->dbObject->save()) {
					\WEPPO\System::redirect($this->path.'thx');
					trigger_error('Neues Feedback ID '.$this->dbObject->id, E_USER_NOTICE);
					exit;
				}
			}
		}
	}
	
	
	
} // FeedbackHelperLektorat
*/
