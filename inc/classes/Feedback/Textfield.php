<?php
namespace Feedback;

class Textfield extends QuestionnaireElement {

function __construct($config) {
  parent::__construct($config);
  $this->data['text'] = '';
}

function controller() {
  $this->data['text'] = filter_input(INPUT_POST, 'text', FILTER_SANITIZE_SPECIAL_CHARS);
  if (empty($this->data['text'])) {
    $this->setErrorMsg('text', true);
  }
}

function render() {
  ?>
<div class="">
  <label class="<?= $this->hasErrorMsg('text') ? 'error' : ''?>" for="feedback_text">
    <?= $this->getSieDuText('Ihre Nachricht', 'Deine Nachricht') ?><br/>
    <small>Worte sagen mehr als ein Rating:</small>
  </label>
  <textarea id="feedback_text" name="text" style="min-height:100px;" ><?= htmlspecialchars($this->data['text'] ?? '') ?></textarea>
</div>
  <?php
}

}

