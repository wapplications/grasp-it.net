<?php
namespace Feedback;



class RecommendAndPublish extends QuestionnaireElement {

function __construct($config) {
  parent::__construct($config);
  $this->data['empfehlung'] = null;
  $this->data['anzeigen'] = null;
}

function controller() {
  $this->data['empfehlung'] = filter_input(INPUT_POST, 'empfehlung', FILTER_SANITIZE_SPECIAL_CHARS);
  $this->data['anzeigen'] = filter_input(INPUT_POST, 'anzeigen', FILTER_VALIDATE_INT);
  if (is_null($this->data['empfehlung'])) {
    $this->setErrorMsg('empfehlung', 'Bitte eine Angabe machen.');
  }
  if (is_null($this->data['anzeigen'])) {
    $this->setErrorMsg('anzeigen', 'Bitte eine Angabe machen.');
  }
}

function render() {
  ?>
<div class="row">
  <div class="col-half">
    <p <?= $this->hasErrorMsg('empfehlung') ? 'class="error"' : '' ?>><?= $this->getSieDuText('Wem würden Sie mich weiterempfehlen?', 'Wem würdest du mich weiterempfehlen?') ?></p>
    <p>
      <?php
      $empfehlung = $this->data['empfehlung'] ?? '';
      foreach ([
        'positiv' => $this->getSieDuText('Ihren Freunden und Kollegen', 'meinen Freunden'),
        'negativ' => $this->getSieDuText('Ihrem schärfsten Konkurrenten', 'Leuten, die ich nicht mag'),
        'no-one'  => 'niemandem',
      ] as $k => $v) {
        echo '<input type="radio" value="'.$k.'" name="empfehlung" id="feedback_'.$k.'" '.($empfehlung==$k ? 'checked' : '' ).'/><label for="feedback_'.$k.'" class="inline">'.htmlentities($v).'</label><br/>';
      }
      ?>
    </p>
  </div>
  <div class="col-half">
    <p <?= $this->hasErrorMsg('anzeigen') ? 'class="error"' : '' ?>>
      <?= $this->getSieDuText('Darf ich anderen Ihr Feedback anonymisiert zeigen?', 'Darf ich anderen dein Feedback anonymisiert zeigen?') ?>
    </p>
    <p>
      <small><em>
      Wenn andere das Feedback sehen, können sie
      besser entscheiden, ob mein Angebot auch etwas für sie ist ...
      </em>
      </small>
    </p>
    <p>
      <input type="radio" value="1" name="anzeigen" id="anzeigen1" <?= ( !is_null($this->data['anzeigen']) && !!($this->data['anzeigen']??0) ? 'checked' : '' ) ?> />
      <label for="anzeigen1" class="inline">Ja, mein Feedback anonymisiert veröffentlichen.</label><br/>
      <input type="radio" value="0" name="anzeigen" id="anzeigen0" <?= ( !is_null($this->data['anzeigen']) &&  !($this->data['anzeigen']??0) ? 'checked' : '' ) ?> />
      <label for="anzeigen0" class="inline">Nicht veröffentlichen.</label><br/>
    </p>
  </div>
</div>
  <?php
  }

}


