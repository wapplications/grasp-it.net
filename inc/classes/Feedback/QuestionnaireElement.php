<?php
namespace Feedback;



abstract class QuestionnaireElement extends \ViewControllerBase {
	function getAddressForm() : string {
		return $this->config['section']->detail->form ?? $this->config['section']->form ?? 'Sie';
	}

	function isSie() : bool {
		return $this->getAddressForm() === 'Sie';
	}

	function isDu() : bool {
		return $this->getAddressForm() === 'du';
	}

	function getSieDuText($sie, $du) {
		return [
			'Sie' => $sie,
			'du'  => $du
		][$this->getAddressForm()];
	}
}
