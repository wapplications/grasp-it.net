<?php

class MailSender {
  static function middleware() {
    # because lots of sites uses contact form, call the handler here (like middleware).
    $sendMailResult = include CONTROLLER_DIR.'/send-mail.php';
    if ($sendMailResult) {
      Template::global_set('sendMailResult', $sendMailResult);
    }
  }
}