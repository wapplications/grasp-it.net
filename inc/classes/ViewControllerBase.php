<?php


abstract class ViewControllerBase implements ViewControllerInterface {
	protected $data;
	protected $error;
	protected $config;

	function __construct($config) {
		$this->data = [];
		$this->error = [];
		$this->config = $config;
	}

	function hasError() {
		return !empty($this->error);
	}

	function setErrorMsg($name, $msg) {
		$this->error[$name] = $msg;
	}
	function getErrorMsg($name) {
		return $this->error[$name] ?? '';
	}
	function hasErrorMsg($name) {
		return array_key_exists($name, $this->error);
	}

	function getData() {
		return $this->data;
	}

}
