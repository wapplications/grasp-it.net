<?php
/**
 * Initialize the application.
 * We currently don't use autoload, so classes must be loaded manually.
 */

use Blog\Post;

//if (!defined('ENTRY_POINT')) die();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



function customError(int $errno, string $errstr, string $errfile, int $errline, array $errcontext = []) {
    if (ob_get_length()) ob_end_clean();
    echo "<b>Error:</b> [$errno] $errstr";
    echo '<pre>';
    debug_print_backtrace();
    echo '</pre>';
    //restore_error_handler();
    //return false;
    return true;
}  
function customException(Throwable $exception) {
    if (ob_get_length()) ob_end_clean();
    echo "<b>Exception:</b> <code>".get_class($exception)."</code> <p>" . $exception->getMessage() .'</p>';
    echo '<pre>';
    echo 'in ' . $exception->getFile() . '('.$exception->getLine().')'.PHP_EOL;
    echo $exception->getTraceAsString();
    echo '</pre>';
    return true;
    //restore_exception_handler();
    //return false;
}
  
set_exception_handler('customException');
set_error_handler("customError");


# Paths
define('ROOT_DIR', realpath(__DIR__.'/../'));
define('CONFIG_DIR', realpath(__DIR__.'/../config/'));
define('INCLUDE_DIR', realpath(__DIR__));
define('CLASSES_DIR', realpath(__DIR__.'/classes/'));
define('TEMPLATE_DIR', realpath(__DIR__.'/../templates/'));
define('CONTROLLER_DIR', realpath(__DIR__.'/controller/'));
define('DATA_DIR', realpath(__DIR__.'/../data/'));
define('BLOG_ROOT', realpath(ROOT_DIR.'/blog'));


require_once INCLUDE_DIR.'/autoload.php';


Template::$debug = true;
Template::$template_dir = TEMPLATE_DIR.'/';

Captcha::get_instance()->setWords(include CONFIG_DIR.'/captcha.php');

Middleware::configure(include CONFIG_DIR.'/middleware.php');

require_once INCLUDE_DIR.'/helpers.php';

Post::$sorter = [
    'newfirst' => function($a, $b) {
        return $b->getDate() <=> $a->getDate();
    },
    'oldfirst' => function($a, $b) {
        return $a->getDate() <=> $b->getDate();
    },
];

# Theme colors for manual usage
define('COLOR_CODING', '#347f92');
define('COLOR_EDUCATION', '#b37337');


require_once INCLUDE_DIR.'/components.php';




