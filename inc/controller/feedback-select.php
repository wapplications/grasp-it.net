<?php
if (!defined('FEEDBACK_ENTRY')) die();

$feedbackConfig = include CONFIG_DIR . '/feedback.php';



$section = filter_input(INPUT_GET, 'section', FILTER_SANITIZE_SPECIAL_CHARS);
$detail = filter_input(INPUT_GET, 'detail', FILTER_SANITIZE_SPECIAL_CHARS);
if (!empty($section) && property_exists($feedbackConfig, $section)) {
  $result = new \stdclass;
  $result->section = $feedbackConfig->{$section};
  $result->sectionName = $section;
  if (array_key_exists($detail, $result->section->details)) {
    $result->detail = $result->section->details[$detail];
    $result->detailName = $detail;


    $result->fbObject = null;

    StatUte::tag('feedback', $result->sectionName.':'.$result->detailName);
    return $result;
  }
}



# extending site template
$SITE->start('main_content');
# START MAIN CONTENT
?>

<section class="container">
  <h1>Ihre Rückmeldung</h1>
  <form method="get">
  <p>Bitte wählen Sie, worauf sich Ihr Feedback bezieht:</p>
  <div class="feedbackSectionSelect">
  <?php foreach ($feedbackConfig as $k=>$v) : ?>
    <label id="fbSection_<?= $k ?>">
      <input type="radio" name="section" value="<?= $k ?>" /> <span><?= htmlentities($v->title) ?></span>
    </label>
  <?php endforeach; ?>
  </div>


  <?php foreach ($feedbackConfig as $k=>$v) : ?>
  <div id="feedbackDetail_<?= $k ?>" style="text-align:center;display:none;">
    <select style="max-width:300px;">
      <option value="">- bitte wählen -</option>
      <?php foreach ($v->details as $dk=>$dv) : ?>
        <option value="<?= $dk ?>"><?= htmlentities($dv->title) ?></option>
      <?php endforeach; ?>
    </select>
  </div>
  <?php endforeach; ?>

  <div id="feedbackSubmit" style="text-align:center;display:none;">
    <input type="submit" value="Weiter" />
  </div>
</section>




<script>
  document.addEventListener('DOMContentLoaded', function() {

    let fbs = document.querySelectorAll('.feedbackSectionSelect input[type=radio]');
    fbs.forEach(function(e){
      e.addEventListener('click', fb_selection_click.bind(e), {once:true});
    });

  });
</script>


<?php return true; ?>
