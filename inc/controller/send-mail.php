<?php
if (!defined('ENTRY_POINT')) die();


class ValidationException extends Exception {

}

if (!($_POST['send-mail'] ?? null)) {
  return null;
} 


$data = [];
$errors = [];

$vars = [
  'name' => function($name) {
    $v = filter_input(INPUT_POST, $name, FILTER_UNSAFE_RAW);
    if (!preg_match("`^[\\w\\.'\\-\\(\\)/&:;, ]+$`ui", $v)) {
      return null; //throw new ValidationException();
    }
    return $v;
  },
  'email' => FILTER_VALIDATE_EMAIL,
  'message' => FILTER_UNSAFE_RAW
];

foreach ($vars as $v => $f) {
  if (is_callable($f)) {
    $data[$v] = $f($v);  
  } else {
    $data[$v] = filter_input(INPUT_POST, $v, $f);
  }
  if (!$data[$v]) {
    $data[$v] = filter_input(INPUT_POST, $v, FILTER_UNSAFE_RAW); // damit der wert bearbeitet werden kann
    $errors[] = $v;
  }
}

/* debug
print_r($errors);
return ['data'=>$data, 'errors'=>$errors];
*/

$word = filter_input(INPUT_POST, 'word', FILTER_UNSAFE_RAW);
$word_hash = filter_input(INPUT_POST, 'word_hash', FILTER_UNSAFE_RAW);

if (!Captcha::get_instance()->testWord($word, $word_hash)) {
  $errors[] = 'word';
}

if (!empty($errors)) {
  return ['data'=>$data, 'errors'=>$errors];
}

$location = $_SERVER['REQUEST_URI'] ?? 'https://grasp-it.net/';
$subject = filter_input(INPUT_POST, 'subject', FILTER_SANITIZE_SPECIAL_CHARS);
$from = $data['name'].' <'.$data['email'].'>';


$message = 'Seite: '.$location.PHP_EOL
          .'Betreff: '.$subject. PHP_EOL.PHP_EOL
          .$data['message'];

if (!mb_send_mail('info@grasp-it.net', '[grasp-it.net] Nachricht', $message, "From: ".$from)) {
  $message = date('d.m.Y H:i:s').PHP_EOL.$from.PHP_EOL.$message;
  file_put_contents(DATA_DIR.'/mail.log', $message);
}


//if (strpos($location, '?') === false) {
//  $location
//}
$location .= '#sent';
//file_put_contents('php://stdout', $location.PHP_EOL);
// or redirect to sent page?
header('Location: '.$location);
die();