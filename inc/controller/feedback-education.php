<?php
/**
 * @return Function that returns FeedbackQuestionnaire
 */
if (!defined('FEEDBACK_ENTRY')) die();

use Feedback\{
  Questionnaire,
  Rating,
  NameAndRating,
  Textfield,
  RecommendAndPublish,
  Intro
};

return function($config) {


  $ratings = [
    new Rating('Terminvergabe',						'termine'),
    new Rating('Zuverlässigkeit',					'zuverlaessig'),
    new Rating('Erklärungen',						'erklaerungen'),
    new Rating('fachliche Kompetenz',				'kompetenz'),
    new Rating('zwischen&shy;menschlicher Umgang',		'umgang'),
  ];

  $intro = '
  <p>Ich würde mich freuen, wenn ich von meinen Teilnehmerinnen und Teilnehmern ein Feedback zum Unterricht bzw. Kurs bekomme.</p>
  <p>So kann ich mein Angebot verbessern und neue Kunden können sehen, was sie erwartet.</p>
  ';
  
  $components = [
    new Intro($intro, $config),
    new NameAndRating($ratings, $config),
    new Textfield($config),
    new RecommendAndPublish($config),
  ];
  
  $feedbackQuestionnaire = new Questionnaire($components, DATA_DIR.'/education.jsonlines', $config);
  
  return $feedbackQuestionnaire;
  
  

};
