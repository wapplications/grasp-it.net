<?php
define('ENTRY_POINT', __FILE__);
define('FEEDBACK_ENTRY', __FILE__);
require_once './inc/boot.php';
Middleware::only('stats');
StatUte::tag('site', 'feedback');


$SITE = new Template('site.php');
$SITE->set('body_class', 'general')
->set('title', 'Feedback');








if (filter_has_var(INPUT_GET, 'sent')) {
  $recommand = filter_has_var(INPUT_GET, 'recommand');
  # extending site template
  $SITE->start('main_content');
  # START MAIN CONTENT
  echo Template::component('feedback_start');
  echo '<section class="container">';
  echo '<h1>Rückmeldung geben</h1>';
  echo '<p>Danke für das Feedback.</p>';
  if ($recommand) {
    echo '<p>Wenn Sie meine Leistungen weiterempfehlen möchten,<br/>freue ich mich sehr: <code>https://grasp-it.net/</code></p>';
    echo '<script>insertShareButton()</script>';
  }
  echo '</section>';
  echo Template::component('feedback_end');
  return;
}

$SECTION = include CONTROLLER_DIR . '/feedback-select.php';
if ($SECTION === true) {
  return;
}





# Vorbereitung für Fragebogen
Template::register_component('feedback_start', function() use(&$SECTION) {
  ?>
<section class="container">
    <a href="/feedback.php">Zurücksetzen</a>
    <h1>Rückmeldung geben</h1>
    <?php if (isset($SECTION)) : ?>
      <h2 style="display:none;"><?= htmlentities($SECTION->section->title) ?></h2>
      <h4>Feedback zu: <em><?= htmlentities($SECTION->detail->title) ?></em></h4>
    <?php endif; ?>
  <?php  
}, []);

Template::register_component('feedback_end', function() use(&$SECTION) {
  ?>
      <?php if (isset($SECTION)) : ?>
      <?php endif; ?>
      </section>
  <?php  
}, []);


function errorContent($SITE) {
  # extending site template
  $SITE->start('main_content');
  # START MAIN CONTENT
  ?>

  <?= Template::component('feedback_start') ?>
  <p>Es ist ein Fehler aufgetreten: Dieses Feedback-Modul kann derzeit nicht angezeigt werden.</p>
  <?= Template::component('feedback_end') ?>

  <?php
}








$controllerFile = 'feedback-' . $SECTION->sectionName . '.php';
if (!file_exists(CONTROLLER_DIR . '/' . $controllerFile)) {
  errorContent($SITE);
  return;
}

$createController = include CONTROLLER_DIR . '/' . $controllerFile;

if (!is_callable($createController)) {
  errorContent($SITE);
  return;
}

$CONTROLLER = $createController([
  'section'=>$SECTION
]);


if (!$CONTROLLER->controller()) {
  StatUte::tag('feedback-given');
  $recommand = $CONTROLLER->getMergedData()['empfehlung'] === 'positiv';
  header('Location: /feedback.php?sent'.($recommand ? '&recommand' : ''));
  return;
}

# extending site template
$SITE->start('main_content');
# START MAIN CONTENT
?>


<?= Template::component('feedback_start'); ?>

<?php
$CONTROLLER->render();
?>

<?= Template::component('feedback_end'); ?>



