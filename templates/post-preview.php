<?php
/**
 * Template-File
 * @param post
 * @var \Blog\Post $post 
 */
if (!defined('ENTRY_POINT')) die();


Template::global_add('css', '

');

/*Template::global_set('title', $post->meta->title);
Template::global_set('keywords', $post->meta->keywords);
Template::global_set('date', $post->meta->date);
Template::global_set('description', $post->meta->description);
*/

$postUrl = '/blog?a='.$post->getName();
$thumb = $post->getMeta('thumbnail');
$emblem = $post->getMeta('emblem');
if (!$thumb) {
  $thumb = dummyPicture();
} else {
  $thumb = $post->getHTTPPath($thumb);
  $thumb = '<img src="' . e($thumb) . '" />';
}

$series = $post->getSeries();
?>


<article class="preview">
  <div class="picture">
    <?= $thumb ?>
    <?php if ($emblem) : ?>
    <img class="emblem" src="<?= htmlentities($emblem) ?>" />
    <?php endif; ?>
  </div>
  <div class="excerpt">
    <?php if ($series) : ?>
    <h2>
      <?= e($series->title) ?>
    </h2>
    <?php endif ?>
    <h1>
      <a href="<?= e($postUrl) ?>">
      <?= e($post->getTitle()) ?>
      </a>
    </h1>
    <?= timeTag($post->getDate()) ?>
    <p>
      <?= e($post->getDescription()) ?>
    </p>
    <p>
      <a href="<?= e($postUrl) ?>">Ansehen »</a>
    </p>
  </div>
</article>



