<?php
/**
 * Template-File
 * @param post
 * @var \Blog\Post $post 
 */
if (!defined('ENTRY_POINT')) die();


Template::global_add('css', '

');

/*Template::global_set('title', $post->meta->title);
Template::global_set('keywords', $post->meta->keywords);
Template::global_set('date', $post->meta->date);
Template::global_set('description', $post->meta->description);
*/

function nextpage(\Blog\Post $post, $page) {
  $count = $post->getPageCount();
  if ($page === 'all') return;
  if ($count <= 1) return;
  $name = $post->getName();
  $nextP = $page+1;
  if ($nextP<=$count) {$series = $post->getSeries();

    $h = $post->getPageHeading($nextP);
    if (empty($h)) return;
    echo '<section class="container next-page">';
    echo 'Auf der nächsten Seite:  <a href="?a='.e($name).'&amp;p='.$nextP.'">'.e($h).'&nbsp;»</a></li>';
    echo '</section>';
  }
}


function pagination(\Blog\Post $post, $page) {
  $count = $post->getPageCount();
  if ($count <= 1) return;
  $name = $post->getName();
  echo '<section class="container pagination">';
  if ($page === 'all') {
    echo '<div class="pagination-mode"><a href="?a='.e($name).'&amp;p=0">auf einzelnen Seiten lesen</a></div>';
    echo '</section>';
    return;
  }
  echo '<ul>';
  for ($p = 1; $p <= $count; $p++) {
    echo '<li class="'.($p==$page ? 'current' : '').'"><a href="?a='.e($name).'&amp;p='.$p.'">'.$p.'</a></li>';
  }
  /*$nextP = $page+1;
  if ($nextP<=$count) {
    echo '<li class="next-page"><a href="?a='.e($name).'&amp;p='.$nextP.'">nächste Seite »</a></li>';
  } */
  echo '</ul>';
  echo '<div class="pagination-mode"><a href="/blog?a='.e($name).'&amp;p=all">auf einer Seiten lesen</a></div>';
  echo '</section>';
}

$postUrl = '/blog/?a='.$post->getName();
$thumb = $post->getMeta('thumbnail');
if (!$thumb) {
  $thumb = dummyPicture(true, true);
} else {
  $thumb = $post->getHTTPPath($thumb);
}

$series = $post->getSeries();
?>
<script>
  function openBigPic(el) {

  }


</script>
<article class="blog-post">
  <header style="background-image:url(<?= e($thumb) ?>);">
    <section class="container">
      <div style="margin-bottom: 2em;">
        <a href="?">« alle Artikel</a>
      </div>
      <?php if ($series) : ?>
        <h2><?= e($series->title) ?></h2> <!-- TODO link zu allen posts zu dieser serie -->
      <?php endif ?>
      <h1><?= e($post->getTitle()) ?></h1>
      <div class="meta">
        <?= timeTag($post->getDate()) ?>
        <div>
          <?php foreach($post->getTags() as $tag) : ?>
            <?= hashtag($tag) ?>
          <?php endforeach; ?>
        </div>
      </div>
    </section>
  </header>
  <section class="container">
    <div class="row">
      <div class="col-big">
        <p style="font-weight:bold;">
          <?= e($post->getDescription()) ?>
        </p>
      </div>
      <div class="col-small" style="font-size:80%;">
        <?= renderPartOf($series, $post, 'last', 4) ?>
      </div>
    </div>
  </section>

  <?php pagination($post, $page); ?>
  
  <?php if ($post->getMeta('container') ?? true) : ?>
  <section class="container">
  <?php endif; ?>


  <?php if ($page === 'all') : ?>
    <?= $post->getContent() ?>
  <?php else : ?>
    <?= $post->getContent($page) ?>
  <?php endif; ?>


  <?php if ($post->getMeta('container') ?? true) : ?>
  </section>
  <?php endif; ?>

  <?php nextpage($post, $page); ?>
  <?php pagination($post, $page); ?>

</article>

<br class="clear"/>

<section class="container">
  <?= renderPartOf($series, $post, 'last', 4) ?>
</section>
