<?php
/**
 * Template-File
 * @param content
 * @param image
 * @param featured
 */
if (!defined('ENTRY_POINT')) die();

?>

<?php if($featured ?? false) : ?><div class="white-feature" style="margin-top:-1em; padding: 3em 0;"><?php endif; ?>
  <section class="container">
    <div class="row">
      <div class="col-big">
        <h2><em><?= $heading ?></em></h2>
        <?= $content ?>
      </div>
      <div class="col-small portrait" style="">
        <img src="<?= $image ?>" style="width:180px; height:180px; border-radius:50%; display:block; margin: 1em auto; box-shadow:0 2px 4px #444; border:solid 1px #ccc;"/>
      </div>
    </div>
    <?= $after ?? '' ?>
  </section>
<?php if($featured ?? false) : ?></div><?php endif; ?>
