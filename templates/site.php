<?php
/**
 * Template-File
 *  - Main Site Frame
 * @param main_content
 * @param title
 * @param keywords
 * @param date
 * @param body_class
 */
if (!defined('ENTRY_POINT')) die();

$current = $_SERVER['PHP_SELF'];
$isStart = ($current === '/index.php');

$default = (object)[
  'description'=>'Als Lehrer für Mathematik und Informatik erstelle ich Lern-Tools und didaktische Konzepte. Als Programmierer für PHP, Javascript und TypeScript bin ich auf das Erstellen und Anpassen von Webapplikationen im Backend und Frontend spezialisiert.',
];
?><!DOCTYPE html>
<html lang="de">
<head>
  <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script>
  <![endif]-->
  <meta charset="UTF-8">
  <meta name="author" content="Stefan Beyer">
  <meta name="creator" content="My fucking own software. Handmade.">

  <title><?= e($title ?? Template::global_get('title') ?? '') ?> | grasp:it - Stefan Beyer</title>
  <meta name="description" content="<?= e($description ?? Template::global_get('description') ?? $default->description) ?>" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="/images/logo16x16.png" type="image/png">
  <style>
  @import url("<?= e(cachePrevent('/styles.css')) ?>");
  @import url("<?= e(cachePrevent('/contact.css')) ?>");
  <?php /* < ?= implode(PHP_EOL, self::global_get('css'))*/ ?>
  </style>
  <script src="<?= e(cachePrevent('/js/tools.js')) ?>"></script>
</head>
<body class="section-<?= $body_class ?>">
  <header class="theme-bg">
    <section class="container">
      <div class="title">
        <a href="/" class="logo">
          <h1>grasp:it</h1>
        </a>
        <h2>Stefan Beyer - Web-Entwickler und Lehrer</h2>
      </div>
      <nav>
        <?php
        $nav_links = [
          '/index.php'=>['Startseite', 'home', 'general'],
          '/coding.php'=>['Entwicklung', 'coding', 'coding'],
          '/education.php'=>['Bildung', 'education', 'education'],
          '/blog'=>['Blog', 'book-open', 'blog'],
        ];
        foreach ($nav_links as $lurl=>$llink) {
          $isCurrent = ($lurl == $current);
          list($ltitle, $licon, $lclass) = $llink;
          echo '<a title="'.$ltitle.'" href="'.$lurl.'" class="'.$lclass.'-bg '.($isCurrent ? 'current' : '').'">
          '.Template::component('icon', ['name'=>$licon, 'color'=>'inherit']) .'
          </a>';
        }
        ?>
      </nav>
    </section>
  </header>
  <main>


<?= $main_content ?>


  </main>  
  <footer class="theme-bg">
    <section class="container">
      &copy; Stefan Beyer | <a href="<?= Template::global_get('imprint_uri') ?>">Impressum</a> | <a href="<?= Template::global_get('privacy_uri') ?>">Datenschutz</a>
    </section>
    <script>
    //document.addEventListener('DOMContentLoaded', function() {
      //<span id="_mail2"><span>
      //document.getElementById('_mail2').textContent = 'info'+'@'+'grasp'+'-it.net';
    //});
    </script>
  </footer>



  <script>
    
    (function(){
      let paralax = document.getElementById('paralax');
      if (!paralax) return;
      let cy = parseInt(paralax.style.backgroundPositionY);
      window.addEventListener('scroll', function(e) {
        let sy = window.scrollY;
        let f = 0.08;
        sy = 50 - sy*f;
        let a = sy+'%';
        //console.log(a);
        paralax.style.backgroundPositionY = a;
      });
    })();


    
    window.addEventListener('load', function() {
      
      if (location.hash === '#sent') {
        location.hash = '';
        let backdrop = document.createElement('div');
        backdrop.classList.add('backdrop');
        let dlg = document.createElement('div');
        dlg.classList.add('dlg');
        backdrop.appendChild(dlg);
        document.getElementsByTagName('body').item(0).appendChild(backdrop);
        dlg.innerHTML = "<h2>Danke</h2><p>Die Nachricht wurde übermittelt.</p>";
        backdrop.addEventListener('click', function() {
          backdrop.remove();
        });
        dlg.addEventListener('click', function(event) {
          //event.stopImmediatePropagation();
        });
      }

    });


    fillContact();


  </script>

<?php

    $endscripts = Template::global_get('endscripts');
    if (is_array($endscripts)) {
      foreach ($endscripts as $endscript) {
        echo '<script src="'.e(cachePrevent($endscript)) . '"></script>';
      }
    }

  ?>
  

</body>
</html>
