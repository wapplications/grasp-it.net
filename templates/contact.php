<?php
/**
 * Template-File
 * @param subject
 * @param ruler
 */
if (!defined('ENTRY_POINT')) die();

$word = Captcha::get_instance()->getWord();

$sendMailResult = Template::global_get('sendMailResult');
if (!$sendMailResult) {
  $sendMailResult['errors'] = [];
  $sendMailResult['data'] = ['name'=>'', 'email'=>'', 'message'=>''];
}

Template::global_add('css', '

');
?>






<section class="container" id="contactFormOuter">
  <?php if (!isset($ruler) || $ruler) : ?>
  <hr/>
  <?php endif; ?>
  <div class="contact-info">

    <?= Template::component('illu', ['name' => 'ornament2left', 'w' => 230, 'h' => 120]) ?>

    <div class="cinfo">
      <p class="name"><span>Stefan Beyer</span></p>
      <!--<p><?= Template::component('icon', ['name'=>'phone', 'color'=>'#534646', 'size'=>'1em']) ?> <span class="fillContact phone"></span></p>-->
      <p><?= Template::component('icon', ['name'=>'mail',  'color'=>'#534646', 'size'=>'1em']) ?> <span class="fillContact email"></span></p>
    </div>

    

  </div>



<form class="contactForm" method="post" action="#contactForm" id="contactForm">
  <input type="hidden" name="subject" value="<?= htmlentities($subject) ?>"/>
  <div class="row">
  <div class="col-half">
    <p>
      <label class="<?= in_array('name', $sendMailResult['errors']) ? 'error' : '' ?>">Name</label>
      <input name="name" type="text" value="<?= htmlentities($sendMailResult['data']['name']) ?>"/>
    </p>
    <p>
      <label class="<?= in_array('email', $sendMailResult['errors']) ? 'error' : '' ?>">E-Mail</label>
      <input name="email" type="email" value="<?= htmlentities($sendMailResult['data']['email']) ?>"/>
    </p>
  </div>
  <div class="col-half">
    <p>
      <label class="<?= in_array('message', $sendMailResult['errors']) ? 'error' : '' ?>">Nachricht</label>
      <textarea name="message" rows="4"><?= htmlentities($sendMailResult['data']['message']) ?></textarea>
    </p>
    <p>
      <label class="<?= in_array('word', $sendMailResult['errors']) ? 'error' : '' ?>">Bitte korrigieren Sie den Fehler (Spam-Schutz)</label>
      <input type="text" name="word" value="<?= htmlentities($word->wrong) ?>" />
      <input type="hidden" name="word_hash" value="<?= htmlentities($word->hash) ?>" />
    </p>
  </div>
  </div>
  <p style="text-align:center;">
    <input type="submit" name="send-mail" value="Senden" />
  </p>
</form>





</section>




<section class="container">
  <svg viewbox="0 0 465 150" width="232" height="75" style="display:block; margin:2em auto;">
    <use xlink:href="/images/Filigrana.svg#Layer_1" style="fill:#dddddd;" />
  </svg>
</section>


