<?php
/**
 * @param widths
 * @param first
 * @param second
 */
$widths = explode('|', $widths ?? 'half|half');
if (count($widths) !== 2) {
  $widths = ['half', 'half'];
}
list($firstWidth, $secondWidth) = $widths;
?>
<div class="row">
<div class="col-<?= $firstWidth ?>">
<?= $first ?>
</div>
<div class="col-<?= $secondWidth ?>">
<?= $second ?>
</div>
</div>