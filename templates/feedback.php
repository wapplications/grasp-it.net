<?php
/**
 * Template-File
 * @param section
 * @param mode single|all
 */
if (!defined('ENTRY_POINT')) die();

$mode = $mode ?? 'single';

$references = include DATA_DIR.'/feedback-data.php';

$sectionReferences = $references[$section] ?? [];
if (empty($sectionReferences)) {
  return;
}

if ($mode === 'single') :
  $randIndex = array_rand($sectionReferences);
  list($title, $text) = $sectionReferences[$randIndex];
  //eine< ?= ($gender=='female'?'r Kundin':'s Kunden') ? > 
  ?>

<div class="singleFeedback <?= $class ?? '' ?>">
  <h4>Feedback</h4>
  <?php render_reference($title, $text); ?>
</div>

<?php endif;

function render_reference($title, $text) { ?>


<p>»<?= nl2br(htmlentities($text)) ?>«</p>
<small><?= htmlentities($title) ?></small>


<?php }
