



function fillContact() {
  document.addEventListener('DOMContentLoaded', function() {
    let fillElements = document.querySelectorAll('.fillContact');
    fillElements.forEach((e)=>{
      let style = getComputedStyle(e, '::after');
      //console.log(style);
      let content = style.content;
      if (content.startsWith('"')) content = content.substring(1);
      if (content.endsWith('"')) content = content.substring(0, content.length-1);
      e.innerText = content;
      e.classList.add('filled');
    });
  });  
}



//##############


/**
 * Extract pixel value from string
 */
function parsePX(px) {
  return parseFloat(px);
}

/**
 * returns Promise that rsolves at next event loop
 */
function nextEL(passOn) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(passOn);
    });
  });
} 


/**
 * returns Promise that rsolves at end of transitions
 * startet in transitions callback
 */
function promisifyTransition(element, transitions) {
  return new Promise((resolve, reject) => {
    element.addEventListener('transitionend', ()=>{
      resolve(element);
    }, {once: true});
    transitions(element);
  });
}


function insertShareButton() {
  if (typeof navigator.share === 'undefined') {
    return;
  }
  console.log('insertShareButton()');
  document.open();
  document.write('<a href="javascript: share({title:\'Stefan Beyer - Web-Entwickler und Lehrer\',url:\'https://grasp-it.net/\'})">Teilen</a>');
  document.close();
}


/**
 * 
 */
function share(data) {
  if (typeof navigator.share === 'undefined') {
    return;
  }
  return navigator.share(data);
}


//################################################


function fb_selection_click() {
  let label = this.parentElement;
  let offsetParent = label.offsetParent;
  //let labelPos = {x: label.offsetLeft, y: label.offsetTop};
  let s = getComputedStyle(label);
  let containerStyles = getComputedStyle(offsetParent);
  let section = this.value;
  
  let fbs = document.querySelectorAll('.feedbackSectionSelect input[type=radio]');
  fbs.forEach((e) => {
    if (e!==this) {
      let animationPromise = null;
      // je nach aktueller anzeige unterschiedliche animationen machen
      if (containerStyles.display === 'flex') {

        let marginProp = {education: 'marginLeft', coding: 'marginRight'}[e.value];
        e.disabled = true;

        e.parentElement.style.transitionProperty = 'opacity';

        animationPromise = promisifyTransition(e.parentElement, (element)=>{
          element.style.opacity = 0;
        }).then((element)=>{
          label.style[marginProp] = (parsePX(s.width) + parsePX(s.marginLeft)*3)+'px';
          element.style.display = 'none';
          return nextEL();
        }).then(()=>promisifyTransition(label, (element)=>{
          label.style.transitionProperty = marginProp.replace("R", '-r').replace("L", '-l');
          label.style[marginProp] = '1em';
        }));
      } else {// s.display === 'flex'
        e.parentElement.style.height = s.height;
        e.parentElement.style.overflow = 'hidden';
        e.parentElement.style.transitionProperty = 'height,margin-top,margin-bottom,padding-top,padding-bottom,opacity';

        animationPromise = nextEL(e.parentElement).then((element)=>promisifyTransition(element, (element)=>{
          element.classList.add('remove');
          element.style.height = 0;
          element.style.paddingTop = 0;
          element.style.paddingBottom = 0;
          element.style.marginBottom = 0;
          element.style.marginTop = e.value === 'education' ? '-1em' : 0;
          element.style.opacity = 0;
        })).then((element)=>{
          element.style.display = 'none';
          return nextEL(element);
        });
      }

      animationPromise.then(()=>{
        let detailContainer = document.getElementById('feedbackDetail_'+section);
        let detailContainerSelect = detailContainer.querySelector('select');
        detailContainerSelect.name = "detail";
        detailContainerSelect.addEventListener('change', ()=>{
          let value = detailContainerSelect.value;
          if (!value) return;
          let submitContainer = document.getElementById('feedbackSubmit');
          submitContainer.style.opacity = 0;
          submitContainer.style.transitionProperty = 'max-height,opacity,margin-top,margin-bottom';
          submitContainer.style.transitionDuration = '0.4s';
          submitContainer.style.display = 'block';
          nextEL(submitContainer).then((element)=>{
            return promisifyTransition(element, (element)=>{
              element.style.opacity = 1;
              element.style.marginTop = "1em";
              element.style.marginBottom = "1em";
            });
          });
        });
    
        detailContainer.style.maxHeight = 0;
        detailContainer.style.opacity = 0;
        detailContainer.style.overflow = 'hidden';
        detailContainer.style.transitionProperty = 'max-height,opacity,margin-top,margin-bottom';
        detailContainer.style.transitionDuration = '0.4s';
        detailContainer.style.display = 'block';
        return nextEL(detailContainer);
      }).then((element)=>{
        return promisifyTransition(element, (element)=>{
          element.style.maxHeight = "100px";
          element.style.opacity = 1;
          element.style.marginTop = "1em";
          element.style.marginBottom = "1em";
        });
      });

    }
  });
}
