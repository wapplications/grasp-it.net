console.info('Lern to differ');


class Learntodiffer {


    constructor($element, options) {

        this.cls = options;

        this.mode = 'learn';

        this.$element = $element;
        this.$element.style.display = 'flex';
        this.$element.style.flexDirection = 'column';
        this.$element.style.alignItems = 'center';
        //this.$element.style.maxHeight="100vw";
        //this.$element.style.maxWidth="100vh";
        this.$element.style.width="100%";


        this.$pic = document.createElement('div');
        this.$pic.style.flex = '1 1 300px';
        this.$element.appendChild(this.$pic);
        this.$label = document.createElement('div');
        this.$label.style.flex = '0 1 auto';
        this.$label.style.fontSize = '2em';
        this.$label.style.fontWeight = 'bold';
        this.$element.appendChild(this.$label);

        let $bc = document.createElement('div');
        $bc.style.display = 'flex';
        $bc.style.flexDirection = 'row';
        $bc.style.gap = '1em';
        this.$element.appendChild($bc);

        this.$button1 = document.createElement('button');
        this.$button1.textContent = 'Weiter lernen'
        this.$button1.addEventListener('click', ()=>this.go1())
        $bc.appendChild(this.$button1);

        this.$button2 = document.createElement('button');
        this.$button2.textContent = "Zum Test »"
        this.$button2.addEventListener('click', ()=>this.go2())
        $bc.appendChild(this.$button2);

        this.counter = {correct: 0, wrong: 0, learn: 0};
        this.indexes = null;
        this.newPic();
    }

    go1() {
        if (this.mode === 'learn') {
            this.newPic();
        } else if (this.mode === 'test') {
            if (this.currentClsIdx === 0) {
                this.counter.correct++;
            } else {
                this.counter.wrong++;
            }
            this.nextTest();
        }
    }

    go2() {
        if (this.mode === 'learn') {
            this.$label.textContent = '';
            this.$button1.textContent = this.cls[0];
            this.$button2.textContent = this.cls[1];
            this.indexes = (new Array(6).fill(0))
            this.indexes.push(...(new Array(6)).fill(1));
            this.indexes.sort(() => Math.random() - 0.5);
            this.mode = 'test';
            this.newPic();
        } else if (this.mode === 'test') {
            if (this.currentClsIdx === 1) {
                this.counter.correct++;
            } else {
                this.counter.wrong++;
            }
            this.nextTest();
        }
    }

    nextTest() {
        if (this.indexes.length === 0) {
            console.log(this.counter); // TODO
            let $bc = this.$button1.parentElement;
            this.$button1.remove();
            this.$button2.remove();
            $bc.innerHTML = `
            Training: ${this.counter.learn}<br/>
            Test: ${this.counter.correct} richtig; ${this.counter.wrong} falsch<br/>
            ${Math.round(100*this.counter.correct/(this.counter.correct+this.counter.wrong))}%
            `;
            return;
        }
        this.newPic();
    }

    newPic() {
        if (this.mode === 'learn') this.counter.learn++;
        if (this.indexes) this.currentClsIdx = this.indexes.shift();
        else this.currentClsIdx = Math.random() < 0.5 ? 0 : 1;
        this.$pic.innerHTML = '';
        this.$pic.appendChild(this.createPicture(this.currentClsIdx))
        if (this.mode === 'learn') {
            this.$label.textContent = this.cls[this.currentClsIdx];
        }
    }

    createPicture(cls) {
        var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute('viewBox', '0 0 100 100');
        svg.style.height="300px";
        svg.style.width="100%";
        svg.style.border = 'solid 1px black';
        for (let i = 0; i< 5; i++) {
            var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
            var c = Math.floor(Math.random()*100)+100;
            var x = Math.floor(Math.random()*100);
            var y = Math.floor(Math.random()*100);
            circle.setAttribute('fill', 'rgba('+(new Array(3)).fill(c).join(',')+',0.5)');
            circle.setAttribute('r', 30);
            circle.setAttribute('cx', x);
            circle.setAttribute('cy', y);
            svg.appendChild(circle);
        }
        if (cls === 0) {
            var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect");
            var c = Math.floor(Math.random()*100)+100;
            var x = Math.floor(Math.random()*(100-30));
            var y = Math.floor(Math.random()*(100-30));
            rect.setAttribute('fill', 'rgba('+(new Array(3)).fill(c).join(',')+',0.7)');
            rect.setAttribute('width', 30);
            rect.setAttribute('height', 30);
            rect.setAttribute('x', x);
            rect.setAttribute('y', y);
            svg.appendChild(rect);
        }
    return svg;
    }
}