<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>



<h3>Was ist das Problem?</h3>
<p>
    Wenn man am ersten Advent die erste Kerze anzündet, brennt diese am Vierten Advent
    bereits 3 Wochen lang und ist oftmals schon recht klein, wenn man den Adventskranz häufig
    an hat. Sinnvoller wäre es dann, wenn die erste Kerze länger wäre und nachfolgenden immer
    ein Stückchen kürzer. Die vierte Kerze ist dann die kürzeste, da sie auch nur ein paar Tage brennt.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'zweizuvier.png', 'caption'=>'Aus zwei langen Kerzen werden vier unterschiedliche.', 'class'=>'right-2']) ?>
<p>
    Ich finde diesen Brauch, an den dunkelsten Tagen immer eine Kerze mehr an zu zünden,
    sehr schön und stimmig. Das Problem kommt aber jetzt erst: Ich hatte keine 4 Kerzen
    erst recht keine 4 unterschiedlich großen.
    Ich hatte zwei dicke, lange, selbstgedrehte Kerzen.
</p>
<p>
    Wenn ich jetzt vier gleichgroße Kerzen haben wollte, hätte ich beide halbiert. Easy.
    Aber ich brauche es ja etwas komplizierter.
</p>
<p>
    Wie also sollte ich die zwei langen Kerzen teilen, sodass ich am Ende vier
    Kerzen mit gleichmäßigem Größenunterschied habe?
</p>


<?php $this->startComponent('hidden') ?>
<?= Template::component('media', ['path'=>$post, 'media'=>'ersterschnitt.png', 'caption'=>'Der erste Schnitt legt die größte und die kleinste Kerze fest.', 'class'=>'right-2']) ?>
<p>
    Wenn ich eine Kerze nicht in der Mitte teile, sondern etwas versetzt, bekomme ich automatisch eine kleine und eine größere Kerze.
    Der erste Schnitt ist eigentlich beliebig.
    Mit ihm lege ich die kleinste und die größte Kerze fest.
</p>
<br class="clear"/>
<?= Template::component('media', ['path'=>$post, 'media'=>'zweiterschnitt.png', 'caption'=>'Der zweite Schnitt erzeugt die zwei mittleren Kerze.', 'class'=>'right-2 clear']) ?>
<p>
    Da ich zwischen der größten und der kleinsten Kerze zwei weitere Größen brauche,
    muss der Abstand zwischen dein beiden in drei gleiche Abschnitte geteilt werden.
    Der Schnitt wird dann am unteren oder oberen Drittel gesetzt.
    Es ergeben sich automatisch zwei Stücke, die sich genau zwischen die kleinste
    und größte Kerze vom ersten Schnitt einreihen.
</p>
<aside class="clear">
    <h4>Mathematischer Lösungsweg</h4>
    `L`: die Länge der unrsprünglichen Kerzen.<br/>
    `K_1` bis `K_4` sollen die Längen der kleinen Kerzen von klein nach groß sein.<br/>
    `K_1`: die Länge der kleinsten Kerze – ergibt sich aus dem ersten Schnitt und ist beliebig wählbar mit `K_1 < L/2`.<br/>
    `K_4 = L - K_1` – die größte Kerze, ergibt sich ebenfalls aus dem ersten Schnitt.<br/>
    `K_2` und `K_3` müssen nun so gewählt werden, dass die Abstände zwischen allen Kerzen gleich sind:<br/>
    `K_2-K_1 = K_3-K_2 = K_4-K_3`<br/>
    `K_2 + K_3 = L` die mittleren Kerzen müssen zusammen auch wieder eine ganze Kerze ergeben.<br/>
    Ich starte hier (der Anfang fällt etwas vom Himmel, sorry):<br/>
    `K_4 - K_1 = K_4 + (K_3-K_3) + (K_2-K_2) - K_1`<br/>
    `= (K_4-K_3) + (K_3-K_2) + (K_2-K_1)`<br/>
    `= (K_2-K_1) + (K_2-K_1) + (K_2-K_1)`<br/>
    ` = 3(K_2-K_1)`<br/>
    demnach ist `K_2-K_1 = {K_4 - K_1}/3`<br/>
    damit ist `K_2 = K_2 + (K_1 - K_1) = (K_2 - K_1) + K_1 = {K_4 - K_1}/3 + K_1 = {L+K_1}/3`<br/>

    <br/>
    Alternative: <br/>
    `K_2 = {3 K_2} / 3` <small>(sorry, auch das fällt vom Himmel… aber hier habe ich nichts verändert.)</small><br/>
    `= {3K_2 - 3K_1 + 3K_1}/3` <small>(hinzufügen und abziehen von `3K_1`)</small><br/>
    `= {3(K_2-K_1)+3K_1}/3` <small>(Umgruppieren und 3 ausklammern)</small><br/>
    `= {(K_4-K_3) + (K_3-K_2) + (K_2-K_1) + 3K_1}/3` <small>(dreimal `K_2-K_1` teile ich auf, sodass jede Differenz einmal vorkommt, da diese ja gleich sein müssen)</small><br/>
    `= {K_4 + (-K_3+K_3) + (-K_2+K_2) - K_1 + 3K_1}/3` <small>(umgruppieren)</small><br/>
    `= {K_4 + K_1 + K_1}/3` <small>(jetzt verschwinden `K_3` und `K_2` und zweimal `K_1` bleibt)</small><br/>
    `= {L+K_1}/3` <small>(setzt `L` anstatt `K_4+K_1` als Gesamtlänge)</small><br/>
</aside>
<?php $this->endComponent('hidden') ?>

<?php

/*
# Wieviele Tage muss die Kerze im Schnitt brennen vom 4. Advent bis zum 24?
# im schnitt 4 tage
$durations = [];
$WDS = ['so', 'mo', 'di', 'mi', 'do', 'fr', 'sa'];
for ($y = 2124; $y>1700; $y--) {
    $HADateString = $y.'-12-24 00:00';
    $date = new DateTimeImmutable($HADateString);
    $wd = intval($date->format('w')); // 0=So
    //echo $date->format('d.m.Y'), ' ', $wd, ' ', $WDS[$wd], '<br/>';
    if ($wd === 0) {
        $lastSun = clone $date;
    } else {
        $lastSun = $date->modify('last sunday');
    }
    //echo $date->format('d.m.Y'), ' ', $wd, ' ', $WDS[$wd], '<br/>';
    //echo '4. Advent: ', $lastSun->format('d.m.Y'), ' ', intval($lastSun->format('w')), ' ', $WDS[intval($lastSun->format('w'))], '<br/>';
    $dur = $date->diff($lastSun)->days + 1;
    //echo $dur, '<br/>';
    $durations[] = $dur;
    //echo '<br/>';
}
echo array_sum($durations) / count($durations);
*/
?>
<p>
    Auch wenn man dann berechnen kann, wie groß die jeweiligen Kerzenstücke sein müssen,
    damit sie ein gleichmäßiges Quartett ergeben, so weiß man natürlich noch nicht,
    welche Größe die erste Kerze haben muss.
    Das hängt natürlich von der wöchentlichen Brenndauer ab.
</p>
<p>
    Eine Adventskranz-Kerze brennt z.B. pro Stunden `0,3cm`
    und am Tag vielleicht im Schnitt zwei Stunden (50 Stunden = 15cm → 0,3cm/h).

    Es sind im Schnitt vier Tage vom 4. Advent bis Heiligabend.
    Plus zwei Weihnachtsfeiertage: 6 Tage.
</p>
<p>
    Die größte Kerze: `(3*7+6) cdot 2 cdot 0,3cm = 16,2cm`.
    Die kleinste Kerze: `6 cdot 2 cdot 0,3cm = 3,6cm`.
    Dafür benötige ich also zwei ca. `20cm` lange Kerzen.

    

</p>