<?php

return [
    'show'=>true,
    'title'=>'Verschieden große Adventskranz-Kerzen',
    'date'=>'2024-12-01',
    'tags'=>['Mathematik', 'Alltag', 'Advent', 'Weihnachten', 'Kerzen', 'Real-World-Math'],
    'description'=>'Am Adventskranz gibt es in der Regel vier Kerzen, die im Abstand von einer Woche angezündet werden. Aus diesem Fakt und der Tatsache, dass ich nur zwei große Kerzen hatte, ergab sich eine Mathe-Aufgabe.',
    'thumbnail'=>'_a44f4e2b-a6ee-4713-a4ec-439a0615865c.jpeg'
];
