<?php

return [
    'show'=>true,
    'title'=>'Ein Rechenwerk mit PiiRi: Der Trick mit den negative Zahlen',
    'date'=>'2024-10-27',
    'tags'=>['Informatik', 'Schule', 'Schaltung', 'TechnischeInformatik', 'piiri', 'Rechenwerk'],
    'description'=>'Um mit dem Rechenwerk auch Subtraktionen durchführen zu können, muss es eine Möglichkeit geben, negative Zahlen binär darzustellen - dafür verwenden wir einen Trick.',
    'thumbnail'=>'_e66a77eb-ec31-4aa7-95e7-17a988934066.jpeg',
    'emblem'=>'https://kursbegleiter.de/piiri/img/logo.svg',
];