<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>


<p>
    PiiRi ist in einer Demo-Version verfügbar unter <a href="https://kursbegleiter.de/piiri" target="_blank">kursbegleiter.de/piiri</a>
</p>


<h3>Das Zweierkomplement: Die Grundlagen</h3>
<?= Template::component('media', ['path'=>$post, 'media'=>'folge6.mp4', 'caption'=>'Negative Binärzahlen', 'class'=>'center']) ?>
<p>
    Damit das Rechenwerk auch subtrahieren kann, muss es negative Zahlen verarbeiten können.

    Dafür verwendet man einen Trick:

    Wenn man eine Zahl und ihre Gegenzahl addiert, so muss immer `0` herauskommen: `4 + (-4) = 0`.
    Da im Computer aber immer mit einer festen Anzahl Stellen gearbeitet wird,
    genügt es, wenn nur diese Stellen Null sind und ein Übertrag an der obersten Stelle weg fällt.
</p>
<p>
    So ist zum Beispiel `0100 + 1100 = (1)0000`. Da aber nur 4 Bit verwendet werden, fällt die vorderste `1` weg.
    `0100` und `1100` sind in diesem Sinne also Gegenzahlen und zwar `4` und `-4`.
</p>
<p>
    Die negative Gegenzahl im Binärcode, das sogenannte Zweierkomplement erhält man,
    indem man in der Binärzahl zunächst alle Bits umdreht
    und dann `1` addiert.
</p>
<aside>
    Beispiel bei einer Verarbeitungsbreite von 4 Bits:<br/>
    `4_10 = 0100_2`.<br/>
    Alle Bits negiert: `1011`<br/>
    Eins addiert: `1100`<br/>
    `0100 + 1100 = (1)0000` - die führende Eins fällt als Übertrag weg.<br/>
    Also: `-4_10 = 1100_2`.<br/>
</aside>
<p>
    Das Tolle am Zweierkomplement ist, dass es auch für eine Rechnung wie `3-5=?` funktioniert.
    Die Drei ist im Binärsystem: `0011`.
    Fünf ist als Binärzahl: `0101`, das Zweierkomplement davon, also `-5_10`, ist `1011_2`.
    Diese zwei Zahlen addiert ergibt ergibt `0101_2 + 1011_2 = 1110_2`.
    Das ist eine negative Zahl, was man am obersten Bit erkennt.
    Wenn ich davon wiederum das Zweierkomplement bilde, kommt `0010_2`, also `2_10` heraus,
    das Ergebnis der Rechnung war also `-2_10`.
</p>


<h3>Das Zweierkomplement in PiiRi</h3>
<?= Template::component('media', ['path'=>$post, 'media'=>'folge7.mp4', 'caption'=>'Das Zweierkomplement mit PiiRi umsetzen.', 'class'=>'center']) ?>
<p>
    Du baust jetzt Komponenten, mit denen das Zweierkomplement einer Binärzahl erzeugt wird.
    Wir bauen diese Komponenten so, dass man sie mit einem Steuersignal aktivieren kann,
    ansonsten bleibt die Zahl unverändert.
    So wird der Addierer zum Subtrahierer, indem der zweite Summand bei Bedarf negiert wird.
</p>
<p>
    Dazu brauchen wir zunächst eine Komponente, die alle Bits invertiert.
    Diese braucht die 4-Bit-Zahl am Eingang und das Steuersignal,
    sowie eine 4-Bit-Zahl am Ausgang.
    Die Zahl müssen wir zunächst mit einem Adapter aufteilen.
    Du könntest jetzt jedes Bit mit einem not-Gatter invertieren.
    Da wir es aber an und aus schalten wollen, nehmen wir das xor-Gatter,
    denn dieses verhält sich so, wie ein schaltbares not-Gatter.
    Für den Ausgang werden die Bits wieder zusammengeführt.

    Über die Wertetabelle sehe ich, dass die Komponente arbeitet, wie sie soll.
</p>
<p>
    Um nun das Zweierkomplement zu berechnen,
    benötigst du wieder die 4-Bit-Zahl und das Steuersignal am Eingang und eine 4-Bit-Zahl am Ausgang.
    Zunächst geht die Zahl durch die Komponente von eben,
    wodurch alle Bits gegebenenfalls invertiert werden.
    Der Addierer addiert Eins, indem wir der Einfachheit halber das Steuersignal auf den Übertrags-Eingang legen.
</p>
<p>
    Ich teste die Komponente erstmal für sich.
    Dann verwendet ich den Addierer um eine Zahl und ihre Gegenzahl (das Zweierkomplement) zu addieren.
    Dabei sollte immer `0` herauskommen, wenn ich das Steuersignal aktiviere.
</p>


<aside>
    <h4>Wie kommt man auf das Zweierkomplement?</h4>
    <p>
        Das Besondere am Zweierkomplement ist, dass es ermöglicht, negative Zahlen
        darzustellen, ohne zusätzlich ein Vorzeichen-Bit speichern und berücksichtigen zu müssen.
    </p>
<p>
    Addiert man `+4` und `-4` so ergibt das `0`. Das ist die wesentliche Eigenschaft, die gegeben sein muss.

    Gebe ich dir also eine Binärzahl vor, kannst du versuchen eine Binärzahl zu finden, die dazu addiert `0` ergibt.
    Dann hast du die Gegenzahl gefunden.
</p>
<p>
    Das geht aber erstmal gar nicht. Aber wenn man sich klar macht, dass die Anzahl der verarbeiteten Stellen
    im Computer immer begrenzt sind, eröffnet sich eine Möglichkeit: Wir brauche nur in den verarbeiteten Stellen eine `0`.
    Eine `1` vorne im Übertrag fällt dann einfach weg, und es sieht dann so aus, als sei `0` herausgekommen.
    Das Ziel ist es also eine zahl mit einer führenden `1` und dann so viele Nullen wie verarbeitete Stellen da sind zu erhalten.
</p>
<p>
    Als Zwischenschritt versuche alle Stellen auf Eins zu bringen. 
    Wenn du dann am Ende eine Eins addierst, bekommst du das gewünschte Ergebnis:
    `1111 + 1 = 10000`.
</p>
<p>
    Um alle Bits auf `1` zu bringen, addierst du genau die Zahl, in der alle Bits umgedreht sind:
    `0110+1001=1111`.
</p>
<p>
    Jetzt wie eben angedeutet noch `1` addieren und alles wird zu `0` und der letzte Übertrag 
    wird aus der Zahl herausgeschoben und fällt weg:
    `1001+1=1010`.
</p>
<p>
    `6_10 = 0110_2`; `-6_10 = 1010_2`; `6-10 + (-6_10) = 0110_2 + 1010_2 = (1)0000_2 = 0`.
</p>
</aside>


