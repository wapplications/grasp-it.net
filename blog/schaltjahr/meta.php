<?php


// TODO mathmx scrollen

return [
    'show'=>true,
    'title'=>'Warum wir Schaltjahre brauchen – und es trotzdem nicht reicht',
    'date'=>'2024-07-07',
    'tags'=>['Mathematik','Physik', 'Astronomie', 'Kalender'],
    'description'=>'Die Schaltjahr-Regelung ist ein Beispiel für den beständigen Versuch der Menschen,
    feste Regeln und Strukturen in den Naturereignisse zu sehen.
    Warum brauchen wir die Schaltjahre und ist das Problem damit gelöst?
    Mit der Schaltjahr-Simulation kannst du verstehen, wie verschiedene Kalenderreformen der Vergangenheit versuchten,
    den Tagesrhythmus und den Lauf der Jahreszeiten in Einklang zu bringen.',
    'thumbnail'=>'erde.jpeg'
];