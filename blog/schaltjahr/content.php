<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>


<?= Template::component('media', ['path'=>$post, 'media'=>'app.png', 'caption'=>'Das Programm zur Visualisierung des Schaltjahr-Problems.', 'class'=>'right-2']) ?>

<p>
    Unser Alltag wird maßgeblich von drei Dingen bestimmt:
</p>
<ol>
    <li>Tag und Nacht</li>
    <li>Jahreszeiten</li>
    <li>Ob wir am 29. Februar Geburtstag haben oder nicht.</li>
</ol>

<p>Wie diese drei Dinge zusammenhängen, schauen wir uns jetzt an.</p>

<h2>Schaltjahr-Simulation</h2>
<p>
    Mit der <a href="https://kursbegleiter.de/schaltjahr/" target="_blank">Schaltjahr-Simulation</a> aus den Kursbegleiter-Tools 
    kannst du die Zusammenhänge sichtbar machen:
    In der Mitte ist die Sonne, der blaue Kreis ist die Erde.
    Die Linie nach Rechts ist die Jahresgrenze.
    Die kleine schwarze Linie auf der Erde macht die Drehung der Erde sichtbar.
</p>
<p>
    Du kannst mit den Knöpfen die Zeit in verschiedenen Schrittweiten vor und zurück schalten.
</p>

<hr class="clear" />


<!-- PAGEBREAK: Tag & Nacht und die Jahreszeiten -->

<?= Template::component('media', ['path'=>$post, 'media'=>'tag.gif', 'caption'=>'Der Tag wird durch die Drehung der Erde um die eigene Achse bestimmt.', 'class'=>'right-1']) ?>
<h2>Tag &amp; Nacht</h2>
<p>
    Tag und Nacht entstehen, indem die Erde sich um sich selbst dreht
    und wir dabei mal auf der sonnezugewandten Seite und mal im Dunkeln sind.
    Eine solche Umdrehung der Erde legt die Dauer eines Tages fest.
    Wir Menschen haben diese Dauer in 24 gleiche Einheiten eingeteilt: 24 Stunden.
</p>
<aside>
    <strong>Warum gerade 24?</strong> Eine wichtige Eigenschaft von Maßeinheiten: 
    man muss sie gut in kleinere Teile zerlegen können.
    Vor allem Hälfte, Viertel oder Drittel sind für uns wichtige Anteile, die wir uns gut vorstellen können.
    Es ist also kein Zufall, dass die 24 eine Zahl ist, die man ohne Rest durch zwei, drei und durch vier teilen kann.
    Mit der Hälfte kann man das Gleiche sogar nochmals machen.
</aside>

<hr class="clear" />

<?= Template::component('media', ['path'=>$post, 'media'=>'jahr2.gif', 'caption'=>'Das Jahr wird durch die Umdrehung der Erde um die Sonne bestimmt.', 'class'=>'right-1']) ?>
<h2>Jahreszeiten</h2>
<p>   
    Was du in der Simulation auch erkennen kannst:
    Pro Tag legt die Erde aber auch ein Stückchen ihres Weges um die Sonne herum zurück.
    Zusammen mit der Neigung der Erdachse entstehen dadurch unsere Jahreszeiten.
    Eine ganze Umdrehung der Erde um die Sonne legt die Dauer eines (astronomischen) Jahres fest.
</p>
<p>
    Nun ist es naheliegend, die Anzahl der Tage (Anzahl der Erddrehungen) zu zählen,
    bis die Erde einmal um die Sonne gereist ist.
    Das kannst du in der Simulation auch machen:
    Setze dafür die Zeit zurück und gehe dann Tag für Tag vorwärts und achte auf den Moment, an dem das Jahr zuende ist.
    Nach 365&nbsp;Tagen sind wir fast am Jahreswechsel angelangt. Jedefalls kurz davor.
</p>
<aside>
    <strong>Aber 365 Tage ist gar keine Zahl, die man in einfache Teile zerlegen kann!</strong>
    Ja, das ist richtig. 365 kann man nur durch 5 und 73 teilen. Aber diese Zahl haben sich eben
    nicht die Menschen ausgedacht, sondern sie ist eine Folge der natürlichen Ereignisse
    und des Zusammenspiels von Erddrehung und Umlaufbahn um die Sonne.<br/>
    Wir gliedern aber auch das Jahr in kleinerer Einheiten: Die 12 Monate!
    Auch wenn das ein Thema für sich ist: 12 ist wiedereinmal durch zwei, drei und vier teilbar.
</aside>

<hr class="clear" />

<!-- PAGEBREAK Weihnachten im Frühling? Wir haben ein Problem! -->

<?= Template::component('media', ['path'=>$post, 'media'=>'roemisch-passt-nicht.gif', 'caption'=>'Nach 365 Tagen ist die Erde noch nicht am Jahreswechsel angekommen. Es fehlen 6 Stunden.', 'class'=>'right-1']) ?>
<h2>Wir haben ein Problem</h2>
<p>
    Nach 365 Tagen fehlen noch ein paar Stunden bis zum Ende des astronomischen Jahres.
    Aber kein ganzer Tag mehr.
    Es geht einfach nicht auf!
    Das Kalender-Jahr ist etwa 6&nbsp;Stunden zu Kurz.
</p>
<p>
    Würde man pro Jahr genau 365&nbsp;Tage zählen, so sammelt sich über die Jahre ein Fehler an
    und der Kalender weicht immer weiter von den astronomischen Jahreszeiten ab.
    Nach etwa 400 Jahren läge Weihnachten im Frühling!
</p>

<hr class="clear"/>
<h2>Der Julianische Kalender</h2>
<p>
    Wir halten fest:
    Das astronomische Jahr (tropisches Jahr) ist etwa 6&nbsp;Stunden länger als 365&nbsp;Tage.
    6 Stunden sind ein Viertel eines 24-Stunden-Tages.
    Alle 4 Jahre würde das Kalender-Jahr also um einen weiteren Tag früher aufhören.
    Es liegt also nahe, alle 4 Jahre einen zusätzlichen Tag einzuführen: Das ist der <em>Schalttag</em>.
    Man legte fest, dass alle Jahre, deren Jahreszahl durch 4 teilbaren ist, einen solchen Schaltag bekommen sollen.
    So die Idee des Julianischen Kalenders.
    Das Kalender-Jahr des Julianischen Kalenders ist damit im Schnitt 365&nbsp;1/4 bzw. 365,25&nbsp;Tage lang.
</p>
<aside>
    <strong>Warum liegt der Schalttag Ende Februar und nicht etwa am Ende des Jahres?</strong>
    Im Römischen Reich lag der Jahresanfang zunächst am 1. März (woran die Monatsnamen September bis Dezember noch erinnern).
    Erst später wurde der 1.&nbsp;Januar als Jahresbeginn festgelegt. Das war damals alles ziemlich kompliziert…
</aside>

<hr class="clear"/>

<!-- PAGEBREAK Auch der Julianische Kalender ist zu ungenau -->



<?= Template::component('media', ['path'=>$post, 'media'=>'julianisch-passt-nicht.gif', 'caption'=>'Hier geht es immer 365,25 Tage weiter. Man sieht, dass das ein klein wenig zu viel ist.', 'class'=>'right-1']) ?>
<h2>Netter Versuch – nächster Versuch</h2>
<p>
    Diese erste Idee, alle 4 Jahre systematisch einen Tag als Korrektur einzuschieben, brachte Besserung.
    Auf diese Weise war aber das Kalender-Jahr etwas zu lang, was nach etwa hundert Jahren einen Tag ausmachte…
</p>
<aside>
    Tatsächlich sind diese Verschiebungen der Grund dafür, dass in manchen Kirchen am 7. Januar Weihnachten gefeiert wird.
</aside>
<p>
    An dieser Stelle schauen wir uns mal an, wie lange das Jahr nach heutigen Erkenntnissen theoretisch sein müsste:<br/>
    <strong>1&nbsp;Sonnen-Jahr&nbsp;=&nbsp;365,24219052&nbsp;Tage</strong>.
</p>
<p>
    Die Aufgabe der Astronomen damals war also, mit ihren Tricksereien so dicht wie möglich an diese Zahl heranzukommen.
</p>

<hr class="clear"/>


<!-- PAGEBREAK Wie sieht nun heute die Lösung aus? -->


<?= Template::component('media', ['path'=>$post, 'media'=>'gregorianisch-passt-nicht.gif', 'caption'=>'Hier geht es immer 365,2425 Tage weiter. Man sieht nur sehr wenig Abweichung.', 'class'=>'right-1']) ?>
<h2>Der Gregorianische Kalender</h2>
<aside>
    <strong>Zur Erinnerung:</strong><br/>
    `1 " Sonnen-Jahr" = 365,24219052 " Tage"`
</aside>
<p>
    Wir sehen, dass das Julianische Kalender-Jahr mit 365,25&nbsp;Tagen
    im Vergleich zum Sonnen-Jahr an der Hunderstel-Stelle um 1 zu groß ist.
    Würden wir also alle hundert Jahre den Schalttag wegfallen lassen,
    so kämen wir auf ein Kalender-Jahr von 365,24&nbsp;Tagen.
    Verglichen mit dem Sonnen-Jahr stimmen dann zumindest die ersten beiden Nachkommastellen.
</p>
<aside>
    <strong>Die Mathematik dahinter:</strong>
    `365 + 1/4 - 1/100 = 365 + 0,25 - 0,01 = 365,24`
</aside>
<p>
    Damit wäre das Kalender-Jahr aber wiederum gut 3&nbsp;Minuten (0,00219052&nbsp;Tage) zu kurz.
    Etwa alle 400&nbsp;Jahre würde das fast schon wieder einen Fehler von einem ganzen Tag ergeben.
    Die Idee ist also, alle 400&nbsp;Jahre den Schalttag doch wieder einzufügen.
</p>
<aside>
    <strong>Warum alle 400 Jahre?</strong><br/>
    <p>Der Fehler zum Sonnen-Jahr beträgt pro Jahr:</p>
    `365,24219052 - 365,24 = 0,00219052 " Tage"`
    <p>Wieviele Jahre müssen vergehen, um einen ganzen Tag zusammen zu bekommen?</p>
    `0,00219052 * "?" = 1`
    <br/>
    `1 : 0,00219052 ~~ 456 " Jahre"`
</aside>
<p>
    Alle 4&nbsp;Jahre einen Schalttag dazu,
    um diesen dann alle 100&nbsp;Jahre ausfallen und
    alle 400&nbsp;Jahre aber doch wieder stattfinden zu lassen:
    Das ist die Idee des Gregorianischen Kalenders.
    Das Gregorianische Kalender-Jahr ist damit im Schnitt 365,2425&nbsp;Tage lang.
</p>
<aside>
    <strong>Die Mathematik dahinter:</strong>
    `365 + 1/4 - 1/100 + 1/400 = 365 + 0,25 - 0,01 + 0,0025 = 365,2425`
</aside>
<?php
$currentYear = (int)date('Y');
$yearsSinceGreg = $currentYear-1582;
$daysErrorPerYear = abs(365.2425 - 365.24219052);
$secondsErrorPerYear = 24*60*60*$daysErrorPerYear;
$secondsErrorSinceGreg = $secondsErrorPerYear * $yearsSinceGreg;
$minutesErrorSinceGreg = $secondsErrorSinceGreg/60;
$hoursErrorSinceGreg = $minutesErrorSinceGreg/60;
?>
<p>
    Das ist natürlich dann wieder ein bisschen länger als das Sonnen-Jahr, nähmlich etwa 27&nbsp;Sekunden.
    Aber es ist immerhin so genau,
    dass ein Fehler von einem ganzen Tag erst nach etwa 3231&nbsp;Jahren zusammenkommt.
    Das wäre dann etwa in <?= 3231 - $yearsSinceGreg ?>&nbsp;Jahren der Fall. (Stand <?= $currentYear ?>).
    Aktuell beträgt die Abweichung <?= number_format($secondsErrorSinceGreg, 1, ',', '') ?>&nbsp;Sekunden.
    Das sind 
    <?= number_format($minutesErrorSinceGreg, 1, ',', '') ?>&nbsp;Minuten
    bzw. 
    <?= number_format($hoursErrorSinceGreg, 1, ',', '') ?>&nbsp;Stunden
</p>
<aside>
    <strong>Wie kommt man darauf?</strong><br/>
    <p>Der Fehler zum Sonnen-Jahr beträgt pro Jahr:</p>
    `365,24219052 - 365,2425 = -0,00030948 " Tage"`
    <p>Wieviele Jahre müssen vergehen, um einen ganzen Tag zusammen zu bekommen?</p>
    `0,00030948 * "?" = 1`
    <br/>
    `1 : 0,00030948 ~~ 3231 " Jahre"`
    <p>Wieviele Sekunden sind das pro Jahr?</p>
    `24 * 60 * 60 * 0,00030948 ~~ 26,7 " Sekunden"`
</aside>



