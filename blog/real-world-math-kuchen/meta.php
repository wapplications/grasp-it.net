<?php

return [
    'show'=>true,
    'title'=>'Kuchenstück halbieren',
    'date'=>'2024-02-20',
    'tags'=>['Mathematik','Alltag', 'Rechnen', 'Kuchen', 'Real-World-Math'],
    'description'=>'Wie man ein Kuchenstück der Länge nach halbieren muss, ist klar.
    Aber wenn man es quer teilen möchte, wo muss man schneiden, damit beide Teile gleich groß sind?
    Ein reales Mathe-Problem bei Kaffee und Kuchen.',
    'thumbnail'=>'thumb.jpeg'
];

