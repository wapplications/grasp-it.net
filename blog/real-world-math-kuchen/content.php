<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>





<?= Template::component('media', ['path'=>$post, 'media'=>'schneiden.png', 'caption'=>'Quer- und Längs-Schnitt', 'class'=>'right-1']) ?>
<p>
    Da sitzt man zu zweit gemütlich im Café und kann sich nicht recht entscheiden.
    Die Lösung: Man bestellt zwei Stück Kuchen und 
    jeder bekommt von beiden die Hälfte.
    Wer allerdings schon mal versucht hat, eine 3&nbsp;km hohe Sahnetorte zu halbieren, wird mir zustimmen:
    Vom Rand zur Spitze schneiden ist fast nicht durchführbar.
    Also quer schneinen. Aber wo schneidet man wenn der vordere Teil viel dünner, als hinten?
    Sicherlich nicht in der Mitte...
</p>

<h3 class="clear">Mathematische modellierung des Problems</h3>
<?= Template::component('media', ['path'=>$post, 'media'=>'skizze.png', 'caption'=>'Modellierung des Kuchenproblems', 'class'=>'right-1']) ?>
<p>
    Um dieses Problem zu vereinfachen, gehe ich davon aus, dass das Kuchenstückes gleichmäßig strukturiert ist
    und nicht etwa Erdbeeren nur im hinteren Teil sind oder ein Teigrand an der Basis unattraktiv ist…
    Zudem können wir sagen, dass ein Kuchenstück von oben gesehen ein Kreissektor ist.
    Daher werde ich nur eine Fläche betrachten.
    Ein Kuchen wird wird üblicherweise in 8, 12 oder 16 Stücke geteilt.
    Die letzte Vereinfachung ist, einen Kuchen mit Radius 1 zu betrachten.
    Würde man den Radius mit in die Rechnung nehmen, würde er sich ohnehin herauskürzen.
</p>


<h3 class="clear">Grundlegendes</h3>
<p>
    Ich suche also `s`, den Abstand vom Kuchenrand, an dem ich das Messer ansetzen muss.
    Das ist abhängig vom Winkel des Kuchenstückes, also von der Anzahl der Kuchenstücke.
</p>
<aside>
    <h4>Anzahl Kuchenstücke</h4>
    <p>`k in {8,12,16}`</p>

    <h4>Kuchenstück-Winkel (Zentralwinkel)</h4>
    <p>`alpha = frac{360°}{k}` bzw. um im Bogenmaß zu rechnen: `alpha = {2pi} / k`</p>
</aside>

<h3>Terme für die Flächen aufstellen</h3>
<p>
    Ich stelle einen Term für das ganze Kuchenstück, sowie für das abzuschneidende Stück.
    Der Einfachheit halber berechne ich das Stück an der Spitze, denn hierbei handelt es sich
    um ein gleichschenkliges Dreieck.
</p>
<aside>
    <h4>Ganzes Kuchenstück (Kreissektor)</h4>
    <p>`A_{"ganz"} = alpha / 2 = frac{pi}{k}`</p>

    <h4>Kuchenstück-Spitze (Dreieck)</h4>
    <p>`h = 1-s`</p>
    <p>`g = 2 * h * tan(frac{alpha}{2}) = 2 * h * tan(pi/k)`</p>
    <p>`A_{"Spitze"} = frac{1}{2} * g * h =  frac{1}{2} * 2 * h * tan(pi/k) * (1-s)
        = (1-s)^2 * tan(pi/k)
        `</p>
</aside>

<h3>Die Bedingung als Gleichung schreiben</h3>
<p>
    Damit die Dreiecksfläche am Ende die Hälfte der Gesamtfläche ist, formuliere ich diesen Zusammenhang in einer Gleichung.
    Diese Gleichung löse ich nach s, der gesuchten Schnittposition.
</p>
<aside>
    <h4>Die abgeschnittene Spitze muss die Hälfte vom ganzen Stück sein.</h4>
    <p>Oder auch: das Doppelte der Spitze muss die ganze Fläche ergeben:</p>
    <p>`2 * A_{"Spitze"} = A_{"ganz"}`</p>
    <p>`2 * (1-s)^2 * tan(pi/k) = frac{pi}{k}`</p>
    <p>`(1-s)^2 = frac{pi}{2*k*tan(pi/k)}`</p>
    <p>`1-s = sqrt{frac{pi}{2*k*tan(pi/k)}}`</p>
    <p>`s(k) = 1-sqrt{frac{pi}{2*k*tan(pi/k)}}`</p>
</aside>


<?= Template::component('media', ['path'=>$post, 'media'=>'ergebnis.png', 'caption'=>'Im Ergebnis könnte das etwa so ausehen', 'class'=>'right-1']) ?>
<h3>Konkretes Beispiel</h3>
<p>Ich berechne die Schnittposition für einen Kuchen mit 12 Stücken.</p>
<aside>
    <p>`k = 12`</p>
    <p>`s(12) = 1-sqrt{frac{pi}{2*12*tan(pi/12)}} ~~ 0,301`</p>
</aside>
<p>
    Der Schnitt muss also bei etwas weniger als einem Drittel vom Kuchenrand her gesetzt werden.
</p>
<h4>Für andere Kuchen-Teilungen</h4>
<p>Bei anderen üblichen Kuchenteilungen ist der Unterschied nur minimal:</p>

<table class="table">
<thead>
    <tr>
        <th>Teiler `k`</th>
        <th>Schnittposition `s(k)`</th>
    </tr>
</thead>
<tbody>
<?php
$f = fn ($k) => (1-sqrt(M_PI/(2*$k*tan(M_PI/$k))));
foreach([4,8,12,16,20,100] as $k) {
    echo '<tr>';
    echo '<td>`k='.$k.'`</td><td>`s_'.$k.' ~~ '.formatNumber($f($k), 4).'`</td>';
    echo '</tr>';
}
?>
</tbody>
</table>

<p>&nbsp;</p>

<aside>
<p>
    Was hier auffällt: die Werte scheinen sich einer Grenze anzunähern. Ich schaue mal, welche das ist...
</p>
<p>
    Es gilt offenbar: `lim_{k->oo} k*tan(pi/k) = pi`.
    Damit ist dann:<br/>
    `lim_{k->oo} s(k) = lim_{k->oo} (1-sqrt{frac{pi}{2*k*tan(pi/k)}}) = 1-sqrt{frac{cancel(pi)}{2*cancel(pi)}} = 1 - 1/sqrt(2) ~~ 0,2929`<br/>
</p>
</aside>

<h3>Alternative Überlegung 1 (ungenauer, ganzer Kreis)</h3>
<?= Template::component('media', ['path'=>$post, 'media'=>'kreisansatz.svg', 'caption'=>'Der ganze Kuchen', 'class'=>'right-1']) ?>
<p>
    Wenn man den ganzen Kuchen betrachtet, bilden alle diese Schnittkanten
    ein Vieleck – vielleicht annähernd einen Kreis.
    Die Fläche des inneren Kreises sollte dann die Hälfte des gesamten Kreises sein.<br/>
    `A_"groß" = pi * r^2`<br/>
    `A_"klein" = pi * {:r_2:} ^2`<br/>
    `2 * A_"klein" = A_"groß"`<br/>
    `2 * pi * {:r_2:} ^2 = pi * r^2`<br/>
    `r=1; r_2 = 1-s`<br/>
    `2 * cancel(pi) * (1-s)^2 = cancel(pi)`<br/>
    `(1-s)^2 = 1/2`<br/>
    `1-s = sqrt(1/2)`<br/>
    `s = 1-sqrt(1/2) ~~ 0,2929`<br/>
    Das ist interessanterweise genau der Grenzwert von oben.
</p>

<h3>Alternative Überlegung 2 (ungenau, Dreiecksmodell)</h3>
<?= Template::component('media', ['path'=>$post, 'media'=>'dreieckansatz.svg', 'caption'=>'Vereinfachung als Dreieck', 'class'=>'right-1']) ?>
<p>
    Wenn man das Kuchenstück vereinfacht als ein Dreieck betrachtet, kann man das Problem
    unter Anderem mit dem Strahlensatz lösen.<br/>
    Wir setzen den Radius des Kuchens (die Höhe des Dreiecks) gleich auf `1`.    
</p>
<p>
    Über den Strahlensatz ermittle ich die Grundseite des kleinen Dreiecks, in Abhängigkeit vom großen Dreieck und der Schnittposition:<br/> 
    `g_1 / 1 = g_2 / {1-s}` d.h. `g_2 = g_1*(1-s)`<br/>
    <br/>
    Ich stelle Formeln für die beiden Dreiecksflächen auf und baue dabei den Zusammenhang zwischen `g_1` und `g_2` ein:<br/>
    `A_"ganz" = 1/2*g_1`<br/>
    `A_"Spitze" = 1/2*g_2*(1-s) = 1/2* g_1*(1-s) * (1-s) = 1/2 * g_1*(1-s)^2`<br/>
    <br/>
    Das gewünschte Verhältnis der Flächen als Gleichung:<br/>
    `2 * A_"Spitze" = A_"ganz"`<br/>
    `cancel(2) * (1/cancel(2) * cancel(g_1)*(1-s)^2) =  1/2*cancel(g_1)`<br/>
    `(1-s)^2 = 1/2`<br/>
    `1-s = 1/sqrt(2)`<br/>
    `s = 1 - 1/sqrt(2) ~~ 0,2929`<br/>
    Es kommt also exakt der gleiche Wert heraus, wie bei der Überlegung mit dem ganzen Kreis.

</p>

