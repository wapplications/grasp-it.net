<?php

return [
    'show'=>true,
    'title'=>'Wieviele Tabletten werden benötigt?',
    'date'=>'2024-08-24',
    'tags'=>['Mathematik', 'Alltag', 'Katzen', 'Real-World-Math'],
    'description'=>'Unsere vier Kater müssen einen ganzen Monat lang ein Antibiotikum bekomen.
    In der Tierarztpraxis wurde berechnet, wieviele Tabletten benötigt werden, abhängig von unterschiedlichen Dosierungen,
    je nach Körpergewicht und Phase der Therapie. Ein Real-World-Scenario, bei dem es nicht schlecht ist, wenn man etwas mitdenkt!',
    'thumbnail'=>'thumb.jpeg'
];