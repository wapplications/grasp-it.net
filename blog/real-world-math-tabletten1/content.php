<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>




<p>
    Manch andere RWPs sind ein bisschen speziell, weil es oft Dinge sind, die ich mich frage,
    die aber andere vielleicht für unwichtige Spielerei halten.
    Diesmal ist das Real-World-Problem mathematisch nicht besonders anspruchsvoll,
    dafür aber ein wirklich realistisches. 
</p>
<p>
    Das sind die Infos:
</p>
<ul>
    <li>Kater 1: bekommt die Medis schon zwei Wochen, braucht sie also noch für weitere 2 Wochen. Tagesdosis: 1¼ Tabletten.</li>
    <li>Kater 2: für 4 Wochen. Tagesdosis: 1¼ Tabletten.</li>
    <li>Kater 3: für 4 Wochen. Tagesdosis: 1¼ Tabletten.</li>
    <li>Kater 4: für 4 Wochen. Tagesdosis: 1 Tablette.</li>
</ul>



<?php $this->startComponent('hidden') ?>
        <p>Es gibt viele mögliche Wege, um hier zum Ziel zu kommen. Zum Beispiel diesen:</p>
        <p>
            Für Kater 2 bis 4: <br/>
            `1frac{1}{4} + 1frac{1}{4} + 1 = 1,25+1,25+1 = 3,5`  Tabletten/Tag<br/>
            `4*7*3,5 = 98` Tabletten für 4 Wochen.<br/>
            Für Kater 1: <br/>
            `2*7*1frac{1}{4} = 14*1,25 = 17,5` Tabletten für 2 Wochen.<br/>
            Zusammen: `98+17,5 = 115,5`
        </p>
        <p>Alternativer Rechenweg:</p>
        <p>
            Kater 1: `2*7*1,25 = 17,5` Tabletten<br/>
            Kater 2: `4*7*1,25 = 35`<br/>
            Kater 3: `4*7*1,25 = 35`<br/>
            Kater 4: `4*7*1 = 28`<br/>
            Zusammen: `17,5 + 35 + 35 + 28 = 115,5`
        </p>
<?php $this->endComponent('hidden') ?>





