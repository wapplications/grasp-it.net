<?php

return [
    'title'=>'KI IV: Ein künstliches Gehirn zur Smiley-Erkennung trainieren',
    'date'=>'2024-03-17',
    'tags'=>['KI','Informatik','Gesellschaft'],
    'description'=>'
    Bei den Affen waren es wir Menschen, die erkannt haben, welche Merkmale ausschlaggebend sind für eine
    richtige Entscheidung. Ein KI-System muss während des Trainings erst »herausbekommen« worauf es ankommt.
    Wir verwenden Trainings- und Test-Daten um ein künstliches Gehirn das Erkennen von Smileys beizubringen.',
    'thumbnail'=>''
];

// bis hin zu Erkenntis, dass mehr nicht unbedingt Besser ist, wenn wir ein künstliches Gehirn bauen.