<?php

return [
    'show'=>true,
    'title'=>'Geschichte des Computers und des Internets',
    'date'=>'2024-09-26',
    'tags'=>['Informatik','Schule', 'Digitalisierung', 'Computer', 'Geschichte'],
    'description'=>'',
    'thumbnail'=>''
];