<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>

<?php

$cats = [
    'gaming'=>[
        'title'=>'Computerspiele & Spielkonsolen',
        'icon'=>'',
        'color'=>'#dd9900',
        // https://de.wikipedia.org/wiki/Spielkonsole
        // https://de.wikipedia.org/wiki/Geschichte_der_Videospiele
    ],
    'mainframe'=>[
        'title'=>'Großrechner',
        'icon'=>'',
        'color'=>'#cc2222',
    ],
    'pc'=>[
        'title'=>'Heimcomuter & PC',
        'icon'=>'',
        'color'=>'#aa00cc',
    ],
    'portable'=>[
        'title'=>'Tragbare Geräte',
        'icon'=>'',
        'color'=>'#00aa00',
    ],
    'online'=>[
        'title'=>'Online-Dienste',
        'icon'=>'',
        'color'=>'#0000ff',
    ],
    'perifer'=>[
        'title'=>'Peripherie-Geräte',
        'icon'=>'',
        'color'=>'#333333',
    ],
];


$entries = [
    '1972' => [
        'title'=>'Erste Spielekonsolen',
        'text'=>'Spielekonsolen (Telespiele) zum Anschluss an den Fernseher, festvertratete Spielvarianten, keine austauschbaren Spielmodule. Kein Computer, kein Programm, kein Microprozessor.',
        'image'=>'Magnavox-Odyssey-Console-Set.jpg',
        'cats'=>['gaming'],
        'cc'=>'Wikipedia User EXSHOW'
    ],
    '1975' => [
        'title'=>'Atari Home Pong',
        'text'=>'',
        'image'=>'AtariPong.png',
        'cats'=>['gaming'],
    ],

    '1976' => [
        'title'=>'8-Bit-Spiele-Konsolen',
        'text'=>'Das waren technisch geshen schon richtige Computer mit 8-Bit-Prozessoren, 2D-Grafik, wenig Farben',
        'image'=>'',
        'cats'=>['gaming'],
    ],
    '1983' => [
        'title'=>'Video-Game-Crash',
        'text'=>'Im Jahr 1983 brach der Videospiele-Markt ein, die Lücke in der Spielkonsolen-Geschichte wurde durch Heimcomputer gefüllt.',
        'image'=>'',
        'cats'=>['gaming'],
    ],
    '1989' => [
        'title'=>'Sega-Mega-Drive',
        'text'=>'16-Bit-Spiele-Konsolen, Umfangreiche 2D-Grafik, rudimentäre 3D-Grafikfähigkeiten, CD als Speichermedium',
        'image'=>'Sega-Mega-Drive-EU-Mk1-wController-FL.jpg',
        'cats'=>['gaming'],
    ],

    // 1977 	Das Video Computer System (VCS) von Atari wird am 11. September in den USA veröffentlicht.
    // 1978 	Der Spielautomat Space Invaders von Taito wird weltweit erfolgreich vertrieben. 
    // 1978 	Roy Trubshaw entwickelt das erste Online-Spiel an der Universität Essex. 


    '1993' => [
        'title'=>'3D-Grafik',
        'text'=>'z.B. Doom',
        'image'=>'',
        'cats'=>['gaming'],
    ],
    '1962' => ['title' => 'Computerspiel: Spacewar!', 'text' => '', 'image' => '', 'cats' => ['gaming']],
    '1979' => ['title' => 'Computerspiel: Star Raiders', 'text' => '', 'image' => '', 'cats' => ['gaming']],
    '1980' => ['title' => 'Computerspiel: Zork', 'text' => '', 'image' => '', 'cats' => ['gaming']],
    '1985' => ['title' => 'Computerspiel: Tetris', 'text' => '', 'image' => '', 'cats' => ['gaming']],
    '1989' => ['title' => 'Computerspiel: SimCity', 'text' => '', 'image' => '', 'cats' => ['gaming']],
    '1990' => ['title' => 'Computerspiel: Super Mario Bros. 3', 'text' => '', 'image' => '', 'cats' => ['gaming']],
    '1996' => ['title' => 'Computerspiel: Civilization II', 'text' => '', 'image' => '', 'cats' => ['gaming']],
    '1993' => ['title' => 'Computerspiel: Doom', 'text' => '', 'image' => '', 'cats' => ['gaming']],
    '1994' => ['title' => 'Computerspiel: Warcraft-Spielereihe', 'text' => '', 'image' => '', 'cats' => ['gaming']],
    '1994' => ['title' => 'Computerspiel: Sensible World of Soccer', 'text' => '', 'image' => '', 'cats' => ['gaming']],


    # PC
    '1976' => [
        'title' => 'Der Apple I wird erfolgreich eingeführt.',
        'text' => 'Der Apple I war ein von Stephen Wozniak entwickelter Personal Computer (PC) des US-amerikanischen Unternehmens Apple. Als erstes Gerät der Welt war er mit 666 US-Dollar für Privathaushalte erschwinglich und zugleich von Haus aus mit allen benötigten Anschlüssen ausgestattet, um ihn auf moderne Weise per Tastatur und Monitor zu bedienen (statt der bislang üblichen Kippschalter und Lämpchen der damaligen Rechner des unteren Preissegments). Aus diesem Grund wird er auch als erster PC der Welt bezeichnet.',
        'image' => 'Apple_I_2_-_CleanBackround_-_compact-CC-BY-SA.jpg',
        'cats' => ['pc'], 
        'cc'=>'<a href="/wiki/User:Narnars0" title="User:Narnars0">Narnars0</a> (photo) and <a href="/wiki/User:NeonZero" title="User:NeonZero">NeonZero</a> (designer of compact version)'
    ], 
    '1977' => [
        'title' => 'Der Apple II kommt auf den Markt.',
        'text' => 'Der Apple II (auch Apple ][ oder Apple //) des Unternehmens Apple Computer gehört zu den ersten 8-Bit-Mikrocomputern, die eine weite Verbreitung fanden. Zusammen mit dem Vorgängermodell, dem Apple I, ist er der bislang letzte in Serie hergestellte Computer, der von einer einzelnen Person, nämlich Steve Wozniak, entworfen wurde.',
        'image' => 'Apple_II-IMG_7067-CC-BY-SA.jpg',
        'cats' => ['pc'],
        'cc'=>'<a href="//commons.wikimedia.org/wiki/User:Rama" title="User:Rama">Rama</a> &amp; Musée Bolo'
    ], 
    '1977' => [
        'title' => 'Commodore präsentiert den programmierbaren Personal Electronic Transactor (PET).',
        'text' => 'Der Commodore PET 2001 (Personal Electronic Transactor; deutsch etwa persönlicher elektronischer Handlungsbeauftragter) wurde bei der Winter CES 1977 im Januar 1977 vorgestellt und verkauft. Er ist damit der weltweit erste für Privathaushalte erschwingliche und in Serie hergestellte persönliche Computer (PC),[1] da dem Apple II aufgrund von Abschirmungsproblemen die FCC-Freigabe bis Ende 1977 fehlte.[2] Er ist ebenso der weltweit erste PC in kompletter, betriebsbereiter Ausführung.',
        'image' => 'Commodore_2001_Series-IMG_0448b-CC-BY-SA.jpg',
        'cats' => ['pc'],
        'cc'=>'Photograph by <a href="//commons.wikimedia.org/wiki/User:Rama" title="User:Rama">Rama</a>'
    ], 
    '1981' => [
        'title' => 'IBM bringt den ersten PC auf den Markt.',
        'text' => 'Der IBM Personal Computer, Modell 5150, war 1981 das Urmodell der heutigen IBM-kompatiblen PCs. 16-Bit, Das Unternehmen setzte damit einen informellen, weltweiten Industriestandard und definierte die bis heute aktuelle Geräteklasse der IBM-kompatiblen Personal Computer.',
        'image' => 'IBM_PC_5150-CC-BY-SA.jpg',
        'cats' => ['pc'],
        'cc'=>'<a href="/wiki/User:Boffy_b" title="User:Boffy b">Boffy b</a>'
    ], 
    // 1982 	Commodore bringt den C64 auf den Markt. 
    // 1987 	Commodore veröffentlicht den Amiga 500. 


    
    


    // Templ
    '' => [
        'title' => '',
        'text' => '',
        'image' => '',
        'cats' => ['pc'],
        'cc'=>''
    ], 


     

];

?>

<?php foreach ($cats as $k=>$cat) : ?>
<div style="background-color:<?= $cat['color'] ?>; color:white; padding:0.5em; margin:1em; border-radius:0.5em;">
    <?= htmlspecialchars($cat['title']) ?>
</div>

<?php endforeach; ?>