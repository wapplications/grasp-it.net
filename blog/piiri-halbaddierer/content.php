<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>



<p>
    PiiRi ist in einer Demo-Version verfügbar unter <a href="https://kursbegleiter.de/piiri" target="_blank">kursbegleiter.de/piiri</a>
</p>
<?= Template::component('media', ['path'=>$post, 'media'=>'cast1folge3.mp4', 'caption'=>'Mit PiiRi einen Halb-Addierer bauen', 'class'=>'center']) ?>

<p>
    Die Grundidee beim Rechnen mit `0` und `1` ist, dass man es auf logische Verknüpfungen zurückführen kann
    nach dem Prinzip <em>»Wenn das und das aber nicht das, dann das…«</em>.
    Es wird also genau genommen gar nicht gerechnet, sondern nur alle möglichen Fälle
    durch Verschaltung von Signalen abgebildet.
    Um ein Rechenwerk zu bauen überlegst du dir zuerst, wie man zwei Bit addiert:
</p>
<aside>
`0+0=00`<br/>
`0+1=01`<br/>
`1+0=01`<br/>
`1+1=10`<br/>
    <br/>
    `1+1=2` als Binärzahl also `10`. 
    Man auch kann sagen Null mit Übertrag Eins.
    Ich habe bei den anderen Ergebnissen den Übertrag Null ergänzt, damit alles gleichförmig aussieht.
</aside>

<p>
    Du kannst nun diese Tabelle betrachten und eine Schaltung aufbauen, die sich genau so verhält.
    Dem ersten Summanden-Bit geben ich den Namen `a`.
    Dem zweiten Summanden-Bit den Namen `b`.
    Die Summe heißt `s` und der Übertrag soll `ddot u` sein.
</p>
<table class="table vline-2">
    <thead>
        <tr><th>`a`</th><th>`b`</th><th>`ddot u`</th><th>`s`</th></tr>
    </thead>
    <tbody>
    <tr><td>0</td><td>0</td><td>0</td><td>0</td></tr>
    <tr><td>0</td><td>1</td><td>0</td><td>1</td></tr>
    <tr><td>1</td><td>0</td><td>0</td><td>1</td></tr>
    <tr><td>1</td><td>1</td><td>1</td><td>0</td></tr>
    </tbody>
</table>
<p>
    Schau dir jetzt an, wie der Ausgang `s` aus den Eingängen `a` und `b` ergibt.
    Falls du schon etwas geübt bist, kannst du erkennen, dass dies genau die xor-Funktion ist.
    Die xor-Funktion verhält sich genau so, wie man es für das Summieren zweier Bits benötigt.
    Es bleibt nur noch, den Übertrag zu ermitteln:
    Vielleicht erkennst du, dass es einen Übertrag genau dann gibt, wenn beide Eingänge `1` sind.
    Das bekommen wir mit dem logischen »Und«.
    Am Ende überprüfe ich die Schaltung mit der automatisch erzeugten Wertetabelle.
</p>
<p>
    Damit du diese Schaltung im Weiteren wiederverwenden kannst, spreichere sie
    unter »ha« für Halb-Addierer ab.
</p>


