<?php

return [
    'show'=>true,
    'title'=>'Ein Rechenwerk mit PiiRi: Wie addiert man zwei Bit mit einem Halb-Addierer?',
    'date'=>'2024-10-17',
    'tags'=>['Informatik', 'Schule', 'Schaltung', 'TechnischeInformatik', 'piiri', 'Rechenwerk'],
    'description'=>'Der erste Schritt auf dem Weg zu einem Addierwerk ist der sogenannte Halbaddierer: zwei Bits werden addiert.',
    'thumbnail'=>'_416d8f55-d63b-4b62-92a6-836226a821d7.jpeg',
    'emblem'=>'https://kursbegleiter.de/piiri/img/logo.svg',
];