<?php

return [
    'show'=>true,
    'title'=>'KI-Spielwiese SmileKI: Die KI lernt Binärzahlen',
    'date'=>'2024-08-27',
    'tags'=>['Informatik', 'Schule', 'KI', 'SmileKI', 'NeuronalesNetz', 'Binärzahlen'],
    'description'=>'Mit der KI-Spielwiese SmileKI bringe ich einem künstlichen neuronalen Netz bei, Binärzahlen in Dezimalzahlen umzuwandeln.',
    'thumbnail'=>'_4dc39512-7c09-47b0-ba5c-011d5d551f92.jpeg'
];