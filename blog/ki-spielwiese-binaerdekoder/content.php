<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>

<p>
    Dieser Artikel bezieht sich auf das Tool
    <a href="https://kursbegleiter.de/smileki/" target="_blank">SmileKI</a>
    aus der KursBegleiter-Tool-Sammlung.
    Damit kannst du ein kleines neuronales Netz bauen, es mit verschiedenen
    Input-Arten ansteuern und das Ergebnis mit unterschiedlichen Output-Modulen
    anzeigen lassen.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'uebersicht.png', 'caption'=>'Die Oberfläche des KI-Tools SmileKI.', 'class'=>'center', 'c'=>'']) ?>


<h3>Binärzahl in Dezimalzahl umwandeln</h3>
<p>
    Das Modell soll erkennen um welche Zahl es sich bei einer dreistelligen Binärzahl handelt.
    Es soll also den Input entsprechend klassifizieren.
</p>


<?= Template::component('media', ['path'=>$post, 'media'=>'input.png', 'caption'=>'Input-Modul eingestellt', 'class'=>'right-2', 'c'=>'']) ?>
<h4>Schritt 1: Input einstellen</h4>
<p>
    Als Eingang verwende ich ein 3 Pixel breites Bitmap.
    Ich wähle dazu das Input-Modul <strong>S/W-Bitmap</strong>,
    setze die Input-Größe auf 3 und über den Stift die Bitmap-Breite ebenfalls auf 3.
    Achtung: Schwarz bedeutet hier 0, weiß 1.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'output.png', 'caption'=>'Output-Modul eingestellt', 'class'=>'right-2 clear', 'c'=>'']) ?>
<h4>Schritt 2: Output einstellen</h4>
<p>
    Da ich die Eingangsdaten klassifizieren möchte,
    wähle ich das Output-Modul <strong>Klassifikation Prozent</strong>
    und stelle die Output-Größe auf 8, denn eine dreistellige Binärzahl
    kann acht verschiedene Werte haben: 0 bis 7.
    Die Klassen werden automatisch mit 0 bis 7 beschriftet,
    ich kann aber über den Stift auch eigene Beschriftungen angeben.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'net.png', 'caption'=>'Das neuronale Netz', 'class'=>'right-2 clear', 'c'=>'']) ?>
<h4>Schritt 3: Neuronales Netz einrichten</h4>
<p>
    Ich belasse die Einstellungen hier zunächst so wie sie sind
    mit einer versteckten Schicht aus zwei Neuronen.
</p>

<h4>Schritt 4: Trainings-Daten aufzeichnen</h4>
<p>
    Ich wechsle auf der rechten Seite auf den Tab <strong>Daten</strong>.
    Hier ist eine leere Liste von Trainingsdaten zu sehen,
    die ich jetzt füllen werde.
</p>
<p>
    Ich stelle im Input-Bereich nacheinander jede Binärzahl ein
    und wähle im Output-Bereich das entsprechende Label aus.
    Für jeden Datensatz klicke ich <strong>+</strong>,
    um ihn der Liste hinzuzufügen.
</p>
<?php $this->startComponent('hidden', ['title'=>'Die Trainings-Daten', 'command'=>'ansehen']); ?>
<table class="table">
    <tr>
        <th>Input</th>
        <th>Ouput</th>
    </tr>
    <tr><td class="right">[0,0,0]</td><td class="right">[1,0,0,0,0,0,0,0]</td></tr>
    <tr><td class="right">[0,0,1]</td><td class="right">[0,1,0,0,0,0,0,0]</td></tr>
    <tr><td class="right">[0,1,0]</td><td class="right">[0,0,1,0,0,0,0,0]</td></tr>
    <tr><td class="right">[0,1,1]</td><td class="right">[0,0,0,1,0,0,0,0]</td></tr>
    <tr><td class="right">[1,0,0]</td><td class="right">[0,0,0,0,1,0,0,0]</td></tr>
    <tr><td class="right">[1,0,1]</td><td class="right">[0,0,0,0,0,1,0,0]</td></tr>
    <tr><td class="right">[1,1,0]</td><td class="right">[0,0,0,0,0,0,1,0]</td></tr>
    <tr><td class="right">[1,1,1]</td><td class="right">[0,0,0,0,0,0,0,1]</td></tr>
</table>
<?php $this->endComponent('hidden') ?>

<h4>Schritt 5: Test-Daten</h4>
<p>
    In diesem Beispiel gibt es nur die Trainings-Daten.
    Es gibt keine anderen Daten, die darüber hinaus gehen würden.
    Ich kopiere daher die Trainings-Daten in den Bereich der Test-Daten.
</p>


<h4>Schritt 6: Das Modell trainieren</h4>
<?= Template::component('media', ['path'=>$post, 'media'=>'training.gif', 'caption'=>'Der Verlauf des Training: Der Fehler wird nicht klein genug.', 'class'=>'right-2', 'c'=>'']) ?>
<p>
    Ich wechsle wieder auf den Tab <strong>neuronales Netz</strong>.
    Dann klicke ich auf <strong>Trainieren</strong>.
    Ich setze Wiederholungen auf 1000 und belasse Lern-Rate und Fehlergrenze.
    Dann starte ich das Lernen.
</p>
<p>
    Ich kann beobachten, wie der Fehler i.d.R. erst schnell runter geht
    und dann immer langsamer.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'testing.png', 'caption'=>'Das  Ergebnis des Testings', 'class'=>'right-1 clear', 'c'=>'']) ?>
<h4>Schritt 7: Das Modell testen</h4>
<p>
    Ich kann nun über das Bitmap selbst testen, ob das Modell
    richtige Ergebnisse liefert.
    Oder ich klicke <strong>Testing</strong> und lasse 
    automatisch alle Test-Fälle durchlaufen.
    Ich bekomme dann eine Zusammenfassung, wie gut das Modell
    gearbeitet hat.
</p>

<h4>Schritt 8: Optimieren</h4>
<p>
    Es kann sein, dass der Fehler irgendwie nicht mehr richtig runter geht
    und einige Zahlen einfach nicht richtig erkeannt werden.
    Dann kann es sein, dass man in der verdeckten Schicht mal ein
    Neuron mehr ausprobieren muss.
    Dazu klicke ich oberhalb vom Graphen die mittlere Schicht an
    und stelle 3 Konten ein. Danach muss ich den Trainigsvorgang erneut durchführen.
    Eventuell konvergiert das Netz jetzt besser.
</p>
<p>
    Hier sieht man, dass der richtige Aufbau eines neuronalen Netzes
    ausschlaggebend sein kann für den Lern-Erfolg.
</p>

<hr class="clear"/>

<h3>Zusammenfassung</h3>
<p>
    Hier hat die KI die Eingangssignale klassifizieren gelernt.
    Ein berechtigter Einwand bei diesem Beispiel ist:
    Ein System könnte die 8 Zustände problemlos auswendig lernen.
    Und tatsächlich: Nichts anderes hat das neuronale Netz hier vermutlich auch getan,
    denn ich habe alle möglichen Eingaben für das Training vorgegeben.
</p>
<p>
    Wenn ein KI-System genau die Eingabe-Ausgabe-Kombinationen »auswendig« lernt,
    handelt es sich im Allgemeinen um ein sogenanntes Overfitting, wodurch
    das System nur noch genau für die gelernten Eingaben richtige Ausgaben produziert.
</p>
<p>
    Als Experiment könnte man versuchen, ein paar der Trainingsdaten zu entfernen
    um zu sehen, ob das System in der Lage sein wird, die Lücken selbst zu füllen…
</p>

<h4>Das ganze Experiment zum Importieren in <a href="https://kursbegleiter.de/smileki/" target="_blank">SmileKI</a></h4>
<textarea readonly style="width:100%; box-sizing:border-box; border-radius:0.2em; background-color:#888888; height:10em;">
{"id":null,"input":{"mode":"bitmap","editable":true,"layer":{"activation":"LOGISTIC","size":3},"config":{"width":3}},"output":{"mode":"percentage","editable":true,"layer":{"activation":"LOGISTIC","size":8},"config":{"names":[{"name":"0","title":"0","icon":""},{"name":"1","title":"1","icon":""},{"name":"2","title":"2","icon":""},{"name":"3","title":"3","icon":""},{"name":"4","title":"4","icon":""},{"name":"5","title":"5","icon":""},{"name":"6","title":"6","icon":""},{"name":"7","title":"7","icon":""}]}},"hiddenLayers":{"layers":[{"size":3,"activation":"LOGISTIC"}],"editable":true},"training":[{"input":[0,0,0],"output":[1,0,0,0,0,0,0,0]},{"input":[0,0,1],"output":[0,1,0,0,0,0,0,0]},{"input":[0,1,0],"output":[0,0,1,0,0,0,0,0]},{"input":[0,1,1],"output":[0,0,0,1,0,0,0,0]},{"input":[1,0,0],"output":[0,0,0,0,1,0,0,0]},{"input":[1,0,1],"output":[0,0,0,0,0,1,0,0]},{"input":[1,1,0],"output":[0,0,0,0,0,0,1,0]},{"input":[1,1,1],"output":[0,0,0,0,0,0,0,1]}],"testing":[{"input":[0,0,0],"output":[1,0,0,0,0,0,0,0]},{"input":[0,0,1],"output":[0,1,0,0,0,0,0,0]},{"input":[0,1,0],"output":[0,0,1,0,0,0,0,0]},{"input":[0,1,1],"output":[0,0,0,1,0,0,0,0]},{"input":[1,0,0],"output":[0,0,0,0,1,0,0,0]},{"input":[1,0,1],"output":[0,0,0,0,0,1,0,0]},{"input":[1,1,0],"output":[0,0,0,0,0,0,1,0]},{"input":[1,1,1],"output":[0,0,0,0,0,0,0,1]}]}
</textarea>

<?php

/*

[
{"input":[0,0,0],"output":[1,0,0,0,0,0,0,0]},
{"input":[0,0,1],"output":[0,1,0,0,0,0,0,0]},
{"input":[0,1,0],"output":[0,0,1,0,0,0,0,0]},
{"input":[0,1,1],"output":[0,0,0,1,0,0,0,0]},
{"input":[1,0,0],"output":[0,0,0,0,1,0,0,0]},
{"input":[1,0,1],"output":[0,0,0,0,0,1,0,0]},
{"input":[1,1,0],"output":[0,0,0,0,0,0,1,0]},
{"input":[1,1,1],"output":[0,0,0,0,0,0,0,1]},
]

*/





?>