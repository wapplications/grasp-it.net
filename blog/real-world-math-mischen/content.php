<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>




<p>
    Ich war unterwegs und wollte mir ein Milchmischgetränk gönnen.
    Die fertigen Drinks sind mir aber zu süß.
    Mein Plan war also, einen Milchdrink mit Haselnussgeschmack zu kaufen,
    ihn aber mit einer ganzen Flasche Milch zu mischen.
    So hätte ich eine riesige Menge Milchshake mit zartem Geschmack…
</p>
<p>
    Das Mischen war nur nicht so einfach, ohne ein drittes Gefäß.
    Ich dachte mir: wenn ich ein paar Schluck von der Milch wegtrinken,
    dann habe ich etwas Platz, um eine Kleine Menge immer hin und her zu schütten.
    Wenn ich zwischendurch immer etwas schüttle, müssten beide Flaschen doch irgendwann die gleiche Mischung
    beinhalten, oder? Wie lange würde das dauern?
</p>

<h3>Voraussetzungen</h3>
<p>
    In den zwei Flaschen sind unterschiedliche Flüssigkeiten (`A` und `B`),
    die sich aber durch leichtes Schütteln wunderbar vermischen.
</p>

<?php
// Kapazitäten der Flaschen
$K = [0.4, 1];
$v = 0.1; // Menge die umgeschüttet werden kann.



$AB = function($A, $B) {
    return (object)['A'=>$A,'B'=>$B];
};
$P = function($i, $f) {
    static $B = true;
    $d = 3;
    /*if ($i>13 && $i<65) {
        if ($B) {
            echo '<tr><td> </td><td>…</td><td>…</td></tr>';
            $B = false;
        } else {
        }
        return;
    }*/
?>
        <tr>
            <td class="right"><?= $i ?></td>
            <td>
                `<?= formatNumber($f[0]->A, $d) ?>A + <?= formatNumber($f[0]->B, $d) ?>B`
                `=<?= formatNumber($f[0]->A + $f[0]->B, $d) ?>l`;
                `frac{B}{A} = <?= $f[0]->A !== 0 ? formatNumber($f[0]->B / $f[0]->A, $d+1) : '?' ?>`
            </td>
            <td>
                `<?= formatNumber($f[1]->A, $d) ?>A + <?= formatNumber($f[1]->B, $d) ?>B`
                `=<?= formatNumber($f[1]->A + $f[1]->B, $d) ?>l`;
                `frac{B}{A} = <?= $f[1]->A !== 0 ? formatNumber($f[1]->B / $f[1]->A, $d+1) : '?' ?>`
            </td>
        </tr>
<?php
};
$f = [
    $AB($K[0], 0),
    $AB(0, $K[1]-$v)
];
$tragetQ = round($f[1]->B / $f[0]->A, 4);
?>
<dl>
<dt>Flasche 1</dt><dd>Fassungsvermögen: `<?= formatNumber($K[0]) ?>l`, gefüllt mit `<?= formatNumber($f[0]->A) ?>l` Haselnussdrink (`A`).</dd>
<dt>Flasche 2</dt><dd>Fassungsvermögen: `<?= formatNumber($K[1]) ?>l`, gefüllt mit `<?= formatNumber($f[1]->B) ?>l` pure Milch (`B`).</dd>
<dt>Umschütt-Menge</dt><dd>`<?= formatNumber($v) ?>l`</dd>
<dt>Ziel-Mischverhältnis</dt><dd>`frac{B}{A} = <?= formatNumber($tragetQ) ?>`</dd>
</dl>


<h3>Simulation (iterativ)</h3>
<table class="table" >
    <thead>
        <tr>
        <th class="right">Stand</th>
        <th>Flasche 1</th>
        <th>Flasche 2</th>
        </tr>
    </thead>
    <tbody id="flaschensimbody">
<?php for ($i=0; $i<71; $i++) : 
$P($i, $f);
$i++;
$vv = $v / $K[0];
$s = $AB($vv*$f[0]->A, $vv*$f[0]->B);
$f[0]->A -= $s->A;
$f[0]->B -= $s->B;
$f[1]->A += $s->A;
$f[1]->B += $s->B;
$P($i, $f);
// es soll ja nicht die die hälfte von der größeren menge umgeschüttet werden,
// sondern so viel, dass wieder 1 übrigbleibt
//a+b=$K[1]
//vv(a+b)=v
//vv($K[1])=v
$vv = $v / $K[1];
$s = $AB($vv*$f[1]->A, $vv*$f[1]->B);
$f[1]->A -= $s->A;
$f[1]->B -= $s->B;
$f[0]->A += $s->A;
$f[0]->B += $s->B;

if ($f[0]->A !== 0) {
    $q = round($f[0]->B / $f[0]->A, 4);
    if ($q === $tragetQ) {
        break;
    }
}

endfor; ?>
    </tbody>
</table>

<script defer>
let $el = document.getElementById('flaschensimbody');
if ($el) {
    let count = $el.children.length;
    let $insertafter = null;
    if (count > 10) {
        for (let i = 0; i < $el.children.length; i++) {
            if (i>4 && i < $el.children.length-4) {
                if ($insertafter === null) {
                    $insertafter = $el.children.item(i);
                }
                $el.children.item(i).style.display = 'none';
            }
        }
        let $insertTR = document.createElement('tr');
        let $td = document.createElement('td');
        $td.setAttribute('colspan', '3');
        $td.classList.add('center')
        let $button = document.createElement('button');
        $button.innerText = 'Alle Zeilen zeigen'
        $insertTR.appendChild($td);
        $td.appendChild($button);
        $el.insertBefore($insertTR, $insertafter);
        $button.addEventListener('click', ()=>{
            $insertTR.remove();
            for (let i = 0; i < $el.children.length; i++) {
                $el.children.item(i).style.display = '';
            }
        })
    }
}
</script>


<h3>Eine direkte Formel</h3>
<p>
    Ich habe auch versucht, eine direkte Formel für die Anteile von A und B in der Mischung aufzustellen.
    Nach den ersten paar Schritten habe ich das aber sein lassen – es erschien mir die Mühe nicht wert.
    Vielleicht hatte ich aber auch einen ungeschickten Ansatz. Vielleicht bekommt das jemand anderes hin?
</p>







