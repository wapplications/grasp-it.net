<?php

return [
    'show'=>true,
    'title'=>'Zwei Flüssigkeiten in zwei Flaschen mischen',
    'date'=>'2024-08-26',
    'tags'=>['Mathematik','Alltag', 'Real-World-Math'],
    'description'=>'Sagen wir, du hast ein sehr süßes Milchmischgeträng und möchtest es mit purer Milch mischen,
    damit es lecker aber nicht zu süß ist. Du hast aber nur die zwei Flaschen zum Mischen zur Verfügung.
    #Man linkt etwas von der Milch, schüttet etwas vom Süßen hinein, schüttelt und schüttet dann den teil wirder zurück,
    schüttelt usw...',
    'thumbnail'=>'thumb.jpeg'
];