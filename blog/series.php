<?php
if (!function_exists('series')) die();

series(
    'ki', [
        'ki0-fake-erkennen',
        'ki1-training-testing',
        'ki2-trainingsdaten-dataworker',
        //'ki3-smiley-erkennung',
        //'ki4-neuronales-netz'
    ],
    'Die KI-Reihe',
    'Fortsetzung folgt…'
);

series(
    'rwm', [
        'real-world-math-kuchen',
        'real-world-math-mischen',
        'real-world-math-sahnemilch',
        'real-world-math-tabletten1',
        'real-world-math-tabletten2',
        'real-world-math-tomatenmark',
        'real-world-math-adventskerzen',
    ],
    'Real-Math-Problems',
    '…und weiter folgen bestimmt.'
);

series(
    'piiri-rechenwerk', [
        'piiri-einfuerung',
        'piiri-analysieren',
        'piiri-halbaddierer',
        'piiri-volladdierer',
        'piiri-addierwerk',
        'piiri-negativ',
        'piiri-rechenwerk',
    ],
    'Die Reihe zur Schaltungssimulation PiiRi',
    '…Fortsetzung folgt.'
);

