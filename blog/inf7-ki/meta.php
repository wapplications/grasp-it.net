<?php

return [
    'show'=>false,
    'title'=>'KI und der Informatik-Aufbaukurs in der 7. Klasse',
    'date'=>'2024-08-29',
    'tags'=>['KI','Informatik','Schule'],
    'description'=>'Da in Baden-Württemberg derzeit KI im Bildungsplan für Klasse 7 keine Rolle spielt, habe ich
    überlegt, wie man die Inhalte rund um das Thema KI anrichten kann. Hier schreibe ich meine Gedanken dazu auf.',
    'thumbnail'=>''
];