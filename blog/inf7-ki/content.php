<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>


<!-- https://km.baden-wuerttemberg.de/de/schule/digitalisierung/kuenstliche-intelligenz-im-unterricht -->

<p>Im Bildungsplans 7. Klasse Informatik  stehen Inhalte zu diesen Themen:</p>
<ol>
    <li>Daten und Codierung
    <li>Algorithmen
    <li>Rechner und Netze
    <li>Informationsgesellschaft und Datensicherheit
</ol>

<p>
    Das Thema Fake-Bilder und Quellen-Angaben behandle ich bereits in der 5. Klasse (Basiskurs Medienbildung).
</p>



<p>
    Ein KI-System kann man sowohl in der Anwendung als auch beim Training sehr gut mit dem EVA-Prinzip beschreiben:
    Input-Daten gehen rein, dann wird da irgendwas verarbeitet und es kommen Output-Daten heraus, eine »Antwort«.
</p>