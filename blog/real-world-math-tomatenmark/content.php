<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>



<h3>Was ist das Problem?</h3>
<p>
    Ich wollte günstig Tomatenmark kaufen und hatte dabei zwei Optionen in der engeren Auswahl:
    Ein »doppelt konzentriertes Tomatenmark« kostete `0,95 €`.
    Ein »dreifach konzentriertes Tomatenmark« gab es hingegen für `1,49 €` im Angebot.
</p>
<p>
    Es war für mich nicht direkt ersichtlich, welches nun die bessere Wahl wäre.
    Die zweite Option war zwar teurer, wegen der höheren Konzentration
    würde ich darin aber auch mehr Tomate erwartet haben.
</p>

<h3>Was bedeutet hier »mehrfach konzentriert«?</h3>
<p>
    Tomatenmark gibt es einfach, doppelt oder dreifach konzentriert zu kaufen.
    Die Angabe sagt dem Verbraucher, wieviel Wasser dem Tomatenpüree entzogen wurde,
    bevor es in die Tube oder das Glas kam. Je »mehrfach«, desto mehr Wasser wurde entzogen.
</p>
<p>
    Unterschieden werden die verschiedenen Konzentrate anhand ihres 
    Tomaten-Trockenmasseanteil – das heißt:
    Das, was übrig bleibt, wenn wir alles Wasser entfernen, setzen wir ins Verhältnis zur gesamt Masse.
</p>
<aside>
    Wenn ich 1kg Tomatenmark trockne und dabei 200g getrocknete Tomate zurückbleibt,
    habe ich einen Trockenmasseanteil von `200/1000 = 20/100 = 20%`.
    Bleibt bei der selben Menge Tomatenmark nur 100g übrig, ist es ein Trockenmasseanteil von
    nur `10%`.
</aside>
<p>
    Handelsübliche Werte für Tomatenmark sind:
</p>
<dl>
    <dt>doppelt / 2-fach konzentriert</dt><dd>mindestens 28&nbsp;% Trockenmasseanteil</dd>
    <dt>dreifach konzentriert</dt><dd>mindestens 36&nbsp;% Trockenmasseanteil</dd>
</dl>

<?= Template::component('media', ['path'=>$post, 'media'=>'imregal.jpeg', 'caption'=>'Verschiedene Preise im Supermarktregal', 'class'=>'right-1']) ?>
<h3>Was ist jetzt das günstigere Produkt?</h3>
<p>
    Um den Preis hier wirklich vergleichen zu können, müssen wir den Preis
    auf den tatsächlichen Tomate-Gehalt des Tomatenmarks umrechnen.
</p>
<aside>
    <h4>Option 1: 200g; doppelt konzentriert; 0,95 €</h4>
    <p>
        ca. 30 % Trockenmasse: `200g cdot 0,30 = 60g`.<br/>
        Preis pro 100g: `100 cdot (0,95 : 60) ~= 1,58`.
    </p>
    <h4>Option 2: 200g; dreifach konzentriert; 1,49 €</h4>
    <p>
        ca. 40 % Trockenmasse: `200g cdot 0,40 = 80g`.<br/>
        Preis pro 100g: `100 cdot (1,49 : 80) ~= 1,86`.
    </p>

</aside>
<p>
    Obwohl Option 2 im Angebot war, ist sie pro 100g Trockenmasse teurer als Option 1.
    Zu beachten ist aber, dass dies keine allgemeine Aussage über unterschiedlich
    konzentriertes Tomatenmark zulässt. Die Rechnung ist nur exemplarisch zu verstehen.
</p>


