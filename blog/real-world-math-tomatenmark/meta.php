<?php

return [
    'show'=>true,
    'title'=>'Tomatenmark dreifach konzentriert?',
    'date'=>'2024-12-12',
    'tags'=>['Mathematik', 'Alltag', 'Tomatenmark', 'Real-World-Math'],
    'description'=>'Tomatenmark gibt es in verschiedenen Konzentrationen – das macht den Preisvergleich nicht so einfach. Ich schaue mir das hier mal mathematisch an, damit ich im Supermarkt eine fundierte Entscheidung treffen kann.',
    'thumbnail'=>'_ba900432-1277-4e67-b6f7-c8f791a43099.jpeg'
];
