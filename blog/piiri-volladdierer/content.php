<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>


<p>
    PiiRi ist in einer Demo-Version verfügbar unter <a href="https://kursbegleiter.de/piiri" target="_blank">kursbegleiter.de/piiri</a>
</p>
<?= Template::component('media', ['path'=>$post, 'media'=>'folge4.mp4', 'caption'=>'Mit PiiRi einen Voll-Addierer bauen', 'class'=>'center']) ?>

<p>
    Der Halb-Addierer aus dem letzten Video addiert zwei Bits und ermittelt die Summe sowie den Übertrag.

    Am Ende soll das Addierwerk wie beim schriftlichen Addieren 
    Stelle für Stelle die passenden Bits addieren.
    
    Dafür muss bei der Berechnung einer Stelle, gegebenenfalls ein Übertrag der vorherigen Stelle
    berücksichtigt, also zusätzlich addiert werden.

    Das ist die Aufgabe des Voll-Addierers.
</p>
<p>
    Du brauchst also zusätzlich zu den Eingängen `a` und `b`
    den Übertrag aus der vorigen Stelle `ddot u`.
    Die Ausgänge bleiben `s` und `ddot u` für das Ergebnis in Form von Summe und Übertrag.
</p>
<p>
    Für die Summe `a+b` verwendest du die zuvor erstellte Halb-Addierer-Komponente.
    Mit Doppelklick kannst du in den Halb-Addierer hineinsehen 
    und siehst seinen Aufbau aus dem letzten Video.

    Ein zweiter Halb-Addierer ist nun dafür zuständig,
    den Übertrag aus der vorherigen Stelle zum Zwischenergebnis `a+b` zu addieren: `s=a+b+ddot u`.
</p>
<p>
    Durch etwas Überlegung stellst du fest, dass ein Übertrag genau dann im Ergebnis stehen muss,
    wenn einer der Halb-Addierer einen Übertrag liefert.
    Du verschaltest die beiden Überträge also mit einem or-Gatter.

    Da beim Addieren von drei Bits höchstens `3` herauskommen kann,
    ist auch kein Übertrag `>1` zu erwarten.
</p>
<p>
    Ich lasse mir zum Überprüfen die Wertetabelle erzeugen.
</p>


<table class="table vline-3">
    <thead>
        <tr><th>`a`</th><th>`b`</th><th>`ddot u`</th>   <th>`ddot u`</th><th>`s`</th></tr>
    </thead>
    <tbody>
    <tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
    <tr><td>0</td><td>0</td><td>1</td><td>0</td><td>1</td></tr>
    <tr><td>0</td><td>1</td><td>0</td><td>0</td><td>1</td></tr>
    <tr><td>0</td><td>1</td><td>1</td><td>1</td><td>0</td></tr>
    <tr><td>1</td><td>0</td><td>0</td><td>0</td><td>1</td></tr>
    <tr><td>1</td><td>0</td><td>1</td><td>1</td><td>0</td></tr>
    <tr><td>1</td><td>1</td><td>0</td><td>1</td><td>0</td></tr>
    <tr><td>1</td><td>1</td><td>1</td><td>1</td><td>1</td></tr>
    </tbody>
</table>

<p>
    Damit du diese Schaltung im Weiteren wiederverwenden kannst, speichere sie
    unter »va« für Voll-Addierer ab.
</p>



