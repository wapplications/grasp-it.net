<?php

return [
    'show'=>true,
    'title'=>'Ein Rechenwerk mit PiiRi: Der Voll-Addierer addiert 3 Bits',
    'date'=>'2024-10-20',
    'tags'=>['Informatik', 'Schule', 'Schaltung', 'TechnischeInformatik', 'piiri', 'Rechenwerk'],
    'description'=>'Der Voll-Addierer addiert zusätzlich einen Übertrag von einer vorigen Stelle und ist damit der nächste Schritt in Richtung
    Rechenwerk.',
    'thumbnail'=>'_0c9c2985-38a6-476f-8461-ef4e1f2ca701.jpeg',
    'emblem'=>'https://kursbegleiter.de/piiri/img/logo.svg',
];