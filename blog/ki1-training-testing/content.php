<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>


<?php
// TODO notiz an mich: kann man ein system machen, in dem alle möglichen entscheidungsbäume da sind und die wichtigen verknüpfungen dann anhand von gewichtungen allmälich sich ergebn werden
// affen spiel mit abstrakten dingen: man zeigt einem menschen viele bilder von dingen und sagt labels dazu
// mit der zeit lernt der mensch auf was es ankommt (z.B. auf das hintergrund muster, oder ein Punkt in einer bestimmten Ecke)



?>





<h2>Stau oder kein Stau?</h2>
<?= Template::component('media', ['path'=>$post, 'media'=>'traffic.jpg', 'caption'=>'Stau oder nicht?', 'class'=>'right-1']) ?>

<p>
    Angenommen, wir wollen eine Autobahn überwachen, um frühzeitig einen Stau zu erkennen.
    So könnten wir beispielsweise den Verkehr vorher verlangsamen und dadurch Gefahren reduzieren.
</p>
<p>
    Wir bauen also an jede Brücke eine Kamera, die in regelmäßigen Abständen ein Live-Bild an eine Verkehrszentrale liefert.
    Dort sitzen dann Herr Langsam und Frau Stockend – Tag und Nacht – und beobachten die übertragenen Bilder.
    Sobald sich was staut, schlagen sie Alarm. – So natürlich nicht.
</p>
<p>
    Da das hier ein Artikel zum Thema KI ist, hat sich jeder schon gedacht, dass man das vielleicht automatisieren will.
    Wir müssen also einem Computer beibringen, wie das aussieht, wenn auf dem Kamerabild ein Stau zu sehen ist.
    Eine Aufgabe, für die wir Menschen keine besondere Ausbildung benötigen: wir können das i.d.R. einfach.
</p>
<p>
    Um einem lernenden Ding soetwas beizubringen, brauchen wir als Allererstes Bilder,
    zu denen wir jeweils notiert haben, ob da Stau zu sehen ist oder nicht,
    um diese dem System zum Trainieren zu zeigen. Diese Bilder mit Beschriftung nennen wir »Trainings-Daten«.
    Dabei lassen wir bewusst mal außen vor, wie dieses Lernen dann eigentlich von Statten geht.
</p>
<p>
    Wenn wir das System trainiert haben, es also hoffentlich gelernt hat, einen Stau zu erkennen,
    müssen wir das Ding natürlich auf die Probe stellen:
    Kann das System auch bei einem bislang unbekannten Bild richtig entscheiden?
</p>
<p>
    Vielleicht hat das System ja nur für jedes Trainings-Bild das gewünschte Ergebnis
    »auswendig gelernt«.
    Wir kennen das von unserem Lernen (z.B. in Schule, Ausbildung, Studium):
    Wir lernen, trainieren, üben anhand einiger Aufgabenstellungen und werden dann mit ähnlichen
    aber nicht den genau gleichen Problemen in einer Prüfung konfrontiert.
    Haben wir nur auswendig gelernt oder haben wir das Konzept verstanden und können es somit auf neue,
    unbekannte Situationen anwenden?
</p>

<p>
    Zusammenfassend: Die Lernmethode, die wir hier anschauen ist das sogenannte »supervised learning« oder »überwachtes Lernen«.
    Dabei werden zuvor vom Menschen erzeugte Datensätze mit gewünschten Ergebnissen benötigt.
    Das Lernen erfolgt dann in zwei Phasen.
</p>
<ol>
    <li>Trainings-Phase</li>
    <li>Testing-Phase</li>
</ol>

<aside>
    Es gibt auch andere Formen des maschinellen Lernens:
    z.B.
    »unüberwachtes Lernen« (englisch »unsupervised learning«)
    und
    »bestärkendes Lernen« (englisch »reinforcement learning«)
</aside>

<!-- PAGEBREAK Wir können das gut: Entscheidende Merkmale erkennen -->

<h2>Ein Experiment</h2>
<p>
    Um etwas unterscheiden zu können, muss man…
</p>
<ol>
    <li>…wissen, worauf man achten muss</li>
    <li>…diese Merkmale den Entscheidungs-Möglichkeiten zuordnen (Begriffbildung).</li>
</ol>
<p>
    Wie ist das eigentlich, wenn man etwas lernen soll, bei dem man nicht mal weiß, worauf man achten muss?
    So wie ein Computer, der lernen soll, Stau von nicht-Stau zu unterscheiden.
    Im folgenden Spiel siehst du Bilder, die entweder »Darsum« oder »Femp« sind, zwei erfundene Kategorieren.
    Wenn du meinst, du kannst die zwei unterscheiden, gehe zum Test und beurteile 12 weitere Bilder.
</p>

<?= Template::component('app', ['name'=>'learntodiffer', 'title'=>'Lerne zu unterscheiden', 'options'=>['Darsum', 'Femp']]) ?>



<?php $this->startComponent('hidden', ['title'=>'Welches Merkmal war ausschlaggebend?', 'command'=>'Schaue dir die Lösung an…']) ?>
    <?= Template::component('media', ['path'=>$post, 'media'=>'experiment.png', 'caption'=>'Genau dann, wenn ein Quadrat vorhanden ist, ist es ein »Darsum«', 'class'=>'center imgmargin']) ?>
<?php $this->endComponent('hidden') ?>




<!-- PAGEBREAK Wir erkennen bissige Affen -->

<h2>Der »Affen-Zoo«</h2>

<?= Template::component('media', ['path'=>$post, 'media'=>'affen1.png', 'caption'=>'Trainingsdaten: ein Teil der bekannten Affen', 'class'=>'right-1', 'c'=>'Basiert auf einer Arbeit von Annabel Lindner und Stefan Seegerer']) ?>
<p>
    Wir stellen uns im Folgenden vor, wir seien die Leitung eines Parks, in dem Affen leben.
    Dabei sind uns einige Affen bekannt, die bissig sind und den Besuchern oder Mitarbeitern gefährlich werden können.
    Andere sind völlig zahm und würden sich sogar gerne von Besuchern streicheln lassen.
    Wenn neue Affen in den Park einziehen, wird man wohl erst durch den (vielleicht) schmerzhaften Versuch
    herausbekommen, ob sie beißen oder nicht.
    Was liegt hier näher, als ein System einzurichten,
    das automatisch und ohne Gefährdung bissige und nicht-bissige Affen erkennt?
</p>
<p>
    Das Material und die Idee für das Affen-Spiel stammt von Annabel Lindner und Stefan Seegerer.
    Siehe <a href="https://coding-for-tomorrow.de/konzepte/ki-analog/">KI analog auf coding-for-tomorrow.de</a>
    und <a href="https://www.aiunplugged.org/">AI unplugged</a>.
</p>

<p>
    Aber wie lassen sich bissige von nicht-bissigen Affen unterscheiden?
    Am besten schaut man sich die Affen an, deren Einordnung uns bekannt ist:
    Welche Gemeinsamkeiten fallen uns auf? Lässt sich daraus eine Entscheidungs-Regel ableiten?
</p>


<h2 class="clear">Entscheidungsregel suchen (Training)</h2>
<p>
    Wir nutzen dabei ein letztes Mal die menschliche Superkraft,
    in unbekannten Daten Gemeinsamkeiten und Unterschiede zu erkennen:
    Wir erkennen vielleicht schnell, dass der einfach lächelnde Mund nur bei den bissigen vorkommt.
    Eine Entscheidungsregel könnte also etwa so beginnen:
</p>
<?= Template::component('media', ['path'=>$post, 'media'=>'regel-baum1.png', 'caption'=>'Die Entscheidungsregel', 'class'=>'center']) ?>


<hr class="clear"/>

<?= Template::component('media', ['path'=>$post, 'media'=>'affen2.png', 'caption'=>'Testdaten: der andere Teil der bekannten Affen', 'class'=>'right-1 clear', 'c'=>'Basiert auf einer Arbeit von Annabel Lindner und Stefan Seegerer']) ?>
<h2 class="">Güte überprüfen (Testing)</h2>
<p>
    Es gibt viele Möglichkeiten, eine solche Entscheidungsregel zu formulieren.
    In jedem Fall müssen wir die Regel mit einem anderen Teil der bekannten Affen testen.
    Wird er alle Affen richtig einordnen, obwohl sie beim Training nicht verwendet wurden?
    Schaut das System auf die richtigen Merkmale?
</p>


<!-- PAGEBREAK: Fehler passieren -->

<?= Template::component('media', ['path'=>$post, 'media'=>'bissig.png', 'caption'=>'Ein neuer bissiger Affe', 'class'=>'right-1', 'c'=>'Basiert auf einer Arbeit von Annabel Lindner und Stefan Seegerer']) ?>
<h2>Fehler passieren…</h2>
<p>
    Es kann gut sein, dass man eine Entscheidungsregel gefunden hat, die nicht alle Test-Affen korrekt erkennt.
    Falls doch: Gratulation.
    Allerdings kommt jetzt ein neuer Affe, der in einem anderen Park schon als bissig aufgefallen ist, zu uns.
    Wie wird dieser Affe vom System beurteilt?
    Wir müssen in jedem Fall das Thema »Fehler« aufgreifen.
    Dafür schauen wir uns an, warum die Systeme Fehler machen, welche Arten von Fehlern
    es hier geben kann und was das für die jeweiligen Akteure bedeuten könnte.
    Letzteres lasse ich die SchülerInnen selbst überlegen.
</p>
<h3>Warum passieren Fehler?</h3>
<p>
    Zwei Gründe fallen mir ein, die zu einer falschen Entscheidung führen können:
</p>
<ol>
    <li>Das System ist nicht optimal oder fehlerhaft trainiert.</li>
    <li>Die Trainingsdaten decken nicht alle tatsächlich möglichen Fälle ab
        oder sie sind mit Vorurteilen belastet.</li>
</ol>
<p>
    Es kann auf diese Weise eigentlich kein garantiert perfektes System geben.
    Darauf entgegnete mir eine Schülerin sinngemäß sehr treffend:
</p>
<blockquote>
    Doch! Man kann einfach alles, was es gibt, zum Trainig verwenden – [Denkpause] – oh, das geht wahrscheinlich schlecht…
</blockquote>

<h3>Was bedeuten Fehler in der Realität?</h3>
<p>
    Es gibt zwei Fehlerarten:
</p>
<ol>
    <li>Das System erkennt einen bissigen Affen nicht und stuft ihn fälschlicherweise als harmlos ein.</li>
    <li>Das System schlägt bei einem völlig harmlosen Affen Alarm, und meint, er sei bissig.</li>
</ol>
<p>
    Wenn ein bissiger Affe nicht erkannt wird, kann das für Mitarbeiter oder Besucher ein Problem sein.
    Wenn hingegen ein harmloser Affe als bissig eingestuft wird, so ist das möglicherweise ein Problem für den Affen:
    Er wird zu unrecht behandelt, als wäre er gefährlich.
</p>
<p>
    Diese Überlegungen lassen sich dann auch auf andere KI-Anwendungen übertragen:
    Welche Art von Fehler ist für wen ein Problem?
</p>



