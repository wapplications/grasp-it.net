<?php

return [
    'show'=>true,
    'title'=>'KI II: Automatische Entscheidungen: Trainieren und Testen',
    'date'=>'2024-03-09',
    'tags'=>['KI','Informatik','Gesellschaft'],
    'description'=>'Ein Entscheidungs-System wird anhand einer Reihe von Informationen
    so aufgebaut, dass es eine Gefahr erkennen kann.
    Danach wird getestet, ob die Entscheidungen bei unbekannten Fällen auch richtig sind.
    Wie kann es dabei zu Fehlern kommen und was hat das für Konsequenzen?',
    'thumbnail'=>'thumb.jpeg'
];
