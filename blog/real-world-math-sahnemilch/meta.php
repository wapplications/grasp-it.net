<?php

return [
    'show'=>true,
    'title'=>'Milch aus Sahne machen',
    'date'=>'2024-05-08',
    'tags'=>['Mathematik', 'Alltag', 'Real-World-Math'],
    'description'=>'Wenn du keine Milch hast, dann nimm doch Sahne – frei nach Marie Antoinette.
    Das führt mich zu einem alten Studentenprobleme:
    Wieviel Wasser muss man zu Sahne geben, damit das Gemsich einen Fettgehalt von Milch hat?',
    'thumbnail'=>'thumb.jpeg'
];