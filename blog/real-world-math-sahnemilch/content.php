<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>




<p>
    Da wollte ich mir in der Studenten-WG ein Müsli machen und hatte keine Milch da; nur noch Sahne.
    Kein Problem. Sahne ist ja wie Milch, nur mit mehr Fett.
    Das stimmt so natürlich nicht ganz,
    aber es hielt mich nicht von der Idee ab, Sahne mit Wasser zu verdünnen, um wieder etwas Milchähnliches zu bekommen.
</p>

<h3>Grundlegendes</h3>
<dl>
    <dt>Fettgehalt Vollmilch</dt><dd>mind. 3,5%</dd>
    <dt>Fettgehalt Schlagsahne</dt><dd>mind. 30%</dd>
    <dt>Fettgehalt Wasser</dt><dd>0%</dd>
</dl>
<p>
    Ich gehe von einem halben Liter Sahne aus und frage mich, wieviel Wassr `x` ich zugeben müsste,
    damit das Gemisch einen Fettgehalt von 3,5% hat.
</p>

<h3>Meine Lösung</h3>
<p>
    Gesamtmenge des Gemisches: `G = 0,5 + x`<br/>
    Fettmenge in der Sahne: `F = 30%*0,5 = 0,15`<br/>
    Fettgehalt des Gemisches: `p = F / G = 3,5%`<br/>
    Ich löse also die Gleichung<br/>
    `frac{0,15}{0,5 + x} = 0,035`<br/>
    `<=> 0,15 = 0,035(0,5 + x)`<br/>
    `<=> 4,285714286 = 0,5 + x`<br/>
    `<=> 3,785714286 = x`<br/>
</p>
<p>
    Zum halben Liter Sahne müsste ich also etwa `3,8l` Wasser zugeben, um den Fettgehalt von Milch zu erhalten.
</p>

<h3>Alternative Lösung ohne Variable (Prozenrechnen)</h3>
<p>
    Prozentwert: Fettmenge im Gemisch: `P = 30%*0,5 = 0,15`<br/>
    Gewünschter Prozentsatz: `p = 3,5%`<br/>
    Prozent-Formel: `p = P / G => 0,035 = 0,15 / G`<br/>
    Gesucht ist die Grundmenge `G`, also wieviel das Gemsich am Ende sein muss, um den gewünschten Fettanteil zu haben.<br/>
    `=> 0,035*G = 0,15`<br/>
    `=> G = 0,15 : 0,035`<br/>
    `=> G = 4,285714286`<br/>
</p>
<p>
    Jetzt nur noch `0,5l` Sahne abziehen und fertig: `4,285714286l - 0.5l = 3,785714286l`.
</p>

<p>
    Ob ich das wirklich gemacht habe und ob das lecker war - dazu werde ich schweigen.
</p>


