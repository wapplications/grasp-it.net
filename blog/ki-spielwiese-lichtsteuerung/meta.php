<?php

return [
    'show'=>true,
    'title'=>'KI-Spielwiese SmileKI: Die KI erlernt eine Licht-Steuerung',
    'date'=>'2024-09-28',
    'tags'=>['Informatik','Schule', 'KI', 'SmileKI', 'NeuronalesNetz'],
    'description'=>'Ein einfaches künstliches neuronales Netz soll lernen, eine Lichtsteuerung aus ein paar Beobachtungen richtig zu berechnen.',
    'thumbnail'=>'_6ab2ccf4-ef01-469e-8479-07e327426cd0.jpeg'
];