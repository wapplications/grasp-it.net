<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>

<p>
    Dieser Artikel bezieht sich auf das Tool
    <a href="https://kursbegleiter.de/smileki/" target="_blank">SmileKI</a>
    aus der KursBegleiter-Tool-Sammlung.
    Damit kannst du ein kleines neuronales Netz bauen, es mit verschiedenen
    Input-Arten ansteuern und das Ergebnis mit unterschiedlichen Output-Modulen
    anzeigen lassen.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'uebersicht.png', 'caption'=>'Die Oberfläche des KI-Tools SmileKI.', 'class'=>'center', 'c'=>'']) ?>


<h3>Eine Steuerung lernen</h3>
<p>
    Ich nehme eine einfache Situation einer Steuerung,
    bei der die gewünschten Steuersignale bekannt sind und
    eine KI lernen soll, diese zu berechnen.
    Es handelt sich um ein super einfaches Beispiel, soll aber die
    Funktion eines solchen Lernprozesses demonstrieren.
</p>

<blockquote>
    Das Licht soll an sein wenn es dunkel ist und ich zuhause bin.
</blockquote>

<?= Template::component('media', ['path'=>$post, 'media'=>'input.png', 'caption'=>'Input-Modul eingestellt', 'class'=>'right-2', 'c'=>'']) ?>
<h4>Schritt 1: Input einstellen</h4>
<p>
    Für die Steuerung werden zwei Inputs benötigt: Die Helligkeit
    und ob ich zuhause bin oder nicht.
    Ich verwende <strong>Zahlen (Slider)</strong> und stelle über den Stift den
    Wertebereich <strong>0-1</strong> ein.
    Der erste Input gibt die Helligkeit zwischen 0 (dunkel) und 1 (hell) an,
    der zweite die Anwesenheit: 0=weg, 1=da.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'output.png', 'caption'=>'Output-Modul eingestellt', 'class'=>'right-2 clear', 'c'=>'']) ?>
<h4>Schritt 2: Output einstellen</h4>
<p>
    Ich benötige einen Ausgang für die Steuerung des Lichts: 0=AUS, 1=AN.
    Dazu stelle ich einen Output und <strong>Zahlen (Balken)</strong> ein.
    So kann ich den genauen Wert des Ausgangs beobachten und
    mir vorstellen, dass das Licht ab einem bestimmten Grenzwert
    angeschaltet würde.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'net.png', 'caption'=>'Das neuronale Netz', 'class'=>'right-2 clear', 'c'=>'']) ?>
<h4>Schritt 3: Neuronales Netz einrichten</h4>
<p>
    Ich belasse die Einstellungen zunächst so wie sie sind
    mit einer versteckten Schicht.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'data.png', 'caption'=>'Aufgezeichnete Daten', 'class'=>'right-2 clear', 'c'=>'']) ?>
<h4>Schritt 4: Trainings-Daten aufzeichnen</h4>
<p>
    Ich wechsle auf der rechten Seite auf den Tab <strong>Daten</strong>
    und achte darauf, dass hier <strong>Training</strong> aktiviert ist.
    Hier ist eine leere Liste von Trainingsdaten zu sehen,
    die ich jetzt füllen werde.
</p>
<p>
    Wenn es sehr hell ist, soll das Licht aus sein, egal ob ich
    zuhause bin oder nicht.
    Ich brauche also Datensätze, bei denen `"Input"_2 = 0` und solche,
    bei denen `"Input"_2 = 1` ist.
    Sehr hell könnte vielleicht `"Input"_1 > 0,7` heißen.
    Jetzt stelle ich auf der linken Seite einige solcher Werte ein
    und achte darauf, dass Input und Output in der gewünschten Logik
    zusammenpassen.
    Um einen Datensatz zum Trainieren zu erfassen,
    klicke ich auf das <strong>+</strong>.
</p>
<?php $this->startComponent('hidden', ['title'=>'Meine Daten', 'command'=>'ansehen']); ?>
<table class="table">
    <tr>
        <th>Helligkeit:<br/>`"Input"_1`</th>
        <th>Zuhause:<br/>`"Input"_2`</th>
        <th>Licht:<br/>`"Ouput"`</th>
    </tr>
    <tr><td class="right">`0,79`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,94`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`1,00`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,82`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,75`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,75`</td><td class="right">`1`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,85`</td><td class="right">`1`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,93`</td><td class="right">`1`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,99`</td><td class="right">`1`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,87`</td><td class="right">`1`</td><td class="right">`0`</td></tr>
</table>
<?php $this->endComponent('hidden') ?>
<p>
    Wenn es allerdings zu dunkel ist (z.B. `"Input"_1 < 0,6`),
    soll das Licht nur angehen, wenn ich zuhause bin.
</p>
<?php $this->startComponent('hidden', ['title'=>'Meine Daten', 'command'=>'ansehen']); ?>
<table class="table">
    <tr>
        <th>Helligkeit:<br/>`"Input"_1`</th>
        <th>Zuhause:<br/>`"Input"_2`</th>
        <th>Licht:<br/>`"Ouput"`</th>
    </tr>
    <tr><td class="right">`0,54`</td><td class="right">`1`</td><td class="right">`1`</td></tr>
    <tr><td class="right">`0,36`</td><td class="right">`1`</td><td class="right">`1`</td></tr>
    <tr><td class="right">`0,18`</td><td class="right">`1`</td><td class="right">`1`</td></tr>
    <tr><td class="right">`0,07`</td><td class="right">`1`</td><td class="right">`1`</td></tr>
    <tr><td class="right">`0,29`</td><td class="right">`1`</td><td class="right">`1`</td></tr>
    <tr><td class="right">`0,00`</td><td class="right">`1`</td><td class="right">`1`</td></tr>
    <tr><td class="right">`0,01`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,29`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,18`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,50`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    <tr><td class="right">`0,53`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
</table>
<?php $this->endComponent('hidden') ?>


<h4>Schritt 5: Test-Daten aufzeichnen</h4>
<p>
    Ich wechsle jetzt in den Abschnitt <strong>Testing</strong>.
    Auch dort sehe ich eine leere Daten-Liste.
    Ich stelle nochmals für jede Kombination ein paar Werte ein
    und füge den Datensatz jeweils mit <strong>+</strong> hinzu.
    Diese Datensätze sind zum Testen, nachdem das Modell trainiert wurde.
</p>
<?php $this->startComponent('hidden', ['title'=>'Meine Daten', 'command'=>'ansehen']); ?>
<table class="table">
    <tr>
        <th>Helligkeit:<br/>`"Input"_1`</th>
        <th>Zuhause:<br/>`"Input"_2`</th>
        <th>Licht:<br/>`"Ouput"`</th>
        <tr><td class="right">`0,45`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
        <tr><td class="right">`0,23`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
        <tr><td class="right">`0,00`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
        <tr><td class="right">`0,00`</td><td class="right">`1`</td><td class="right">`1`</td></tr>
        <tr><td class="right">`0,10`</td><td class="right">`1`</td><td class="right">`1`</td></tr>
        <tr><td class="right">`0,39`</td><td class="right">`1`</td><td class="right">`1`</td></tr>
        <tr><td class="right">`0,83`</td><td class="right">`1`</td><td class="right">`0`</td></tr>
        <tr><td class="right">`1,00`</td><td class="right">`1`</td><td class="right">`0`</td></tr>
        <tr><td class="right">`0,92`</td><td class="right">`1`</td><td class="right">`0`</td></tr>
        <tr><td class="right">`0,87`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
        <tr><td class="right">`1,00`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
        <tr><td class="right">`0,76`</td><td class="right">`0`</td><td class="right">`0`</td></tr>
    </tr>
</table>
<?php $this->endComponent('hidden') ?>


<h4>Schritt 6: Das Modell trainieren</h4>
<?= Template::component('media', ['path'=>$post, 'media'=>'training.gif', 'caption'=>'Der Verlauf des Training', 'class'=>'right-2', 'c'=>'']) ?>
<p>
    Ich wechsle wieder auf den Tab <strong>neuronales Netz</strong>.
    Dann klicke ich auf <strong>Trainieren</strong>.
    Ich setze Wiederholungen auf 1000 und belasse Lern-Rate und Fehlergrenze.
    Dann starte ich das Lernen.
</p>
<p>
    Ich kann beobachten, wie der Fehler i.d.R. schnell runter geht.
    Ich kann auch etwas mit den Einstellungen experimentieren.
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'testing.gif', 'caption'=>'Der Verlauf des Testings', 'class'=>'right-1 clear', 'c'=>'']) ?>
<h4>Schritt 7: Das Modell testen</h4>
<p>
    Ich kann nun mit den Reglern selbst testen, ob das Modell
    richtige Ergebnisse liefert.
    Oder ich klicke <strong>Testing</strong> und lasse die zuvor
    erfassten Test-Daten durchlaufen.
    Ich bekomme dann eine Zusammenfassung, wie gut das Modell
    gearbeitet hat.
</p>

<!-- 
In PiiRi ein KI-Modul einbauen, in das man so ein modell reinladen kann.
und dann mit sensoren und aktoren die Steuerung simulieren?
Brauchen wie dazu in PiiRi einen floating point modus?
-->

<hr class="clear"/>

<h3>Zusammenfassung</h3>
<p>
    Ohne zu wissen, welche logischen Zusammenhänge zwischen Helligkeit, Anwesenheit und Lichtschaltung
    bestehen, kann die KI eine Zeit lang »beobachten«, wann ich das Licht einschalte und wann es aus ist.
    Aus diesen Daten kann das System lernen und für jede Situation ein Verhalten berechnen.
</p>

<h4>Das ganze Experiment zum Importieren in <a href="https://kursbegleiter.de/smileki/" target="_blank">SmileKI</a></h4>
<textarea readonly style="width:100%; box-sizing:border-box; border-radius:0.2em; background-color:#888888; height:10em;">
{"id":null,"input":{"mode":"slider","editable":true,"layer":{"activation":"LOGISTIC","size":2},"config":{"min":0,"max":1}},"output":{"mode":"bars","editable":true,"layer":{"activation":"LOGISTIC","size":1},"config":{}},"hiddenLayers":{"layers":[{"size":2,"activation":"LOGISTIC"}],"editable":true},"training":[{"input":[0.79,0],"output":[0]},{"input":[0.94,0],"output":[0]},{"input":[1,0],"output":[0]},{"input":[0.82,0],"output":[0]},{"input":[0.75,0],"output":[0]},{"input":[0.75,1],"output":[0]},{"input":[0.85,1],"output":[0]},{"input":[0.93,1],"output":[0]},{"input":[0.99,1],"output":[0]},{"input":[0.87,1],"output":[0]},{"input":[0.54,1],"output":[1]},{"input":[0.36,1],"output":[1]},{"input":[0.18,1],"output":[1]},{"input":[0.07,1],"output":[1]},{"input":[0.29,1],"output":[1]},{"input":[0,1],"output":[1]},{"input":[0.01,0],"output":[0]},{"input":[0.29,0],"output":[0]},{"input":[0.18,0],"output":[0]},{"input":[0.5,0],"output":[0]},{"input":[0.53,0],"output":[0]}],"testing":[{"input":[0.45,0],"output":[0]},{"input":[0.23,0],"output":[0]},{"input":[0,0],"output":[0]},{"input":[0,1],"output":[1]},{"input":[0.1,1],"output":[1]},{"input":[0.39,1],"output":[1]},{"input":[0.83,1],"output":[0]},{"input":[1,1],"output":[0]},{"input":[0.92,1],"output":[0]},{"input":[0.87,0],"output":[0]},{"input":[1,0],"output":[0]},{"input":[0.76,0],"output":[0]}]}
</textarea>

<?php

/*
[
{"input":[0,79,0],"output":[0]},
{"input":[0,94,0],"output":[0]},
{"input":[1,0],"output":[0]},
{"input":[0,82,0],"output":[0]},
{"input":[0,75,0],"output":[0]},
{"input":[0,75,1],"output":[0]},
{"input":[0,85,1],"output":[0]},
{"input":[0,93,1],"output":[0]},
{"input":[0,99,1],"output":[0]},
{"input":[0,87,1],"output":[0]},
{"input":[0,54,1],"output":[1]},
{"input":[0,36,1],"output":[1]},
{"input":[0,18,1],"output":[1]},
{"input":[0,07,1],"output":[1]},
{"input":[0,29,1],"output":[1]},
{"input":[0,1],"output":[1]},
{"input":[0,01,0],"output":[0]},
{"input":[0,29,0],"output":[0]},
{"input":[0,18,0],"output":[0]},
{"input":[0,5,0],"output":[0]},
{"input":[0,53,0],"output":[0]},
]


[
{"input":[0.45,0],"output":[0]},
{"input":[0.23,0],"output":[0]},
{"input":[0,0],"output":[0]},
{"input":[0,1],"output":[1]},
{"input":[0.1,1],"output":[1]},
{"input":[0.39,1],"output":[1]},
{"input":[0.83,1],"output":[0]},
{"input":[1,1],"output":[0]},
{"input":[0.92,1],"output":[0]},
{"input":[0.87,0],"output":[0]},
{"input":[1,0],"output":[0]},
{"input":[0.76,0],"output":[0]},
]




[[{"bias":0,"outWeights":[7.048962,8.207246]},{"bias":0,"outWeights":[-5.671737,-6.536548]}],[{"bias":0.755476,"outWeights":[-8.425856]},{"bias":0.914207,"outWeights":[-10.182568]}],[{"bias":7.752255,"outWeights":[]}]]











Binär-Zahlen dekodieren:
{"id":null,"input":{"mode":"bitmap","editable":true,"layer":{"activation":"LOGISTIC","size":3},"config":{"width":3}},"output":{"mode":"percentage","editable":true,"layer":{"activation":"LOGISTIC","size":8},"config":{"names":[{"name":"0","title":"0","icon":""},{"name":"1","title":"1","icon":""},{"name":"2","title":"2","icon":""},{"name":"3","title":"3","icon":""},{"name":"4","title":"4","icon":""},{"name":"5","title":"5","icon":""},{"name":"6","title":"6","icon":""},{"name":"7","title":"7","icon":""}]}},"hiddenLayers":{"layers":[{"size":3,"activation":"LOGISTIC"}],"editable":true},"training":[{"input":[0,0,0],"output":[1,0,0,0,0,0,0,0]},{"input":[0,0,1],"output":[0,1,0,0,0,0,0,0]},{"input":[0,1,0],"output":[0,0,1,0,0,0,0,0]},{"input":[0,1,1],"output":[0,0,0,1,0,0,0,0]},{"input":[1,0,0],"output":[0,0,0,0,1,0,0,0]},{"input":[1,0,1],"output":[0,0,0,0,0,1,0,0]},{"input":[1,1,0],"output":[0,0,0,0,0,0,1,0]},{"input":[1,1,1],"output":[0,0,0,0,0,0,0,1]}],"testing":[{"input":[0,0,0],"output":[1,0,0,0,0,0,0,0]},{"input":[0,0,1],"output":[0,1,0,0,0,0,0,0]},{"input":[0,1,0],"output":[0,0,1,0,0,0,0,0]},{"input":[0,1,1],"output":[0,0,0,1,0,0,0,0]},{"input":[1,0,0],"output":[0,0,0,0,1,0,0,0]},{"input":[1,0,1],"output":[0,0,0,0,0,1,0,0]},{"input":[1,1,0],"output":[0,0,0,0,0,0,1,0]},{"input":[1,1,1],"output":[0,0,0,0,0,0,0,1]}]}
*/





?>