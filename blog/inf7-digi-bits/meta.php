<?php

return [
    'show'=>true,
    'title'=>'Informatik Grundlagen: Bits und Bytes',
    'date'=>'2024-09-22',
    'tags'=>['Informatik','Schule', 'Digitalisierung', 'Daten', 'Bit', 'byte', 'binär'],
    'description'=>'',
    'thumbnail'=>''
];