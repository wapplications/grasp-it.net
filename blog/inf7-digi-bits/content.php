<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>





<p>
    Ob man es glaubt oder nicht: Computer wurden nicht dazu erfunden,
    um Videos zu schauen, Spiele zu spielen und Kommentare unter Posts zu verfassen.
    Computer wurden, wie das im Namen steckende to compute - berechnen sagt, zum Rechnen benutzt.
    Vor allem wissenschaftliche Berechnungen (auch für den Krieg) mussten
    mühevoll mit Stift und Papier berechnet werden.
    Für die ein oder andere Rechenoperation gab es schon Geräte aber 
    so eine programmierbare, universelle Rechenmaschine gab es nicht.
</p>
<p>
    Um rechnen zu können, braucht die Machine eine Möglichkeit, Zahlen zu verarbeiten.
    Unsere Zahlen sind nach dem Dezimalsystem aufgebaut:
    Wir haben 10 Ziffern und können damit beginnen zu zählen:
</p>
<aside>
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9
</aside>
<p>
     – und dann gehen uns die Ziffern aus und wir brauchen für die Zehn eine weitere Ziffer.
    Wir nehmen eine Stelle dazu und sagen, dass diese zehn mal so viel Wert ist:
    10, 11, 12, …, 18, 19 – an der einer Stelle gehen uns wieder die Ziffern 
    aus also gehen wir an der 10er Stelle eins hoch.
    Gehen uns an der 10er Stelle auch die Ziffern aus, brauchen wir eine 100er Stelle usw.
</p>
<p>
    Das 10er Sytem wird viel verwendet für mechanische Rechenmaschinen,
    bei denen man Rädchen und Hebel in 10 verschiedene Stellungen bringen konnte,
    um damit Zahlen einzugeben.
    Wenn ich aber mit Stom arbeiten will, ist es relativ kompliziert und fehleranfällig,
    10 verschiedene Spannungen zu unterscheiden.
    Viel leichter und eindeutiger ist es, wenn man nur an und aus unterscheidet.
</p>
<p>
    An und Aus – das sind zwei Zustände, die man auch als zwei Ziffern betrachten könnte: 0 und 1.
    Wenn ich damit zähle, sieht es so aus:
    0, 1 – und schon gehen mir die Ziffern aus und ich brauche für die Zwei eine weitere Ziffer:
    10, 11 – vier Vier brauch wieder eine neue Ziffer: 100, 101, 110, …
    Jede Ziffer ist also doppelt so viel Wert, wie die vorherige.
</p>
<p>
    Mit dem <a href="https://kursbegleiter.de/legematerial2/binaer.html" target="_blank">digitalen Legematerial</a> möchte ich eine Einführung in die 
    Binärzahlen machen.
</p>
<p>
    Die wild verteilten Kärtchen müssen erstmal sortiert werden:
    Ein Punkt kommt ganz rechts und dann aufsteigend nach links.
</p>
