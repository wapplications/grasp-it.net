<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>



<blockquote>
    Als ich heute Morgen aus dem Fenster schaute, sah ich am Waldrand ein Tier.
    Für einen Fuchs zu groß, für ein Reh zu klein und eher hundeartig… Ein Wolf! Ich hatte ja gehört, dass die Wölfe
    in die deutschen Wälder zurückkehren. Hin und wieder war mir auch so als hätte ich besonders große Hundespuren
    im Wald gesehen… Ich war zunächst wie versteinert.
</blockquote>
<?= Template::component('media', ['path'=>$post, 'media'=>'wolf1.jpg', 'caption'=>'Der Wolf hinterm Haus', 'class'=>'right-1']) ?>
<p>
    Vermutlich wird mir kaum jemand diese Geschichte glauben, hätte ich nicht geistesgegenwärtig schnell ein Foto gemacht.
</p>
<p>
    Auf den ersten Blick ein beeindruckendes Bild – auf den zweiten Blick geht da aber nicht alles mit rechten Dingen zu.
</p>

<h2>Wie kann man KI-Bilder erkennen?</h2>
<p>
    Mit meinen Schülern gehe ich das <a href="https://www.geo.de/wissen/quiz/bilder-quiz--ki-bild-oder-fotografie--33364744.html" target="_blank">Quiz von GEO</a>
    durch und wir diskutieren über Merkmale, die für oder gegen KI bzw. herkömmliche Fotografie sprechen. 
    Dabei lege ich Wert darauf, dass wir versuchen, auch ein Bauchgefühl mit Argumenten zu untermauern.
</p>
<ul>
    <li><a href="https://www.geo.de/wissen/quiz/bilder-quiz--ki-bild-oder-fotografie--33364744.html">Quiz auf geo.de</a>    
    <li><a href="https://www.spiegel.de/netzwelt/web/quiz-zu-kuenstlicher-intelligenz-koennen-sie-ki-bilder-von-echten-fotos-unterscheiden-a-5b2d7cb4-d26f-4a5b-b2bf-c2a2ea98485d?sara_ref=re-xx-cp-sh">Quiz auf Spiegel.de</a>
</ul>
<p>
    Es kristallisieren sich dabei meist diese Merkmale heraus:
</p>
<ul>
    <li>Zu weiche Farben, zu kräftige Farben</li>
    <li>Unnatürliche Proportionen, anatomische Fragwürdigkeiten</li>
    <li>Physikalische Anomalien (zum Beispiel schwebende Dinge)</li>
    <li>Fachwissen steht der Darstellung entgegen</li>
    <li>Reflexionen, Schatten, Transparenz nicht korrekt</li>
    <li>Kamera-Fokus / Tiefen-Unschärfe</li>
    <li>Kryptische Schriftzeichen*</li>
</ul>

<h3>Meine persönliche Einschätzung</h3>
<p>
    Da die Systeme immer besser werden
    (ein Vergleich zwischen den ersten KI-generierten Bildern und dem heutigen Stand wäre interessant),
    sollten wir uns auf Dauer nicht darauf verlassen, KI-Bilder anhand von 
    einzelnen Anomalien (z.B. 6 Finger, falsche Licht-Reflexe etc) zu erkennen.
    Vielmehr muss Sachverstand, (Fach-)Wissen und Erfahrung das Gezeigte einordnen.
    Eine <em>breitgefächerte</em> Allgemeinbildung ist für mich hier der Schlüssel.
</p>
<p>
    Am Ende ist aber der entscheidende Faktor vielleicht:
    Wir glauben doch am liebsten Dinge, die wir gerne glauben möchten, die in unser Weltbild passen,
    die unsere Gefühle bestätigen.
    Das ist meiner Meinung nach das gefährlichste Einfallstor für Fake, Manipulation und Betrug.
</p>


