<?php

return [
    'show'=>true,
    'title'=>'KI I: Woran kann man KI-Bilder erkennen?',
    'date'=>'2024-03-04',
    'tags'=>['KI','Informatik','Gesellschaft'],
    'description'=>'Bilder und Videos gelten oft als Beweis für Ereignisse.
    Wie können wir trotz KI-generierter Inhalte weiterhin Echtheit oder Fälschung beweisen?
    Welche Kompetenzen braucht es, um Bildinformationen einordnen und bewerten zu können?',
    'thumbnail'=>'thumb.jpeg'
];