<?php

return [
    'show'=>true,
    'title'=>'Der Zeitabstand von Medikamentengaben',
    'date'=>'2024-08-29',
    'tags'=>['Mathematik', 'Alltag', 'Katzen', 'Real-World-Math'],
    'description'=>'Die Kater bekommen eine bestimmte Menge Antibiotikum am Tag.
    Bei dem Medikament kann die Tagesdosis auch auf zwei Mal aufgeteilt werden,
    um eine gleichmäßigere Konzentration des Medikamentes über den Tag hinweg zu haben.
    Bei einer Tagesdosis von einer Tablette pro Tag, wäre das einfach, aber was, wenn sich die Dosis nicht einfach halbieren lässt?
',
    'thumbnail'=>'thumb1.jpeg'
];
