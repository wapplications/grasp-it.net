<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>




<p>
    Bei 1 Tablette pro Tag gibt man einfach alle 12 Stunden eine halbe Tablette.
</p>
<p>
    Einige der Kater müssen aber `1 1/4` Tabletten am Tag bekommen.
    Da sich die Tabletten maximal vierteln lassen, kann ich diese Dosis nicht halbieren.
    Ich kann sie aber aufteilen in morgens eine halbe Tablette und abends drei Viertel.
    Jetzt muss ich noch ausrechnen, in welchem Zeitabstand die Teildosen verabreicht werden müssen,
    damit der Abstand zur gegebenen Menge passt.
</p>


<?php $this->startComponent('hidden') ?>
    <h4>Lösung mit dem Dreisatz</h4>
    <p>
        Nach der Gabe von `1,25` Tabletten muss ich `24h` warten bis zur nächsten Gabe.
        Oder anders ausgedrückt: `1,25` Tabletten reichen für `24h`.
        Da weniger Tablette auch für weniger Zeit reicht, ist es ein proportionaler Zusammenhang.
    </p>
    <p>
        `{:( 1,25T , |-> , 25h ),  ( 0,5T  , |-> , "?" ) :}`
    </p>
    <p>
        Ich löse das mit dem schönen Dreisatz:<br/>
        `{:( 1,25T , |-> , 25h ), 
            ( 1T  , |-> , {25h}/1,25 = 19,2h ),
            ( 0,5T  , |-> , 19,2h*0,5 = 9,6h )
            :}`
    </p>

    <h4>Alternativer Ansatz</h4>
    <p>
        Tagesdosis: `1 1/4 = 4/4 + 1/4 = 5/4 = 5 * 1/4` in 24 Stunden,<br/>
        Ich habe also 5 Vierteltabletten für den Tag, von denen ich 2 morgens und 3 Abends gebe.<br/>
        Ich teile also den Tag zunächst auch in 5 Teile: `24h : 5 = 4,8h`<br/>
        Nach der ersten Dosis von 2 Vierteltabletten muss also diese Zeit vergehen: `2*4,8h = 9,6h`<br/>
        Das ist der Abstand zwischen erster und zweiter Dosis.<br/>
        Nach der zweiten Dosis von 3 Vierteltabletten muss diese Zeit vergehen: `3*4,8h = 14,4h`<br/>
        Das kann ich auch so rechnen: `24h - 9,6h = 14,4h`.
    </p>
    <hr/>
    <p>
        Gebe ich also die erste Dosis morgens um 10&nbsp;Uhr, so muss die zweite Dosis um 19:36&nbsp;Uhr gegeben werden.
    </p>
<?php $this->endComponent('hidden') ?>




