<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>


<p>
    PiiRi ist in einer Demo-Version verfügbar unter <a href="https://kursbegleiter.de/piiri" target="_blank">kursbegleiter.de/piiri</a>
</p>
<?= Template::component('media', ['path'=>$post, 'media'=>'folge5.mp4', 'caption'=>'Der 4-Bit-Addierer in PiiRi', 'class'=>'center']) ?>

<p>
    Aus mehreren Volladdierern baust du jetzt einen Addierer für mehrstellige Binärzahlen.
    
    Eine übliche Einheit für die Rechenbreite ist das Byte, also 8 Bit.
    Du Brauchst die Eingänge `a` und `b` für die beiden Summanden,
    auch bekommt die Komponente einen Eingang für einen Übertrag, falls man mal mehrere Addierer zusammenschalten möchte.

    Als Ausgang benötigst du die Summe und einen Anschluss für den letzten Übertrag.

    Um das Ganze übersichtlicher zu halten, reduziere ich die Stellen auf 4 Bit.
    (Du kannst später auch einen 8-Bit-Addierer bauen, in dem du zwei 4-bit-Addierer zusammenschaltest.)
</p>
<p>
    Jede Stelle wird jetzt von einem Volladdierer verarbeitet, der zwei Summanden-Bits und den Übertrag von der vorherigen Stelle entgegen nimmt.
</p>
<p>
    Du benötigst noch Adapter, um Einzelleitungen zu Multipin-Leitungen zusammenzufassen und 
    umgekehrt Multipin-Leitungen in Einzelsignale aufzutrennen.
    So hast du später nach außen eine 4-Bit-Leitung und kannst in der Schaltung jede Stelle für sich verarbeiten.
</p>
<p>
    Jetzt geht's ans Verbinden. 
    Der erste Volladdierer bekommt von beiden Summanden das erste Bit, sowie den Übertrag aus dem Eingang ü. 
    Der zweite Volladdierer bekommt von beiden Summanden jeweils das zweite Bit, sowie den Übertrag aus der vorherigen Stelle.
    Und so weiter...
</p>
<p>
    Der letzte Übetrag wird an den Ausgang geleitet.
    Die Summen-Bits der Volladdierer bilden zusammen das Ergebnis der Rechnung.
</p>
<p>
    Mithilfe der Wertetabelle überprüfe ich das Ganze. Hier wird in der Tat gerechnet.
</p>
<p>
    Interessant ist hier zu erwähnen, dass die Schaltung genau genommen
    nicht Stelle für Stelle arbeitet, sondern alles auf einmal macht:
    Sobald die Eingangs-Signale da sind, ist das Ergebnis praktisch sofort da.
</p>
<p>
    Speichere die Schaltung ab, um sie im nächsten Schritt testen zu können.
</p>


