<?php

return [
    'show'=>true,
    'title'=>'Ein Rechenwerk mit PiiRi: Zwei richtige Zahlen addieren',
    'date'=>'2024-10-22',
    'tags'=>['Informatik', 'Schule', 'Schaltung', 'TechnischeInformatik', 'piiri', 'Rechenwerk'],
    'description'=>'Mit der Schaltungssimulation PiiRi bauen wir einen 4-Bit-Addierer und verwenden dabei den Baustein aus dem letzten Video.',
    'thumbnail'=>'_ca9ad7ba-aa5b-4c2e-91ff-cfda8a66e32a.jpeg',
    'emblem'=>'https://kursbegleiter.de/piiri/img/logo.svg',
];