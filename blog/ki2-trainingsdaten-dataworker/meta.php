<?php

return [
    'show'=>true,
    'title'=>'KI III: Trainingsdaten und Dataworker',
    'date'=>'2024-08-06',
    'tags'=>['KI','Informatik','Gesellschaft'],
    'description'=>'Das Training eines KI-Systems hängt maßgeblich von der Qualität der Trainingsdaten ab.
    Wo kommen diese eigentlich her? Wir erzeugen selbst Trainingsdaten zur Smiley-Erkennung
    und schauen uns auch den gesellschaftlichen Aspekt an.',
    'thumbnail'=>'thumb.jpeg'
];