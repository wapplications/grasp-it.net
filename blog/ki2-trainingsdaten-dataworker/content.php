<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>



<p>
    Das Frauenhofer Institut schreibt in einer Veröffentlichung:
</p>
<blockquote>
Der Rohstoff der Digitalisierung und der Künstlichen Intelligenz (KI) sind Daten.
</blockquote>

<p>
    Für KI sind das insbesondere Daten, mit denen die Systeme trainiert werden,
    das zu tun, was man von ihnen erwartet, die sogenannten Trainingsdaten.
    Das bedeutet aber auch: Schlechte Trainingsdaten führen zu schlechten Ergebnisse.
    Dazu gibt es diese Anekdote:
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'husky.png', 'caption'=>'Ein Husky wurde als Wolf erkannt.', 'class'=>'right-2', 'c'=>'Ribeiro et al. 2016']) ?>
<blockquote style="overflow:hidden;">
Das KI-System, das auf Fotos von Hunden und Wölfen trainiert wurde,
versagte bei der Identifizierung eines Husky-Fotos.
Die Analyse ergab, dass das System nicht die Tiere selbst,
sondern die Umgebung für die Entscheidungsfindung herangezogen hatte,
da es nur mit Wolfsbildern trainiert worden war, auf denen Schnee zu sehen war.
Daher führte die fehlerhafte Annahme, dass Schnee gleich Wolf und kein Schnee gleich
Hund ist, zu einer falschen Identifizierung.
<cite>
»“Why Should I Trust You?” Explaining the Predictions of Any Classifier«, Ribeiro et al. 2016,<br/>
<a href="https://arxiv.org/pdf/1602.04938" target="_blank">https://arxiv.org/pdf/1602.04938</a>
</cite>
</blockquote>

<p>
    Diese Anekdote ist vielleicht ganz niedlich.
    Aber was, wenn 
    <a href="https://www.zeit.de/digital/internet/2015-07/google-fotos-algorithmus-rassismus" target="_blank">
    dunkelhäutige Menschen von einer KI als Gorillas kategorisiert
    </a>
    werden?
    So geschenen im Jahre 2015 bei der automatischen Verschlagwortung in der Google Foto App.
    Dann hört der Spaß auf…
</p>

<h3>Woher kommen Trainingsdaten?</h3>

<p>Trainingsdaten für künstliche Intelligenz können aus unterschiedlichen Quellen stammen:</p>
<ul>
    <li>
        <strong>Öffentlich zugängliche Daten:</strong> 
        Dazu gehören Daten aus dem Internet, wie Webseiten, soziale Medien, Foren und wissenschaftliche Publikationen.
    <li>
        <strong>Proprietäre Daten:</strong> 
        Unternehmen sammeln oft eigene Daten durch ihre Produkte und Dienstleistungen, die sie dann zur Schulung ihrer KI-Modelle verwenden.
    <li>
        <strong>Öffentliche Datensätze:</strong> 
        Es gibt viele öffentlich verfügbare Datensätze, die speziell für die Schulung von KI-Modellen erstellt wurden, wie z.B. ImageNet für Bilderkennung oder das Common Crawl Dataset für Text.
    <li>
        <strong>Simulierte Daten:</strong>
        In einigen Fällen werden Daten durch Simulationen erzeugt, um spezifische Szenarien zu modellieren, die in der realen Welt schwer zu erfassen sind.
    <li>
        <strong>Crowdsourcing:</strong> 
        Plattformen wie Amazon Mechanical Turk ermöglichen es, Daten durch die Beiträge vieler Menschen zu sammeln.
</ul>

<p>
    Es ist ein umstrittenes Thema, welche Daten für das Trainig einer KI verwendet werden dürfen.
    Problematisch sind dabei Datenschutz und das Urheberrecht.
    Dazu wurde und wird auch vor Gericht gestritten. Aber das ist ein Thema für sich.
</p>

<h4>Crowdsourcing &amp; Clickworker</h4>
<p>
    Sogenannte Clickworker oder Data-Worker beschreiben Bilder,
    beurteilen die Verständlichkeit von Texten oder klassifizieren zum Beispiel das Verhalten von Menschen in einem Video,
    um Datensätze für KI-Firmen zu generieren.
    Dabei verdienen sie pro Aufgabe ein paar Cent.
</p>
<p>
    Manchen Firmen, die Clickworker anheuern, wird vorgeworfen, es auszunutzen,
    dass Menschen in bestimmten Teilen der Erde lieber so einen Job für sehr
    sehr wenig Geld machen, als nichts zu Essen zu haben. 
    Außerdem sind wohl einige der Data-Worker psychisch heftigen Dingen ausgesetzt,
    wenn sie z.B. Gewalt oder andere unangenehme Sachen auf Bildern identifizieren müssen.
</p>
<p>
    Andere sagen: Es ist doch gut wenn die Menschen dort eine bezahlte Arbeit haben – besser als gar nichts.
    Und bei manchen Firmen bekommen die Arbeiter wohl auch psychologische Unterstützung bei solchen Aufgaben.
</p>

<aside>
    <strong>Video-Ressourcen</strong>
    <ul>
    <li><strong>tagesschau.de: </strong><a href="https://www.tagesschau.de/multimedia/sendung/tagesthemen/video-1291858.html" target="_blank">Füttern der KI - Lohnarbeiter in Billiglohnländern im Dienst der Hightech-Branche</a></li>
    <li><strong>DW: </strong><a href="https://www.dw.com/de/datenarbeiter-die-unsichtbaren-trainer-der-ki/video-68471539" target="_blank">Datenarbeiter, die unsichtbaren Trainer der KI</a></li>
    </ul>
</aside>

<!-- < ?= Template::component('media', ['path'=>$post, 'media'=>'untrainiert.png', 'caption'=>'Ein untrainiertes System liefert unbrauchbare Antworten.', 'class'=>'right-1', 'c'=>'Stefan Beyer | smileki']) ? > -->
<?= Template::component('media', ['path'=>$post, 'media'=>'testing-untrainiert.mp4', 'caption'=>'Ein untrainiertes System liefert unbrauchbare Antworten.', 'class'=>'right-1', 'c'=>'Stefan Beyer | smileki']) ?>
<h3>Trainingsdaten für eine Smiley-Erkennung</h3>
<p>
    Mit der <a href="https://kursbegleiter.de/smileki/smileys.html" target="_blank">SmileKI Smiley-Erkennung</a>
    kann man einfache Trainingsdaten selbst erzeugen,
    um später damit eine Smiley-Erkennung zu trainieren.
    Das System soll traurige, föhliche und neutrale Smileys anhand von kleinen Pixelbildern erkennen.
    Allerdings kann es am Anfang noch gar nichts: Die Ergebnisse sind zufällig und unbrauchbar.
    Das sieht man auch, wenn man einmal das Testing durchlaufen lässt:
    Vielleicht ist zufällig mal etwas richtig, aber das kann man ja nicht »intelligent« nennen.
</p>
<p>
    Ähnlich wie bei der <a href="?a=ki1-training-testing">Stau-Erkennung oder den bissigen Affen</a>,
    brauchen wir Bilder, deren zugehörige Kategorie bekannt ist, sog. <em>gelabelte Daten</em>.
    Mit einem Teil dieser Bilder wird das System trainiert, mit einem anderen dann die Güte geprüft. 
</p>

<?= Template::component('media', ['path'=>$post, 'media'=>'daten-erstellen.png', 'caption'=>'Trainings-Datensatz erstellen.', 'class'=>'right-1 clear', 'c'=>'Stefan Beyer | smileki']) ?>
<p>
    Man zeichnet zum Beispiel das Pixelbild eines glücklichen Smileys und gibt dazu das passende Label (»happy«) an.
    Dann wird der Datensatz (bestehend aus Bild und Label) zu den Trainingsdaten hinzugefügt.
    Damit das System am Ende alles erkennt, was es erkennen soll,
    müssen von jeder Kategorie einige Beispiele vorhanden sein.
    Auch für das Testing müssen Beispiele aufgezeichnet werden.
    Gespeichert wird das Ganze so, dass der Computer später damit rechnen kann.
</p>
<aside>
    <style>
        .codeblock {
            white-space:pre; font-family: monospace; font-style:normal;            
        }
        .codeblock .key {color: #7700aa;}
        .codeblock .number {font-weight: bold; color:#55aa22}
    </style>
    <strong>Wie die Trainingsdaten gespeichert werden</strong>
    <p>
        Es müssen hier sowohl das Bild, als auch die erwartete Emotion (Label) in eine Form gebracht werden,
        mit der gerechnet werden kann: Zahlen.
    </p>
    <blockquote class="codeblock">
{
    <span class="key">"input"</span>: [
        <span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>, <span class="number">1</span>,<span class="number">1</span>,<span class="number">0</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">0</span>,<span class="number">1</span>,<span class="number">1</span>,
        <span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>, <span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,
        <span class="number">1</span>,<span class="number">0</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">0</span>,<span class="number">1</span>, <span class="number">1</span>,<span class="number">0</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">0</span>,<span class="number">1</span>,
        <span class="number">1</span>,<span class="number">1</span>,<span class="number">0</span>,<span class="number">0</span>,<span class="number">0</span>,<span class="number">0</span>,<span class="number">1</span>,<span class="number">1</span>, <span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>,<span class="number">1</span>
    ],
    <span class="key">"output"</span> [ <span class="number">1</span>, <span class="number">0</span>, <span class="number">0</span> ]
}
    </blockquote>
    <p>
        Als Eingabe oder »input« sehen wir eine lange Folge von 1 und 0, in der jede Zahl für ein Pixel des Bildes steht.
        Welche Ausgabe wir zu dem Bild erwarten, geben wir mit »output« an:
        Eine Liste von dreimal 0 oder 1.
        Die erste Stelle steht für »happy«, die zweite für »sad« und die dritte für »neutral«.
        Da in unserem Fall immer nur eines davon zutrifft, ist immer nur an einer der drei Stellen eine 1.
        Zum Beispiel steht <code>[0,1,0]</code> für »traurig«.
    </p>
</aside> 
<p>
    Im nächsten Teil geht es darum, ein neuronales Netz zu konfigurieren und mit den erstellten Daten zu trainieren.
</p>

