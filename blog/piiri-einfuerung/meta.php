<?php

return [
    'show'=>true,
    'title'=>'Ein Rechenwerk mit PiiRi: Einführung in die Simulation logischer Schaltungen',
    'date'=>'2024-10-07',
    'tags'=>['Informatik', 'Schule', 'Schaltung', 'TechnischeInformatik', 'piiri', 'Rechenwerk'],
    'description'=>'PiiRi ist eine Browser-App zum Simulieren von logischen Schaltungen und Schaltkreisen.
    In diesem Artikel gebe ich einen ersten Überblick, wie man mit dem Tool logische Schaltungen aus Logik-Gattern aufbaut.',
    'thumbnail'=>'_07a53deb-2346-463c-8bc1-6c22cf076ee2.jpeg',
    'emblem'=>'https://kursbegleiter.de/piiri/img/logo.svg',
];