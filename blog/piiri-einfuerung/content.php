<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>


<p>
    PiiRi ist in einer Demo-Version verfügbar unter <a href="https://kursbegleiter.de/piiri" target="_blank">kursbegleiter.de/piiri</a>
</p>
<?= Template::component('media', ['path'=>$post, 'media'=>'cast1folge1.mp4', 'caption'=>'Einleitung in die Schaltungssimulation mit PiiRi', 'class'=>'center']) ?>

<p>
    Auf der linken Seite hast du alle möglichen Komponenten, mit denen du eine Schaltung aufbauen kannst:
    Normale Logikbausteine wie "und" und "oder".
    Eingebaute Komponenten wie LEDs, Schalter oder eine Sieben-Segment-Anzeige.
    Du kannst aber auch eigene Komponenten abspeichern und in anderen Schaltungen wiederverwenden.
</p>
<p>
    Du erstellst zuerst EIngänge und Ausgänge.
    Danach ziehst du die gewünschten Komponenten in deine Schaltung und verbindest sie mit Leitungen.
</p>
<p>
    Mit der Maus kannst du jetzt die Eingänge an- und ausschalten, um die Schaltung auszuprobieren.
</p>



