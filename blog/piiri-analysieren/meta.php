<?php

return [
    'show'=>true,
    'title'=>'Ein Rechenwerk mit PiiRi: Wertetabellen und Impulsdiagramme erstellen',
    'date'=>'2024-10-13',
    'tags'=>['Informatik', 'Schule', 'Schaltung', 'TechnischeInformatik', 'piiri', 'Rechenwerk'],
    'description'=>'Mit der Schaltungssimulation PiiRi können Schaltungen analysiert werden,
    indem man sich Wertetabellen und Impulsdiagramme erzeugen lässt.',
    'thumbnail'=>'_d14a14f1-889f-42fc-bffe-f62df4dd8d1c.jpeg',
    'emblem'=>'https://kursbegleiter.de/piiri/img/logo.svg',
];