<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>



<p>
    PiiRi ist in einer Demo-Version verfügbar unter <a href="https://kursbegleiter.de/piiri" target="_blank">kursbegleiter.de/piiri</a>
</p>
<?= Template::component('media', ['path'=>$post, 'media'=>'cast1folge2.mp4', 'caption'=>'Mit PiiRi Schaltungen untersuchen', 'class'=>'center']) ?>

<p>
    Um deine Schaltung zu untersuchen, kannst du Wertetabellen erstellen
    von einzelnen Komponenten oder der ganzen Schaltung.
    Du markierst dazu die Komponente und wählst das Tabellen-Tool.
    Wenn keine Komponente markiert ist, wird das Verhalten der gesamten Schaltung analysiert
    und in der Tabelle dargestellt.
</p>
<p>
    Du kannst auch das Verhalten deiner Schaltung über die Zeit aufzeichen
    und ein Impulsdiagramm erstellen.
</p>




