<?php

return [
    'title'=>'KI V: Wie ein künstliches Gehirn aufgebaut ist',
    'date'=>'2024-03-21',
    'tags'=>['KI','Informatik','Gesellschaft'],
    'description'=>'Wir wagen einen Blick in die kleinste Einheit eines künstlichen Gehirns
    und sehen dadurch, dass das alles keine Hexerei ist:
    Eine geschickte Vernetzung von künstlichen Neuronen macht Dinge möglich,
    die wir oft im Detail gar nicht mehr so genau nachvollziehen können.',
    'thumbnail'=>''
];