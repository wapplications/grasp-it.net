<?php

return [
    'show'=>true,
    'title'=>'Ein Rechenwerk mit PiiRi: Das ganze Rechenwerk',
    'date'=>'2024-10-31',
    'tags'=>['Informatik', 'Schule', 'Schaltung', 'TechnischeInformatik', 'piiri', 'Rechenwerk'],
    'description'=>'Jetzt kommt alles zusammen: Das Rechenwerk mit auswählbarer Addier- und Subtrahier-Funktion wird zusammengebaut und getestet.',
    'thumbnail'=>'_49ca58d5-9d8a-48cf-997d-444c423246e6.jpeg',
    'emblem'=>'https://kursbegleiter.de/piiri/img/logo.svg',
];