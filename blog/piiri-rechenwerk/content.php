<?php if (!defined('ENTRY_POINT')) die('no entry point'); ?>


<p>
    PiiRi ist in einer Demo-Version verfügbar unter <a href="https://kursbegleiter.de/piiri" target="_blank">kursbegleiter.de/piiri</a>
</p>
<?= Template::component('media', ['path'=>$post, 'media'=>'folge8.mp4', 'caption'=>'Mit PiiRi ein Rechenwerk mit Addierer und Subtrahierer bauen', 'class'=>'center']) ?>
<p>
    Für das Rechenwerk, das auf Wunsch addieren und subtrahieren kann
    braucht es wieder zwei 4-Bit Eingänge und ein Steuersignal, ob wir addieren oder Subtrahieren wollen.
    Als Ausgang braucht es eine 4-Bit-Zahl und einen Übertrag.
</p>
<p>
    Die Komponente soll so arbeiten, dass entweder `a+b` oder bei aktiviertem Steuersignal `a-b` gerechnet wird.
    Das heißt, der Summand b muss erstmal durch die schaltbare Gegenzahl-Komponente geführt werden,
    bevor er an den Addierer angeschlossen wird. Der Summand `a` geht direkt an den Addierer.
</p>
<p>
    Zum Testen baue ich mir hier noch eine Anzeige-Komponente, die erkennt,
    ob am Eingang eine negative Zahl anliegt.
    Diese wird dann zum Anzeigen negiert (positiv gemacht) und ein Vorzeichen-Lämpchen leuchtet auf.
    Die Teile, die sichtbar bleiben sollen, werden entsprechend markiert.
</p>
<p>
    Ich baue jetzt eine Testschaltung auf, in der ich sowohl die beiden Summanden, als auch das Ergebnis
    mit der eben erstellten Komponente anzeigen lasse.
    Im Zentrum steht das Rechenwerk, das zwei 4-Bit-Zahlen an den Eingängen erhält, sowie einen Schalter,
    der zwischen Addition und Subtraktion umschaltet.
</p>
<p>
    Zum Beispiel kann folgendes mit dem Rechenwerk berechnet werden: `5 + 2 = 7`; `5 - 2 = 3`.<br/>
    `2+3=5`; `2-3=-1`.
</p>
<p>
    Aber Achtung: bei nur 4 Bit ist der Zahlenraum sehr beschränkt. So ist Zum Beispiel 5 plus 5 gleich minus Sechs. 
</p>
