<?php

use Blog\Post;

define('ENTRY_POINT', __FILE__);
require_once dirname(__FILE__) . '/../inc/boot.php';
Middleware::all();
StatUte::tag('site', 'blog');

// TODO copy code component

class Series {
  static $series = [];
  static function register($name, $names, $title, $info = null) {
    self::$series[$name] = (object)[
      'name' => $name,
      'names' => $names,
      'title' => $title,
      'info' => $info
    ];
  }

  static function for($postName) {
    return array_filter(static::$series, function($s) use($postName) {
      return in_array($postName, $s->names);
    });
  }

  static function find($name) {
    return self::$series[$name] ?? null;
  }
}


function series($name, $names, $title, $info = null) {
  Series::register($name, $names, $title, $info);
}

/** @deprecated */
function partOf($name, $me) {
  return '';
}

function renderPartOf($s, $me, $mode = 'last', $count = 4) {
  if (!$s) return '';
  if ($me instanceof Post) {
    $me = $me->getName();
  }
  $names = $s->names;
  if ($mode === 'random') {
    $indexes = array_rand($s->names, $count);
    $names = array_map(function($i) use($names) {
      return $names[$i];
    }, $indexes);
  } else if ($mode === 'last') {
    $names = array_slice($names, -$count);
  }
  return Template::component('related', [
    'names'=>$names,
    'title'=>$s->title,
    'current'=>$me,
    'html'=>'<a href="/blog?series='.e($s->name).'">Alles aus dieser Reihe »</a>'
  ]);
}



Template::register_component('media', function($params) {
  $path = $params['path'];
  $mediaUrl = $params['media'];
  $class = $params['class'];
  $c = $params['c'] ?? '';
  $caption = $params['caption'];
  if ($path instanceof PathProvider) {
    $mediaFile = $path->getPath($mediaUrl);
    $mediaHTTPPath = $path->getHTTPPath($mediaUrl);
  } else if (is_string($path)) {
    die('Path is string in media component not implemented');
    //$mediaUrl = rtrim($path, '/') . '/' . ltrim($mediaUrl, '/');
  }
  if (file_exists($mediaFile)) {
    $type = explode('/', mime_content_type($mediaFile));
  } else {
    $type = ['NOT_FOUND',null];
  }
  if (!empty($c)) {
    $c = '<br/><small>&copy; '.e($c).'</small>';
  }
  if (!empty($caption)) {
    $caption = '<figcaption>'.e($caption).$c.'</figcaption>';
  }
  $media = '';
  $fullSizeButton = true;
  switch ($type[0]) {
    case 'image':
      $media = '<img src="'.e($mediaHTTPPath).'" />';
      break;
    case 'video':
      $media = '<video controls>
      <source src="'.e($mediaHTTPPath).'" type="'.implode('/', $type).'" />
      <a href="'.e($mediaHTTPPath).'">download</a>
      </video>';
      break;
    case 'NOT_FOUND':
      $media = '<div class="not_found">Ressource nicht gefunden<br/><code><small>'.e($mediaUrl).'</small></code></div>';
      break;
  
  }
  if ($fullSizeButton) {
    // ="'.e($mediaUrl).'"
    $fullSizeButton = '<button class="fullSizer" data-fullSizer>
    <svg width="100%" height="100%" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M4 4H10V6H6V10H4V4ZM14 4H20V10H18V6H14V4ZM4 14H6V18H10V20H4V14ZM18 14H20V20H14V18H18V14Z" fill="currentColor"/>
</svg>


</button>';
  } else {
    $fullSizeButton = '';
  }
  return '<figure class="'.e($class).'">'.$media.''.$caption.$fullSizeButton.'</figure>';
}, [
  'path'=>null,
  'media'=>'',
  'class'=>'big',
  'caption'=>'',
  'c'=>null,
]);

Template::register_component('related', function($params) {
  $mode = $params['mode'];
  $current = $params['current'];
  $title = $params['title'];
  $names = $params['names'];
  $html = $params['html'];
  $list = '';
  if (!empty($html)) {
    $html = '<div class="related_extra_html">'.$html.'</div>';
  } else $html = '';
  if ($mode === 'linklist') {
    $list .= '<ul style="padding-left:1em;">';
    foreach($names as $name) {
      try {
        $post = new Post($name, false);
        $list .= '<li>';
        if ($name === $current) {
          $list .= e($post->getTitle());
        } else {
          $list .= '<a href="?a='.e($post->getname()).'">'.e($post->getTitle()).'</a>';
        }
        $list .= '</li>';  
      } catch (\Blog\NoPostException $e) {
      }
    }
    $list .= '</ul>';
  }
  return '<div class="related related_'.$mode.'">
  <h4>'.e($title).'</h4>
  '.$list.'
  '.$html.'
  </div>';
}, [
  'title'=>'',
  'names'=>[],
  'mode'=>'linklist',
  'current'=>null,
  'html'=>''
]);


Template::register_component('app', function($params) {
  static $appCounter = [];
  $name = $params['name'];

  if (!key_exists($name, $appCounter)) {
    $appCounter[$name] = 0;
  }
  $appCounter[$name]++;
  $title = $params['title'];
  $options = $params['options'];
  $id = $name . '_' . $appCounter[$name];
  $constructor = ucfirst($name);
  if ($appCounter[$name] === 1) {
    Template::global_add('endscripts', 'js/'.$name.'.js');
  }
  $init='
  <script>
    document.addEventListener("DOMContentLoaded", ()=>(new '.$constructor.'(document.getElementById('.json_encode($id).'), '.json_encode($options).')));
  </script>
  ';
  return '<div class="app '.$name.'">
  <h4>'.e($title).'</h4>
  <div id="'.$id.'"></div>
  '.$init.'
  <noscript style="color:red;">Das hier eingebundene Tool »'.$name.'« benötigt JavaScript.</noscript>
  </div>
  ';
}, [
  'title'=>'',
  'name'=>'',
  'options'=>null,
]);


Template::register_component('hidden', function($params) {
  static $counter = 0;
  $inner = $params['inner'];
  $title = $params['title'];
  $command = $params['command'];
  $id = 'hidden_'.$counter++;
  return '
<div class="dotborder">
    <p style="margin:0;"><strong>'.$title.'</strong> <a onclick="this.style.display=\'none\'; document.getElementById(\''.$id.'\').style.display=\'block\'">'.$command.'</a></p>
    <div id="'.$id.'" style="display:none;">
    '.$inner.'
      <div style="clear:both;"></div>
    </div>
</div>
  ';
}, ['title'=>'Lösung', 'command'=>'Lösung anzeigen…', 'inner'=>'-kein inneres angegeben-']);


Template::register_component('useAsciiMath', function($params) {
  return '

<script>
MathJax = {
  loader: {load: ["input/asciimath", "output/chtml"]},
  asciimath: {
    decimalsign: ","           // character to use for decimal separator
  },
}
</script>

<script type="text/javascript" id="MathJax-script" async
  src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/startup.js">
</script>

  ';
}, []);




function dummyPicture($dataUrl = false, $base64 = false) {
  $inner = '';
  for ($i = 0; $i < 80; $i++) {
    $x = random_int(0,50);
    $y = random_int(0,50);
    $r = random_int(0,30);
    $c = random_int(0,60)+30;
    $c = array_fill(0,3,$c);
    $c[] = random_int(20,100)/100;
    $c = implode(',',$c);
    $inner .= '<circle cx="'.$x.'" cy="'.$y.'" r="'.$r.'" fill="rgba('.$c.')" />' . PHP_EOL;
  }
  $thumb = '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 50 50">
  <!--<rect x="0" y="0" width="50" height="50" />-->
  '.$inner.'
  </svg>';
  if ($dataUrl) {
    $d = 'data:image/svg+xml;';
    if ($base64) {
      $d .= 'base64';
      $thumb = base64_encode($thumb);
    } else $d .= 'utf8';
    $thumb = $d . ',' . $thumb;
  }

  return $thumb;
}

function formatNumber($n, $places = 10) {
  return rtrim(rtrim(number_format($n, $places, ',', '.'), '0'), ',');
}


include BLOG_ROOT . '/series.php';





////////////////////////////////////////






$a = $_GET['a'] ?? null;

if (!empty($a)) {
  try {
    $post = \Blog\Post::load($a);
  } catch (\Blog\PostException $e) {
    http_response_code(404);
    die('Post not found.');
  }
  if (!$post->isVisible()) {
    http_response_code(404);
    echo 'post not found'; // TODO
    die();
  }
} else {
  $filter = null;
  $sort = 'newfirst';
  
  $tag = $_GET['tag'] ?? null;
  $series = $_GET['series'] ?? null;

  $filter = (object)[
    'tags'   => $tag ? [$tag] : null,
    'series' => $series
  ];
  $posts = \Blog\Post::list($filter, $sort);
}





# extending site template
($t = new Template('site.php'))
->set('body_class', 'blog')
->set('title', 'Blog')
->start('main_content');
# START MAIN CONTENT

// < ? = Template::component('icon', ['name'=>'education', 'color'=>COLOR_EDUCATION, 'size'=>'1.2em', 'class'=>"ri"]) ? >

// < ? = Template::component('paralax', ['image'=>'/images/paralax-education.jpg', 'class'=>'px_other'])  ? >

// ffmpeg -i video-2024-07-07_00.58.17.mp4 -vf "fps=20" jahr.gif


if (isset($posts)) { ?>

  <section class="container">
    <h1> Blog </h1>
    <?php if (!empty(array_filter(array_values(get_object_vars($filter))))) : ?>
      <div>
        Filter: 
        <?php if (!empty($filter->tags)) : ?>
          <?= e(implode(', ', array_map(fn ($e)=>('#'.$e), $filter->tags))) ?>
        <?php endif ?>
        <?php if (!empty($filter->series)) : ?>
          <?= e($filter->series) ?>
        <?php endif ?>
        <a href="?">&times</a>
      </div>
    <?php endif; ?>
    <div style="padding:1em; background-color:beige; border:chocolate 1px solid; margin-bottom:2em;">
      <?php foreach (\Blog\Post::$tag_cloud as $tag=>$count) : ?>
        <a href="?tag=<?= e($tag) ?>">#<?= e($tag) ?> (<?= e($count) ?>)</a>
      <?php endforeach; ?>
    </div>
    <div class="article-list">
    <?php
    foreach ($posts as $post) {
      echo Template::component('post-preview.php', ['post' => $post]);
    }
    ?>
    </div>
  </section>

<?php
} else if (isset($post)) {
  $p = $_GET['p'] ?? 1;
  if ($post->getPageCount()<=1) $p = 'all';
  if ($p !== 'all') {
    $p = (int)$p;
    if ($p < 1) $p = 1;
    if ($p > $post->getPageCount()) $p = $post->getPageCount();
  }
  echo Template::component('useAsciiMath');
  echo Template::component('post.php', [
    'post' => $post,
    'page' => $p
  ]);
  $t->set('title', $post->getTitle());
}
?>
  <?php if (!isset($posts) && !isset($post)) : ?>
    <section class="container">
    Fehler: Post nicht gefunden.
    </section>
  <?php endif; ?>

<script> 

function openFullSize(content) {
  /*let bd = document.createElement('div');
  bd.classList.add('backdrop');
  */

  let dialog = document.createElement('dialog');
  if (typeof content === 'string') {
    dialog.innerHTML = content;
  } else {
    dialog.appendChild(content)
  }
  let cb = document.createElement('button');
  cb.classList.add('close');
  cb.innerHTML = '&times;';
  cb.addEventListener('click',()=>{
    dialog.close();
  })
  dialog.appendChild(cb);
  document.body.append(dialog);
  dialog.showModal();

  dialog.addEventListener('click', ()=>{
    dialog.close();
  }, {once: true})
}


document.addEventListener('DOMContentLoaded', ()=>{
  let elements = document.querySelectorAll('[data-fullSizer]');
  for (let i = 0; i < elements.length; i++) {
    let parent = elements.item(i).parentElement;
    let firstElement = parent.firstElementChild;
    if (typeof firstElement === 'undefined' || firstElement === null) {
      continue;
    }
    elements.item(i).addEventListener('click', ()=>{
      openFullSize(firstElement.cloneNode(true));
    }); 
  }
})

</script>

<section class="container">
<p>
  <small>Bilder zum Teil KI-generiert.</small>
</p>
</section>


<?= Template::component('contact.php', ['subject' => 'blog', 'ruler' => false]) ?>
