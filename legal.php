<?php
define('ENTRY_POINT', __FILE__);
require_once './inc/boot.php';
Middleware::all();
StatUte::tag('site', 'legal');

# extending site template
($t = new Template('site.php'))
->set('body_class', 'general')
->set('title', 'Rechtliches')
->start('main_content');
# START MAIN CONTENT
?>

<section id="imprint">
  <section class="container">
    <h2>Impressum</h2>
    <div class="row">
      <div class="col-half">
        <p>Diese Seite wird betrieben von:</p>
        <p>
          Stefan Beyer<br/>
          IT-Dienstleistungen &amp; Bildung<br/>
          - Einzelunternehmer<br/>
          Untertal 17<br/>
          77790 Steinach<br/>
        </p>
      </div>
      <div class="col-half">
        <dl>
          <dt>E-Mail</dt>
          <dd class="fillContact email"></dd>
          <!--<dt>Telefon</dt>
          <dd class="fillContact phone"></dd>-->
        </dl>
      </div>
    </div>
  </section>
</section>


<?= Template::component('contact.php', ['subject' => 'impressum', 'ruler' => true]) ?>



<section id="privacy" class="info-notice" >
  <section class="container">
    <h2>Datenschutzerklärung</h2>

<?php
($cols = new Template('two-columns.php'))
->set('widths', 'big|small')
->start('first');
?>

  <p>
    Ich klären Sie hiermit auf, welche Daten zu welchem Zweck und für welche Dauer von mir
    (oder ggf. von weiteren Dienstleistern) über diese Webseite erhoben, gespeichert und verarbeitet werden.
    Des weiteren nenne ich die jeweilige Rechsgrundlage, auf die ich mich dabei berufe.
  </p>

  <p style="font-weight:bold;">
    Diese Seite verwendet keine Tools und Module, die Daten an dritte weitergibt:
    Es werden keine externen Analyse-Tools und keine Affiliate-Programme verwendet.
    Daher werden während des Besuchs auf dieser Seite keine personenbezogene Daten
    unbemerkt von dritten gesammelt!
  </p>

<?php $cols->start('second'); ?>

  <div style="background-color:#ddd; padding:1em;">
    <h5>Verantwortlich ist:</h5>
    <p>Stefan Beyer<br>Untertal 17<br>D-77790 Steinach<br><span class="fillContent email"></span></p>
    <p>
      <a href="#contactFormOuter">Zum Kontaktformular</a>
    </p>
  </div>
  
<?php $cols->output(); ?>




<h3>Allgemeine Rechte von Betroffenen</h3>


<?php ($cols = new Template('two-columns.php'))
->set('widths', 'half|half')
->start('first');
?>

  <p>Ich informieren Sie vorab darüber, dass Sie jederzeit folgende Rechte ausüben können:</p>
  <ul>
    <li>Auskunft über Ihre bei mir gespeicherten Daten und deren Verarbeitung,</li>
    <li>Berichtigung unrichtiger personenbezogener Daten,</li>
    <li>Löschung Ihrer bei mir gespeicherten Daten,</li>
    <li>Einschränkung der Datenverarbeitung, sofern ich Ihre Daten aufgrund gesetzlicher Pflichten noch nicht löschen darf,</li>
    <li>Widerspruch gegen die Verarbeitung Ihrer Daten bei mir</li>
    <li>und Datenübertragbarkeit, sofern Sie in die Datenverarbeitung eingewilligt haben oder einen Vertrag mit mir abgeschlossen haben.</li>
  </ul>

<?php $cols->start('second'); ?>

  <p>Wenn Sie mir eine Einwilligung zur Speicherung und Verarbeitung erteilt haben, können Sie diese jederzeit mit Wirkung für die Zukunft widerrufen.</p>
  <p>Sie können sich jederzeit mit einer Beschwerde an die für Sie zuständige Aufsichtsbehörde wenden. Ihre zuständige Aufsichtsbehörde richtet sich nach dem Bundesland Ihres Wohnsitzes, Ihrer Arbeit oder der mutmaßlichen Verletzung. Eine Liste der Aufsichtsbehörden (für den nichtöffentlichen Bereich) mit Anschrift finden Sie unter:
    <span style="overflow-wrap: break-word;">https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html</span>.
  </p>

<?php $cols->output(); ?>







<h3>Die Datenverarbeitung im Detail</h3>

<?php ($cols = new Template('two-columns.php'))
->set('widths', 'half|half')
->start('first');
?>

  <p>In diesen Bereichen werden Personenbezogene Daten, zum Teil aber nur nach ausdrücklicher Erlaubnis
    durch den Betroffenen, erhoben:</p>
  <ul>
  <li><a href="#privacy_log"><span class="glyphicon glyphicon-hdd"></span> Logdatei auf dem Server</a></li>
  <li><a href="#privacy_contact"><span class="glyphicon glyphicon-envelope"></span> Verwendung des Kontaktformulars</a></li>
  <li><a href="#privacy_feedback"><span class="glyphicon glyphicon-envelope"></span> Feedback-Funktion</a></li>
  </ul>

  <h3 id="privacy_log"><span class="glyphicon glyphicon-hdd"></span> Logdatei auf dem Server</h3>
  <p>
    Mein Web-Server zeichnet bei jedem Besuch verschiedene Informationen über die Zugriffe auf die Seite auf.
    Es handelt sich um Daten, die Ihr Browser bei der Anfrage an den Server mitsendet.
  </p>
  <h5>Diese Daten werden erfasst:</h5>
  <ul><li>Datum und Uhrzeit des Zugriffs,</li><li>Pfad der angeforderten Ressource,</li><li>ob die Anfrage erfolg reich war oder nicht,</li><li>die Größe der ausgelieferten&nbsp;Daten (Dateigröße),</li><li>ggf. die URL der Seite, von aus man hier her gelangt ist (engl. <i>referrer</i>),</li><li>Name von Betriebssystem und Browser-Software, sofern Ihr Browser diese Information überträgt,</li><li>IP-Adresse des aufrufenden Rechners.</li></ul>
  <h5>Zweck der Datenerhebung:</h5>
  <p>Diese Informationen werden genutzt,</p><ul><li>um im Fehlerfall die betreffenden Seitenaufrufe nachvollziehen zu können,</li><li>um daraus Statistiken über die Anzahl der Seitenaufrufe abzuleiten (Hierbei wird die globale Nutzung des Angebots insgesamt, nicht jedoch das Verhalten einzelner Nutzer ausgewertet),</li><li>um ggf. bei strafrechtlich relevanten Aktivitäten (z.B. Missbrauchs- oder Betrugshandlungen) entsprechende Schritte einleiten zu können.</li></ul>
  <h5>Dauer der Speicherung:</h5>
  <p>Die Dauer der Speicherung ist technisch auf 7 Tage begrenzt. Wenn der Zweck in Einzelfällen eine längere Speicherung ausgewählter Datensätze (z.B. als Beweismittel) erforderlich macht, werden die Daten erst nach Klärung des Sachverhaltes gelöscht.</p>
  <h5>Rechtsgrundlage:</h5>
  <ul><li><a href="https://dsgvo-gesetz.de/art-6-dsgvo/" target="_blank">Art. 6 Abs. 1 lit. f DSGVO</a>&nbsp;(berechtigtes Interesse): Absicherung des Angebots, Aufrufszahlen</li></ul>

<?php $cols->start('second'); ?>

  <h3 id="privacy_contact"><span class="glyphicon glyphicon-envelope"></span> Verwendung des Kontaktformulars</h3>
  <p>Sie können über das Kontaktformular direkt eine E-Mail an mich senden. </p>
  <h5>Diese Daten werden erfasst:</h5>
  <ul><li>Name</li><li>E-Mail-Adresse</li><li>Ihre Nachricht</li><li>ggf. IP-Adresse</li></ul>
  <h5>Zweck der Datenerhebung:</h5>
  <p>Die Daten werden genutzt, um eine Kommunikation – begrenzt auf das von Ihnen formulierte Anliegen – zu ermöglichen und bei strafrechtlich relevanter Aktivität (Beleidigungen, verbotene politische Propaganda, etc.) ggf. notwendige Schritte einzuleiten.</p>
  <h5>Dauer der Speicherung:</h5>
  <p>Die Daten werden für die Dauer der laufenden Kommunikation, bzw. bis zur Klärung Ihres Anliegens gespeichert. Wenn der Zweck in Einzelfällen eine längere Speicherung erforderlich macht (z.B. als Beweismittel), werden die Daten ggf. erst nach Klärung des Sachverhaltes gelöscht.</p>
  <h5>Rechtsgrundlage:</h5>
  <ul><li><a href="https://dsgvo-gesetz.de/art-6-dsgvo/" target="_blank">Art. 6 Abs. 1 lit. b DSGVO</a>&nbsp;(Erfüllung eines Vertrags, vorvertragliche Maßnahmen)</li><li><a href="https://dsgvo-gesetz.de/art-6-dsgvo/" target="_blank">Art. 6 Abs. 1 lit. f DSGVO</a>&nbsp;(berechtigtes Interesse)</li></ul>


  <h3 id="privacy_feedback"><span class="glyphicon glyphicon-envelope"></span> Feedback-Funktion</h3>
  <p>Über das Feedback-Formular können Kunden eine Rückmeldung zu von mir erbrachten Dienstleistungen verfassen.
  Die Nutzung dieser Funktion ist freiwillig und an eine Einwilligung zur Verarbeitung der anfallenden Daten gebunden.</p>
  <h5>Diese Daten werden erfasst:</h5>
  <ul><li>alle Informationen die Sie im jeweiligen Fragebogen angeben.</li></ul>
  <h5>Zweck der Datenerhebung:</h5>
  <p>Die Daten werden genutzt, um eine unkomplizierte Rückmeldung meiner Kunden zu ermöglichen und mein Angebot daraufhin ggf. zu verbessern oder anzupassen. Falls Sie die Erlaubnis dazu erteilen, kann Ihr Feedback anonymisiert veröffentlicht werden.</p>
  <h5>Dauer der Speicherung:</h5>
  <p>
    Die erfassten Daten werden bis auf Widerruf dauerhaft gespeichert.
    Ein Widerruf ist nur möglich, wenn eine eindeutige Zuordnung zum Verfasser der Daten möglich ist.
    Wird das Feedback annonym abgegeben ist eine derartige Zuordnung nicht möglich.
  </p>
  <h5>Rechtsgrundlage:</h5>
  <ul><li><a href="https://dsgvo-gesetz.de/art-6-dsgvo/" target="_blank">Art. 6 Abs. 1 lit. a DSGVO</a> (Einwilligung zur Verarbeitung, die Sie beim versenden Ihrer Nachricht explizit abgeben)</li><li><a href="https://dsgvo-gesetz.de/art-6-dsgvo/" target="_blank">Art. 6 Abs. 1 lit. f DSGVO</a>&nbsp;(berechtigtes Interesse)</li></ul>

<?php $cols->output(); ?>







  </section>
</section>



